from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.preprocessing import StandardScaler
import multiprocessing as mp
import numpy as np

from utils import logger


class Predictor(object):
    """ final evaluation on held back data """

# ______________________________________________________________________________________________________________________
    def scale_features(self, feature_matrix_train, feature_matrix_test):
        scaler = StandardScaler(copy=False)
        self.scaler_obj = scaler
        feature_matrix_train = scaler.fit_transform(feature_matrix_train)
        feature_matrix_test = scaler.transform(feature_matrix_test)
        return feature_matrix_train, feature_matrix_test

# ______________________________________________________________________________________________________________________
    def create_classifier(self):
        """ takes the classifier as specified by self.clf_name and creates the chosen classifier. choices are
        ['RF', 'SVM', 'AB', 'NB', 'KNN', 'GP', 'QDA', 'DT']
        :return: the classifier object
        """
        # AdaBoost
        if self.clf_name == 'AB':
            from sklearn.ensemble import AdaBoostClassifier
            clf = AdaBoostClassifier(
                n_estimators=self.n_estimators,
            )

        # Logistic Regression
        elif self.clf_name == 'LR':
            from sklearn.linear_model import LogisticRegression
            clf = LogisticRegression(
                n_jobs=self.n_jobs,
            )

        # Support Vector Machine
        elif self.clf_name == 'SVM':
            from sklearn.svm import SVC
            clf = SVC(
                kernel='rbf',
                probability=True,
                # recommendation by matthias to increase cache size
                cache_size=2048,
            )

        # Naive Bayes
        elif self.clf_name == 'NB':
            from sklearn.naive_bayes import GaussianNB
            clf = GaussianNB()

        # K nearest neighbors
        elif self.clf_name == 'KNN':
            from sklearn.neighbors import KNeighborsClassifier
            clf = KNeighborsClassifier(
                n_jobs=self.n_jobs,
            )

        # Gaussian Process
        elif self.clf_name == 'GP':
            from sklearn.gaussian_process import GaussianProcessClassifier
            clf = GaussianProcessClassifier(
            )

        # Decision Tree
        elif self.clf_name == 'DT':
            from sklearn.tree import DecisionTreeClassifier
            clf = DecisionTreeClassifier(
            )

        # Quadratic Discriminant Analysis
        elif self.clf_name == 'QDA':
            from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
            clf = QuadraticDiscriminantAnalysis()

        # Default: Random Forest
        else:
            from sklearn.ensemble import RandomForestClassifier
            clf = RandomForestClassifier(
                n_jobs=self.n_jobs,
                n_estimators=self.n_estimators,
                oob_score=False,
            )

        return clf

# ______________________________________________________________________________________________________________________
    def evaluate_repetition(self, labels_test, predictions, repetition, n_classes, clf):
        self.confusion_matrices[repetition] = confusion_matrix(labels_test, predictions, labels=np.arange(n_classes))

        # copute specificity if wanted/needed
        # [[tn, fp], [fn, tp]] = self.confusion_matrices[fold_id]
        # print(tn, fp, fn, tp)

        # evaluate fold predictions and store results
        self.results[repetition] = [
            accuracy_score(labels_test, predictions),
            precision_score(labels_test, predictions),
            recall_score(labels_test, predictions),
            f1_score(labels_test, predictions)
        ]

        if self.clf_name == 'RF':
            self.importances[repetition] = clf.feature_importances_
            # self.oob_scores[repetition] = clf.oob_score_

# ______________________________________________________________________________________________________________________
    def create_labels(self, feature_matrices):
        labels = []
        for class_id, feature_matrix in enumerate(feature_matrices):
            labels.extend(len(feature_matrix) * [class_id])
        return np.asarray(labels)

# ______________________________________________________________________________________________________________________
    def execute(self, in_q, out_q, clfs):
        while not in_q.empty():
            repetition_id, train, labels_train, test, labels_test = in_q.get(timeout=1)
            train, test = self.scale_features(train, test)

            # clf = self.create_classifier()
            clf = clfs[repetition_id]
            clf.fit(train, labels_train)
            predictions = clf.predict(test)

            acc = accuracy_score(labels_test, predictions)
            prec = precision_score(labels_test, predictions)
            rec = recall_score(labels_test, predictions)
            f1 = f1_score(labels_test, predictions)
            cm = confusion_matrix(labels_test, predictions)

            out_q.put((repetition_id, [acc, prec, rec, f1], cm))

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ from each subprocess take the results from out_q """
        results = out_q.qsize() * [None]
        cms = out_q.qsize() * [None]
        while not out_q.empty():
            fold_id, fold_results, cm = out_q.get()
            results[fold_id] = fold_results
            cms[fold_id] = cm
        return results, cms

# ______________________________________________________________________________________________________________________
    def parallel_predict(self, feature_matrices_train, feature_matrices_eval, clfs):
        # TODO: how to use parallel_executer here?
        manager = mp.Manager()
        in_q, out_q = manager.Queue(maxsize=self.n_repetitions), manager.Queue(maxsize=self.n_repetitions)
        processes = np.ndarray(shape=(self.n_repetitions,), dtype=mp.Process)

        labels_train = self.create_labels(feature_matrices_train)
        train = np.vstack(feature_matrices_train)

        labels_eval = self.create_labels(feature_matrices_eval)
        eval = np.vstack(feature_matrices_eval)

        # parellalize cv folds. but only start that many processes as specified by n_proc
        # for each cv fold pick data, generate labels and hand it to a subprocess
        worked_folds = 0
        while worked_folds < self.n_repetitions:
            for process_id in range(worked_folds, min(worked_folds+self.n_jobs, self.n_repetitions)):
                in_q.put((process_id-worked_folds, train, labels_train, eval, labels_eval))
                processes[process_id] = mp.Process(target=self.execute, args=(in_q, out_q, clfs))

            for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_jobs]):
                logger.info("\t\tTraining and predicting fold {}.".format(worked_folds+process_id))
                process.start()

            for process in processes[worked_folds:worked_folds+self.n_jobs]:
                process.join()

            # catch the results as computed by the subprocesses
            resutls, cms = self.catch_results(out_q)
            self.results[worked_folds:worked_folds+self.n_jobs] = resutls
            self.confusion_matrices[worked_folds:worked_folds+self.n_jobs] = cms
            worked_folds += self.n_jobs

# ______________________________________________________________________________________________________________________
    def predict(self, feature_matrices_train, feature_matrices_eval):
        # create labels for training and evaluation: normal is 0 abnormal 1
        labels_train = self.create_labels(feature_matrices_train)
        labels_eval = self.create_labels(feature_matrices_eval)

        # stack matrices for usage in training/prediction step
        feature_matrix_train = np.vstack(feature_matrices_train)
        feature_matrix_eval = np.vstack(feature_matrices_eval)

        clf = self.create_classifier()
        for repetition in range(self.n_repetitions):
            logger.debug("repetition {}".format(repetition))
            clf = clf.fit(feature_matrix_train, labels_train)
            predictions = clf.predict(feature_matrix_eval)
            # prediction_probs = clf.predict_proba(feature_matrix_eval)

            self.evaluate_repetition(labels_eval, predictions, repetition, len(feature_matrices_train), clf)

            if self.clf_name == 'RF':
                self.importances[repetition] = clf.feature_importances_

            # this varies for all repetitions. save all? save mean?
            # io.write_classifier_output(
            #     cmd_args.output,
            #     self.n_repetitions,
            #     labels_eval,
            #     predictions,
            #     prediction_probs,
            #     rec_names_eval
            # )

# ______________________________________________________________________________________________________________________
    def get_results(self):
        """ compute mean and std of cv fold metrics accuracy, precision, recall and f1-score """
        results = self.results * 100
        mean_results = np.mean(results, axis=0)
        std_results = np.std(results, axis=0)

        # 0: accs, 1: precs, 2: recs, 3: f1s
        return [results[:, 0], mean_results[0], std_results[0]], \
               [results[:, 1], mean_results[1], std_results[1]], \
               [results[:, 2], mean_results[2], std_results[2]], \
               [results[:, 3], mean_results[3], std_results[3]]

# ______________________________________________________________________________________________________________________
    def __init__(self, clf_name, n_estimators, n_jobs=1, n_repetitions=10):
        self.n_jobs = n_jobs
        self.clf_name = clf_name
        self.n_estimators = n_estimators
        self.n_repetitions = n_repetitions

        # 4 metrics
        self.results = np.ndarray(shape=(self.n_repetitions, 4), dtype=float)
        self.confusion_matrices = np.ndarray(shape=(self.n_repetitions, ), dtype=np.ndarray)

        self.feature_labels = None
        if self.clf_name == "RF":
            # self.oob_scores = np.ndarray(shape=(self.n_folds,), dtype=float)
            self.importances = np.ndarray(shape=(self.n_repetitions,), dtype=np.ndarray)

        self.scaler_obj = None
