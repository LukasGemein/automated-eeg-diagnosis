from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
import numpy as np
import os


class AutosklPredictor(object):
    """
    """
# ______________________________________________________________________________________________________________________
    def create_classifier(self, out_dir):
        import autosklearn.classification
        # this was taken from auto-sklearn website and adapted
        tmp_folder = os.path.join(out_dir, 'autoskl', 'tmp')
        output_folder = os.path.join(out_dir, 'autoskl', 'output')

        clf = autosklearn.classification.AutoSklearnClassifier(
            time_left_for_this_task=self.total_budget,
            per_run_time_limit=self.run_budget,
            ml_memory_limit=4096,
            tmp_folder=tmp_folder,
            output_folder=output_folder,
            delete_tmp_folder_after_terminate=False,
            delete_output_folder_after_terminate=False,
        )
        return clf

# ______________________________________________________________________________________________________________________
    def evaluate_repetition(self, labels_test, predictions, repetition, n_classes):
        self.confusion_matrices[repetition] = confusion_matrix(labels_test, predictions, labels=np.arange(n_classes))

        # copute specificity if wanted/needed
        # [[tn, fp], [fn, tp]] = self.confusion_matrices[fold_id]
        # print(tn, fp, fn, tp)

        # evaluate fold predictions and store results
        self.results[repetition] = [
            accuracy_score(labels_test, predictions),
            precision_score(labels_test, predictions),
            recall_score(labels_test, predictions),
            f1_score(labels_test, predictions)
        ]

# ______________________________________________________________________________________________________________________
    def create_labels(self, feature_matrices):
        labels = []
        for class_id, feature_matrix in enumerate(feature_matrices):
            labels.extend(len(feature_matrix) * [class_id])
        return np.asarray(labels)

# ______________________________________________________________________________________________________________________
    def predict(self, feature_matrices_train, feature_matrices_eval):
        # create labels for training and evaluation: normal is 0 abnormal 1
        labels_train = self.create_labels(feature_matrices_train)
        labels_eval = self.create_labels(feature_matrices_eval)

        # stack matrices for usage in training/prediction step
        feature_matrix_train = np.vstack(feature_matrices_train)
        feature_matrix_eval = np.vstack(feature_matrices_eval)

        clf = self.create_classifier(self.output)
        for repetition in range(self.n_repetitions):
            clf.fit(feature_matrix_train, labels_train)
            predictions = clf.predict(feature_matrix_eval)
            prediction_probs = clf.predict_proba(feature_matrix_eval)

            self.evaluate_repetition(labels_eval, predictions, repetition, len(feature_matrices_train))
            print(clf.show_models())

        print(self.results)

        # write the results
        # self.write_results(cmd_args, class_names, len(feature_matrix_train), len(feature_matrix_eval), clf)

# ______________________________________________________________________________________________________________________
    def get_results(self):
        """ compute mean and std of cv fold metrics accuracy, precision, recall and f1-score """
        results = self.results * 100
        mean_results = np.mean(results, axis=0)
        std_results = np.std(results, axis=0)

        # 0: accs, 1: precs, 2: recs, 3: f1s
        return [results[:, 0], mean_results[0], std_results[0]], \
               [results[:, 1], mean_results[1], std_results[1]], \
               [results[:, 2], mean_results[2], std_results[2]], \
               [results[:, 3], mean_results[3], std_results[3]]

# ______________________________________________________________________________________________________________________
    def __init__(self, autoskl, clf_name='ASC', n_jobs=1, n_repetitions=1, output='./'):
        self.n_jobs = n_jobs
        self.clf_name = clf_name
        self.total_budget, self.run_budget = autoskl
        self.output = output

        # 4 metrics
        self.n_repetitions = n_repetitions
        self.results = np.ndarray(shape=(self.n_repetitions, 4), dtype=float)
        self.confusion_matrices = np.ndarray(shape=(self.n_repetitions, ), dtype=np.ndarray)
