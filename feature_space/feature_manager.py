import numpy as np

from utils import io, logger
from utils.io import JsonIO


class FeatureManager(object):
    """ the feature manager handles feature matrices and feature labels. loading them from file as well as picking
    subsets wrt. command line arguments
    """
# TODO: add exclude feats arg
# TODO: remove output arg
# ______________________________________________________________________________________________________________________
    def load_features(self, inputs, output):
        """ load individual domain features from file, assemble them in a feature matrix and prune that matrix wrt
        command line arguments s.t. subset of electrodes or features
        :param inputs: input directories
        :param output: output directory
        :return: a list of feature matrices for all classes, a list of class names, a list of recording names for all
        classes
        """
        # read feature labels from file
        if self.feature_labels is None:
            json_io = JsonIO()
            feature_labels_dict = json_io.load(output)
            feature_labels = []
            for domain in self.domains:
                feature_labels.extend(feature_labels_dict[domain])
            self.feature_labels = np.array(feature_labels)

        # load features and retrieve information whether time windows were agged as well as recording names
        class_names, feature_matrices = [], []
        for in_dir_id, in_dir in enumerate(inputs):
            feature_matrix, window_count = self.create_feature_matrix(in_dir)
            feature_matrices.append(feature_matrix)
            self.window_count = window_count

            # class names are now file names due to whatever reason
            class_names.append(in_dir.split('/')[-2])

        # prune feature matrices wrt cmd arguments
        feature_matrices = self.prune_feature_space_wrt_cmd_args(
            feature_matrices,
        )

        return feature_matrices, class_names

# ______________________________________________________________________________________________________________________
# TODO: only load a crop of the data for unagged features
    def create_feature_matrix(self, in_dir):
        """ Reads the features from files and arranges them.
        :param in_dir: the input directory
        :return: an array with the shape n_classes x n_recordings x n_features
        """
        logger.info("Reading from directory {}".format(in_dir))

        # get a list of domain hdf5 files in the specified directory
        file_reader = io.FileReader()
        hdf5_files = sorted(file_reader.read_all_file_names(in_dir, '.hdf5', key='natural'))
        if len(hdf5_files) == 0:
            logger.warning("Found no hdf5 files. Trying h5.")
            hdf5_files = sorted(file_reader.read_all_file_names(in_dir, '.h5', key='natural'))

        h5_reader = io.Hdf5Reader()
        feature_matrix = []
        for hdf5_file in hdf5_files:
            splits = hdf5_file.split('/')[-1].split('_')
            if len(splits) == 7:
                class_name, train_or_test, domain, agged, crop_time, window_info, band_info = splits
            elif len(splits) == 6:
                class_name, domain, agged, crop_time, window_info, band_info = splits
            # if the read domain hdf5 should not be read, skip it
            if domain not in self.domains:
                logger.info("Domain {} was not selected. Skipping.".format(domain))
                # if the domain was not specified via cmd args, skip it and prune the respective feature labels
                if self.feature_labels is not None:
                    self.feature_labels = np.asarray([
                        feature_label 
                        for feature_label in self.feature_labels
                        if not feature_label.startswith(domain)
                    ])
                continue

            logger.info("{}".format(hdf5_file.split('/')[-1]))
            # only read a cropped part (specified in s, here converted to number of time windows) of unagged files 
            if agged in ["none", "None", None] and not self.window_count:
                window_type, window_size, t_overlap = window_info.split('-')
                if self.crop:
                    self.window_count = int(self.crop/float(window_size[:-1]))
                else:
                    # TODO: -60 is the starttime shift hardcoded in here. FIX THIS
                    self.window_count = int((int(crop_time[:-1])-60)/float(window_size[:-1]))

            # patient features cannot be cropped since they don't have a time-based structure
            if domain != "patient":
                crop = self.window_count
            else:
                crop = None
            # data has shape n_files x n_windows x ... if unagged else n_files x ...
            data = h5_reader.read_domain_hdf5(hdf5_file, crop=crop)
            
            # already agged
            if agged not in ["none", "None", None]:
                data = np.reshape(data, (len(data), -1))

            # not yet agged
            else:  # agged in ["none", "None", None]
                # agging
                if self.agg not in ["none", "None", None]:
                    # do not agg patient features
                    if domain != "patient":
                        data = self.agg(data, axis=1)
                    # bring all domain feature matrices to shape n_files x -1, s.t. they can be combined to a whole
                    data = np.reshape(data, (len(data), -1))

                # no agging
                elif self.agg in ["none", "None", None]:
                    # patient features age and gender do not have a time-based structure
                    if domain == "patient":
                        n_files, n_features = data.shape
                        # fake time-based structure for age and gender
                        data = np.repeat(data, self.window_count, axis=0)
                        data = np.reshape(data, newshape=(n_files, self.window_count, n_features))

                    (n_files, n_windows) = data.shape[:2]
                    # bring all unagged domain feature matrices to shape n_files * n_windows x -1, s.t. they can be combined
                    data = np.reshape(data, (n_files*n_windows, -1))

            # add the (reshaped) data to feature matrix
            feature_matrix.append(data)
         
        return np.hstack(feature_matrix), self.window_count


# ______________________________________________________________________________________________________________________
    def take_subset_of_feats(self, feature_matrices, subsets, split_id):
        """ the labels are arranged as follows: <domain>_<feature>_<freq band>_<electrode>
        :param feature_matrices: list of feature matrices with all features that were computed in the preprocessing
        :param subsets: can be either domains, or features or electrodes
        :param split_id: the location where domain, feature or electrode can be found in the feature name
        :return: reduced list of features as specified by subsets. selected features are restored by self.feature_labels
        """
        if 'all' in subsets or (split_id == -1 and len(subsets) == 21):
            return feature_matrices
        else:
            # if doing electrodes (-1) make sure the given query matches the internal name convention (upper case)
            if split_id == -1:
                subsets = [subset.upper() for subset in subsets]

            new_feature_label_ids = []
            for subset in subsets:
                for feature_label_id, feature_label in enumerate(self.feature_labels):
                    # elecs
                    if split_id == -1:
                        if subset in feature_label.split('_')[split_id]:
                            new_feature_label_ids.append(feature_label_id)
                    # feats
                    elif split_id == 1:
                        # do regex?: ^[A-Za-z]*_([A-Za-z-]*)
                        if subset in feature_label.split('_'):
                            new_feature_label_ids.append(feature_label_id)

            # only pick those columns from the features that correspond to the subset of features / electrodes
            if not new_feature_label_ids:
                return feature_matrices

            new_feature_label_ids = np.array(sorted(np.unique(new_feature_label_ids)))
            for feature_matrix_id in range(len(feature_matrices)):
                feature_matrices[feature_matrix_id] = feature_matrices[feature_matrix_id][:, new_feature_label_ids]

            # also adapt the feature_labels
            self.feature_labels = self.feature_labels[new_feature_label_ids]
            return feature_matrices

# ______________________________________________________________________________________________________________________
    def prune_feature_space_wrt_cmd_args(self, feature_matrices):
        """ prune feature space wrt to command line arguments such as subset of electrodes or features
        :param feature_matrices: list of ndarrays of features of individual classes
        :return: a list of feature matrices with pruned feature space
        """
        # picks subsets of the feature matrix as specified by the feats cmd argument
        feature_matrices = self.take_subset_of_feats(feature_matrices, self.feats, 1)
        # picks subsets of the feature matrix as specified by the elecs cmd argument
        feature_matrices = self.take_subset_of_feats(feature_matrices, self.elecs, -1)
        return feature_matrices

# ______________________________________________________________________________________________________________________
    def get_feature_labels(self):
        return self.feature_labels

# ______________________________________________________________________________________________________________________
    def __init__(self, agg='median', elecs='all', feats='all', domains='all', crop=None):
        self.feature_labels = None
        self.elecs = elecs
        self.feats = feats
        self.domains = domains
        self.window_count = None
        self.crop = crop

        self.agg = getattr(np, agg) if agg not in ["none", "None", None] else agg
