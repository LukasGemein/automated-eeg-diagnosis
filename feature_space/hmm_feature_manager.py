import numpy as np
import logging

from utils import io


class HMMFeatureManager(object):
    """
    """

# ______________________________________________________________________________________________________________________
    def retrieve_window_counts(self, inputs):
        # if available read the window counts from file
        window_counts, min_n_windows = [], []
        for in_dir_id, in_dir in enumerate(inputs):
            window_counts.append([])
            window_counts[-1] = io.read_window_counts(in_dir)

            if window_counts[-1] is None:
                return None, None

            min_n_windows.append(np.min(window_counts[-1]))
        min_n_windows = np.min(min_n_windows)

        self.window_counts, self.min_n_windows = window_counts, min_n_windows
        return window_counts, min_n_windows

# ______________________________________________________________________________________________________________________
    def build_feature_matrix(self, per_domain_data, domains):
        """ take the features
        :param per_domain_data: features per domain
        :param domains: the domains
        :return: a feature matrix that either has agged features
                 or still not agged features together with the number of time windows per recording which is needed to
                 combine the not agged features in a training matrix
        """
        # from individual domain matrices, build per recording feature matrices/vectors
        n_files, n_domains = per_domain_data.shape
        feature_matrix = np.ndarray(shape=(n_files,), dtype=np.ndarray)

        if self.agged != 'none' or self.agg != 'none':
            logging.error("Agging does not make sense with HMMs.")
            exit()

        logging.debug("agged {} agg {}".format(self.agg, self.agg))

        for rec_id, rec_data in enumerate(per_domain_data):
            feature_vector = []
            for domain_id, domain_data in enumerate(rec_data):
                logging.debug("read shapes {}".format(per_domain_data[rec_id][domain_id].shape))
                logging.debug(domains[domain_id])

                if domains[domain_id] == 'patient':
                    logging.warning("Patient domain does not have a time structure")
                    break

                # keep the window dimension
                per_domain_data[rec_id][domain_id] = per_domain_data[rec_id][domain_id].reshape(
                    per_domain_data[rec_id][domain_id].shape[0], -1
                )

                logging.debug("agged or non agged shape {}".format(per_domain_data[rec_id][domain_id].shape))
                feature_vector.append(per_domain_data[rec_id][domain_id])

            feature_matrix[rec_id] = np.hstack(feature_vector)
            logging.debug(feature_matrix[rec_id].shape)

        # make this shape n_filex x n_windows x_features.
        # n_features are the feature of all domains concatenated
        feature_matrix = np.stack(feature_matrix)
        logging.debug(feature_matrix.shape)

        return feature_matrix

# ______________________________________________________________________________________________________________________
    def take_subset_of_feats(self, feature_matrices, subsets, split_id):
        """ the labels are arranged as follows: <domain>_<feature>_<freq band>_<electrode>
        :param feature_matrices: list of feature matrices with all features that were computed in the preprocessing
        :param subsets: can be either domains, or features or electrodes
        :param split_id: the location where domain, feature or electrode can be found in the feature name
        :return: reduced list of features as specified by subsets. selected features are restored by self.feature_labels
        """
        if 'all' in subsets:
            return feature_matrices
        else:
            # if doing electrodes make sure the given query matches the internal name convention (upper case)
            if split_id == -1:
                subsets = [subset.upper() for subset in subsets]

            new_feature_label_ids = []
            for subset in subsets:
                for feature_label_id, feature_label in enumerate(self.feature_labels):
                    if split_id == -1:
                        if subset in feature_label.split('_')[split_id]:
                            new_feature_label_ids.append(feature_label_id)
                    elif split_id == 1:
                        # do regex?: ^[A-Za-z]*_([A-Za-z-]*)
                        if subset in feature_label.split('_'):
                            new_feature_label_ids.append(feature_label_id)

            # only pick those columns from the features that correspond to the subset of features / electrodes
            if not new_feature_label_ids:
                return feature_matrices

            new_feature_label_ids = np.asarray(sorted(np.unique(new_feature_label_ids)))
            for feature_matrix_id in range(len(feature_matrices)):
                # ! feature_matrices here have another dimension since they were not agged
                feature_matrices[feature_matrix_id] = feature_matrices[feature_matrix_id][:, :, new_feature_label_ids]
                logging.debug("after taking subset of feats {}".format(feature_matrices[feature_matrix_id].shape))

            # also adapt the features_labels
            self.feature_labels = self.feature_labels[new_feature_label_ids]
            return feature_matrices

# ______________________________________________________________________________________________________________________
    def prune_feature_space_wrt_cmd_args(self, feature_matrices, feats, elecs):
        # picks the subsets of the feature matrix as specified by the domain cmd argument
        # feature_matrix = self.take_subset_of_feats(feature_matrix, domain, 0)
        # picks the subsets of the feature matrix as specified by the feats cmd argument
        feature_matrices = self.take_subset_of_feats(feature_matrices, feats, 1)
        # picks the subsets of the feature matrix as specified by the elecs cmd argument
        feature_matrices = self.take_subset_of_feats(feature_matrices, elecs, -1)
        return feature_matrices

# ______________________________________________________________________________________________________________________
    def retrieve_data_from_feature_files(self, out_dir, in_dir, specified_domains):
        """ Reads the features from files and arranges them.
        :param out_dir: the directory where the hdf5 files were originally written to
        :param in_dir:
        :param specified_domains:
        :return: an array with the shape n_classes x n_recordings x n_features
        """
        logging.info("\t\tReading from directory {}".format(in_dir))
        # read feature labels and recording names from file
        if self.feature_labels is None:
            self.feature_labels = io.read_feature_labels(out_dir)

        # only take a number of windows wrt cmd arguments which is effectively taking a cut of the original signal
        # and computing features only on that part
        if self.window_limit:
            self.min_n_windows = min(self.min_n_windows, self.window_limit)
            logging.info("\t\t\tWill only use {} windows".format(self.min_n_windows))

        recording_names = io.read_recording_names2(in_dir)
        # get a list of hdf5 files in the specified directory
        hdf5_files = sorted(io.read_all_file_names(in_dir, '.hdf5', key='natural'))
        n_files, n_domains = len(recording_names), min(len(specified_domains), len(hdf5_files))

        domains = []
        domain_id = 0
        per_domain_data = np.ndarray(shape=(n_files, n_domains,), dtype=np.ndarray)
        for hdf5_file in hdf5_files:
            class_name, domain, agged, dummy, window_info, dummy = hdf5_file.split('/')[-1].split('_')
            window_type, window_size, overlap_size = window_info.split('-')
            self.window_size = float(window_size[:-1])
            self.overlap_size = float(overlap_size[:-1])

            if self.agged is None:
                self.agged = agged

            # if the domain was specified through cmd_args, read it from file
            # if not, skip it and prune the respective feature labels
            if domain not in specified_domains:
                self.feature_labels = np.asarray([feature_label for feature_label in self.feature_labels
                                                  if not feature_label.startswith(domain)])
                continue

            logging.info("\t\t\t{}".format(hdf5_file.split('/')[-1]))
            data = io.read_domain_hdf5(hdf5_file, self.min_n_windows)

            per_domain_data[:, domain_id] = data
            domains.append(domain)
            domain_id += 1

        return class_name, domains, per_domain_data, recording_names

# ______________________________________________________________________________________________________________________
    def get_feature_labels(self):
        return self.feature_labels

# ______________________________________________________________________________________________________________________
    def __init__(self, agg='median', window_limit=None):
        self.feature_labels = None

        self.agged = None
        self.agg = getattr(np, agg) if (agg != 'none' and agg != 'None') else agg
        self.more_samples = False

        self.window_size = None
        self.overlap_size = None

        self.window_counts = None
        self.min_n_windows = None

        self.window_limit = window_limit
