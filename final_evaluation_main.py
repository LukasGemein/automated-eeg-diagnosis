#!/usr/bin/env python3.5
from datetime import datetime, date

from utils import arg_parser, logger
import pipeline


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    today, now = date.today(), datetime.time(datetime.now())
    logger.create_loggers(cmd_args.output, cmd_args.verbosity)
    logger.info('\tStarted on {} at {}.'.format(today, now))

    # trainer_type None implies that this is not a CV run but a final evaluation run
    pl = pipeline.Pipeline(trainer_type=None)
    pl.run(cmd_args)

    logger.info('\tFinished on {} at {}.\n\n'.format(date.today(), datetime.time(datetime.now())))


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    parser = arg_parser.ArgParser()
    cmd_arguments, unknown = parser.parse_known_args()
    main(cmd_arguments, unknown)
