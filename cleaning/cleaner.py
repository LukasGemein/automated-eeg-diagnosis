from abc import ABC, abstractmethod
import numpy as np
import unittest
import mne

# TODO: create several cleaners and potentially apply them in order
# TODO: can different montages improve classifier performance / detection of abnormalities?

ELECTRODES = sorted([
    'FP1', 'FP2', 'F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7',
    'F8', 'T3', 'T4', 'T5', 'T6', 'A1', 'A2', 'FZ', 'CZ', 'PZ',
])


# class AbstractCleaner(ABC):
#     @abstractmethod
#     def clean(self, raw):
#         raise NotImplementedError
#
#
# class CleanerCollection(AbstractCleaner):
#     def __init__(self, cleaners):
#         self.cleaners = cleaners
#
#     def clean(self, raw):
#         for cleaner in self.cleaners:
#             raw = cleaner.clean(raw)
#         return raw
#
#
# class ChannelPicker(AbstractCleaner):
#     def __init__(self):
#         self.chs = sorted([
#             'FP1', 'FP2', 'F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7',
#             'F8', 'T3', 'T4', 'T5', 'T6', 'A1', 'A2', 'FZ', 'CZ', 'PZ',
#         ])
#
#     def clean(self, raw, chs):
#         self.pick_subset_of_chs(chs)
#         # select international 10-20 placement electrodes
#         channel_names = [self.full_name(ch_name) for ch_name in self.chs]
#         raw.pick_channels(channel_names)
#         self.rename_channels(raw, channel_names)
#         return raw
#
#     def pick_subset_of_chs(self, chs):
#         """ pick channels as specified by cmd argument """
#         if 'all' in chs:
#             return self.chs
#         else:
#             # transform to upper case
#             chs = [ch.upper() for ch in chs]
#             # check which electrodes were picked by cmd arg
#             cmd_elecs = [standard_ch for ch in chs for standard_ch in self.chs if ch in standard_ch]
#             return np.unique(cmd_elecs)
#
#     @staticmethod
#     def full_name(short_ch_name):
#         """ recreate full ch name present in the recordings starting from short names """
#         return "{}{}{}".format("EEG ", short_ch_name, "-REF")
#
#     def rename_channels(self, raw, channel_names):
#         """ abbreviate long ch names present in the recordings """
#         mapping = {}
#         for i, electrode in enumerate(channel_names):
#             mapping.update({electrode: self.chs[i]})
#         raw.rename_channels(mapping)
#
#
# class StartTimeMover(AbstractCleaner):
#     def __init__(self, start_time):
#         self.start_time = start_time
#
#     def clean(self, raw):
#         raw.crop(self.start_time)
#         return raw
#
#
# class StopTimeMover(AbstractCleaner):
#     def __init__(self, stop_time):
#         self.stop_time = stop_time
#
#     def clean(self, raw):
#         raw.crop(self.stop_time)
#         return raw
#
#
# class Resampler(AbstractCleaner):
#     def __init__(self, resample_freq):
#         self.resample_freq = resample_freq
#
#     def clean(self, raw):
#         # if the eeg was not recorded with 250Hz resample it to 250Hz
#         if raw.info["sfreq"] != self.resample_freq:
#             raw.resample(self.resample_freq)
#         return raw
#
#
# class PowerLineFreqRemoved(AbstractCleaner):
#     def __init__(self, power_line_freq=60):
#         self.power_line_freq = power_line_freq
#
#     def clean(self, raw):
#         # remove power line frequency
#         raw.notch_filter(
#             np.arange(self.power_line_freq, 121, self.power_line_freq),
#             fir_design="firwin"
#         )
#         return raw
#
#
# class ValueClipper(AbstractCleaner):
#     def __init__(self, clip_value):
#         self.clip_value = clip_value
#
#     def clean(self, raw):
#         # convert to microvolts and clip the values to -800, +800
#         signals = raw.get_data() * 1E+06
#         clipped_signals = self.clip_values(signals)
#         raw = mne.io.RawArray(clipped_signals, raw.info, verbose='error')
#         return raw
#
#     def clip_values(self, signals):
#         """ sth is an outlier if it shows more than 800 or less than -800 microvolts amplitude. clip those outliers to
#         (-800, 800) """
#         # NOTE: the values cannot be exact 800 or -800 since feature computation crashes (reason and position unclear)
#         # that is why a small amount of noise is added/subtracted to/from the clipping value
#         n = len(signals[signals > self.clip_value])
#         noise = np.random.rand(n)
#         signals[signals > self.clip_value] = self.clip_value
#         signals[signals == self.clip_value] -= noise
#
#         n2 = len(signals[signals < -self.clip_value])
#         noise2 = np.random.rand(n2)
#         signals[signals < -self.clip_value] = -self.clip_value
#         signals[signals == -self.clip_value] += noise2
#
#         return signals
#
#
# class BandFilterer(AbstractCleaner):
#     def __init__(self):
#         pass
#
#     def clean(self, raw, lower, upper):
#         """ use mne filter to filter to a specified frequency band """
#         raw.filter(
#             lower,
#             upper
#         )
#         return raw


class Cleaner(object):
    """
    """
# ______________________________________________________________________________________________________________________
    def full_name(self, short_ch_name):
        """ recreate full ch name present in the recordings starting from short names """
        return "{}{}{}".format("EEG ", short_ch_name, "-REF")

# ______________________________________________________________________________________________________________________
    def pick_subset_of_channels(self, chs):
        """ pick channels as specified by cmd argument """
        if 'all' in chs:
            return ELECTRODES
        else:
            # transform to upper case
            chs = [ch.upper() for ch in chs]
            # check which electrodes were picked by cmd arg
            cmd_elecs = [standard_ch for ch in chs for standard_ch in ELECTRODES if ch in standard_ch]
            return np.unique(cmd_elecs)

# ______________________________________________________________________________________________________________________
    def band_filter_to_specific_band(self, signals, lower, upper):
        """ use mne filter to filter the frequency bands for synchronicity features """
        band_filtered_signal = mne.filter.filter_data(
            data=signals,
            sfreq=self.resample_frequency,
            l_freq=lower,
            h_freq=upper,
            verbose='error',
        )
        return band_filtered_signal

# ______________________________________________________________________________________________________________________
    def band_filter_to_specific_band2(self, raw, lower, upper):
        raw.filter(lower, upper)
        return raw

# ______________________________________________________________________________________________________________________
    def filter_to_bands(self, signals, bands):
        """ filter signals to frequency ranges defined in bands """
        signals = signals.astype(np.float64)
        (n_signals, n_times) = signals.shape
        band_signals = np.ndarray(
            shape=(len(bands), n_signals, n_times)
        )
        for band_id, band in enumerate(bands):
            lower, upper = band
            curr_band_signals = self.band_filter_to_specific_band(
                signals=signals,
                lower=lower,
                upper=upper
            )
            band_signals[band_id] = curr_band_signals
        return band_signals

# ______________________________________________________________________________________________________________________
    def clip_values(self, signals):
        """ sth is an outlier if it shows more than 800 or less than -800 microvolts amplitude. clip those outliers to
        (-800, 800) """
        # TODO: re-check if this can be exactly +/- 800
        # NOTE: the values cannot be exact 800 or -800 since feature computation crashes (reason and position unclear)
        # that is why a small amount of noise is added/subtracted to/from the clipping value
        n = len(signals[signals > self.clip_value])
        noise = np.random.rand(n)
        signals[signals > self.clip_value] = self.clip_value
        signals[signals == self.clip_value] -= noise

        n2 = len(signals[signals < -self.clip_value])
        noise2 = np.random.rand(n2)
        signals[signals < -self.clip_value] = -self.clip_value
        signals[signals == -self.clip_value] += noise2

        return signals

# ______________________________________________________________________________________________________________________
    def rename_channels(self, raw, channel_names):
        """ abbreviate long ch names present in the recordings """
        mapping = {}
        for i, electrode in enumerate(channel_names):
            mapping.update({electrode: ELECTRODES[i]})
        raw.rename_channels(mapping)

# ______________________________________________________________________________________________________________________
    def clean(self, edf_file):
        """ apply simple cleaning rules: pick ch, rename ch, remove start, take until cut time, resample if necessary,
        apply notch filter
        :param edf_file: path to edf file
        :return: cleaned raw
        """
        # use mne to read edf file as instance of raw. preload data to alter it in place
        raw = mne.io.read_raw_edf(edf_file, preload=True, verbose='error')

        # select international 10-20 placement electrodes
        channel_names = [self.full_name(ch_name) for ch_name in ELECTRODES]
        raw.pick_channels(channel_names)

        # abbreviate the channel names by A1, O2, CZ, ... and pick channels wrt cmd args
        self.rename_channels(raw, channel_names)
        raw.pick_channels(self.elecs)

        # remove start time from recording and pick to cut_time (default: None -> take everything until the end)
        raw.crop(self.start_time_shift, self.cut_time)

        # if the eeg was not recorded with 250Hz resample it to 250Hz
        if raw.info["sfreq"] - self.resample_frequency > 0.001:
            raw.resample(self.resample_frequency)

        # remove power line frequency
        raw.notch_filter(np.arange(self.power_line_freq, 121, self.power_line_freq), fir_design="firwin")

        # can be used for plotting
        # raw.set_montage(mne.channels.read_montage("standard_1020", ch_names=raw.info.ch_names))

        # convert to microvolts and clip the values to -800, +800
        signals = raw.get_data() * 1E+06

        # assure that number of samples is a multiple of sampling frequency
        # if not, that causes weird behavior in computation of plv, hilbert transform/fourier transform
        # see https: // github.com / scipy / scipy / issues / 6324
        (n_channels, n_times) = signals.shape
        if n_times % self.resample_frequency != 0:
            signals = signals[:, :int(int(n_times/self.resample_frequency)*self.resample_frequency)]

        signals = self.clip_values(signals)

        raw = mne.io.RawArray(signals, raw.info, verbose='error')

        return raw

# ______________________________________________________________________________________________________________________
    def get_bands(self, bands=None):
        """ splits cmd_arg bands in upper and lower limits of band. if band overlap adds 50% band overlap """
        if bands:
            bands = [int(digit) for digit in bands]
            new_bands = []
            for i in range(len(bands)-1):
                new_bands.append([bands[i], bands[i+1]])
                if self.band_overlap and i != len(bands)-2:
                    new_bands.append([int((bands[i]+bands[i+1])/2),
                                      int((bands[i+1]+bands[i+2])/2)])
            self.bands = new_bands
        return self.bands

# ______________________________________________________________________________________________________________________
    def __init__(self, bands, start_time_shift=60, clip_value=800, power_line_freq=60, cut_time=None,
                 elecs=['all'], band_overlap=False, resample_frequency=250):

        self.start_time_shift = start_time_shift
        self.cut_time = cut_time

        self.resample_frequency = resample_frequency

        self.clip_value = clip_value
        self.power_line_freq = power_line_freq

        self.elecs = self.pick_subset_of_channels(elecs)

        self.band_overlap = band_overlap
        self.bands = self.get_bands(bands)
