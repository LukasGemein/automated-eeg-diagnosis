import numpy as np
import re

from prediction import predictor, autoskl_predictor
from feature_space import feature_manager
from preprocessing import preprocessor
from visualization import plotting
from utils import io, logger


# TODO: maybe split this up into 3 parts? preprocessing / cv / evaluation?

class Pipeline(object):
    """
    """
# ______________________________________________________________________________________________________________________
    def perform_cross_validation(self, cmd_args):
        fm = feature_manager.FeatureManager(
            agg=cmd_args.agg,
            elecs=cmd_args.elecs,
            feats=cmd_args.feats,
            domains=cmd_args.domains
        )
        feature_matrices, class_names = fm.load_features(
            cmd_args.input,
            cmd_args.output,
        )
        self.trainer.train(feature_matrices)
        # results = self.trainer.get_results()
        # logger.log_results(results)

        # upload the results to google spreadsheet
        if not cmd_args.no_up:
            io.write_to_google_result_tracking(
                class_names,
                [len(feature_matrices[0]), len(feature_matrices[1])],
                cmd_args.agg,
                cmd_args.window,
                cmd_args.windowsize,
                cmd_args.t_overlap,
                self.trainer.clf_name,
                self.trainer.n_estimators,
                cmd_args.domains,
                cmd_args.feats,
                cmd_args.elecs,
                feature_matrices[0].shape[1],
                cmd_args.bands,
                cmd_args.f_overlap,
                str(self.trainer.scaler_obj).split('(')[0],
                self.trainer.n_folds,
                '',
                results,
                '',
                ' '.join(re.split('\s{2,}', str(self.trainer.clf))),
                location=cmd_args.output,
                no_up=cmd_args.no_up,
            )

        # write the results of cv
        io.write_results(cmd_args.output, cmd_args.input, results)

# ______________________________________________________________________________________________________________________
    def perform_final_evaluation(self, cmd_args):
        # make sure the number of inputs is correct and in expected order
        assert len(cmd_args.input) == 4, logger.error(
            "Incorrect number of directories. Expecting two train followed by two eval dirs"
        )
        train_dirs = cmd_args.input[:2]
        for train_dir in train_dirs:
            if "train" not in train_dir:
                logger.error("This seems to be the wrong training directory {}".format(train_dir))
                exit()
        eval_dirs = cmd_args.input[2:]
        for eval_dir in eval_dirs:
            if "eval" not in eval_dir:
                logger.error("This seems to be the wrong evaluation directory {}".format(eval_dir))
                exit()

        # load train and eval data
        fm = feature_manager.FeatureManager(
            elecs=cmd_args.elecs,
            feats=cmd_args.feats,
            domains=cmd_args.domains,
            agg=cmd_args.agg
        )
        feature_matrices_train, class_names = fm.load_features(
            train_dirs,
            cmd_args.output,
        )
        feature_matrices_eval, class_names = fm.load_features(
            eval_dirs,
            cmd_args.output,
        )

        # use sklearn or autosklearn for final evaluation
        self.predictor.predict(
            feature_matrices_train,
            feature_matrices_eval
        )
        results = self.predictor.get_results()
        logger.log_results(results)

        # upload the results to google spreadsheet
        # if not cmd_args.no_up:
        #     io.write_to_google_result_tracking(
        #         class_names,
        #         [len(feature_matrices[0]), len(feature_matrices[1])],
        #         cmd_args.agg,
        #         cmd_args.window,
        #         cmd_args.windowsize,
        #         cmd_args.t_overlap,
        #         self.trainer.clf_name,
        #         self.trainer.n_estimators,
        #         cmd_args.domains,
        #         cmd_args.feats,
        #         cmd_args.elecs,
        #         feature_matrices[0].shape[1],
        #         cmd_args.bands,
        #         cmd_args.f_overlap,
        #         str(self.trainer.scaler_obj).split('(')[0],
        #         self.trainer.n_folds,
        #         '',
        #         results,
        #         '',
        #         ' '.join(re.split('\s{2,}', str(self.trainer.clf))),
        #         location=cmd_args.output,
        #         no_up=cmd_args.no_up,
        #     )

        # write the results
        # io.write_results(cmd_args.output, cmd_args.input, results)

# ______________________________________________________________________________________________________________________
    def perform_visualization(self, cmd_args, class_names, feature_labels=None):
        # visualize the results
        if 'all' in cmd_args.visualize or 'train' in cmd_args.visualize:
            # visualize test results
            plotting.plot_cv_results(cmd_args.output, self.trainer.n_folds, self.trainer.get_results(), title_="test")
            self.trainer.confusion_matrices = np.stack(self.trainer.confusion_matrices)
            avg_confusion_matrix = np.mean(self.trainer.confusion_matrices, axis=0)
            plotting.plot_confusion_matrix(cmd_args.output, avg_confusion_matrix, classes=class_names, title_="test")

            # visualize train results
            # plotting.plot_cv_results(cmd_args.output, self.n_folds, self.get_results(train=True), title_="train")
            # self.confusion_matrices_train = np.stack(self.confusion_matrices_train)
            # avg_confusion_matrix_train = np.mean(self.confusion_matrices_train, axis=0)
            # plotting.plot_confusion_matrix(cmd_args.output, avg_confusion_matrix_train, classes=class_names,
            #                                title_="train")

            if self.trainer.clf_name == 'RF':
                # plotting.plot_oob_scores(cmd_args.output, self.oob_scores, self.n_estimators)

                if len(self.trainer.importances[0]) == len(feature_labels):
                    plotting.plot_feature_importances(cmd_args.output, self.trainer.importances, feature_labels)
                    plotting.plot_feature_importances_spatial(cmd_args.output, self.trainer.importances, feature_labels)

# ______________________________________________________________________________________________________________________
    def run(self, cmd_args):
        """ run the pipeline in different modes, e.g. preprocessing only, preprocessing + cv, cv only, evaluation """

# --------------------------------------------------- PREPROCESSING ---------------------------------------------------#
        if self.trainer is not None:
            # run preprocessing if not disabled
            if not cmd_args.no_pre:
                """  
                create an output directory for the features following the naming convention: 
                class0_class1_agg-mode_cut-time_window-type_window-size_window-overlap_frequency-bands_band-overlap
                """
                cmd_args.output = io.assemble_outdir_name(cmd_args)
                io.check_out(cmd_args.output, cmd_args.input)

                logger.log_cmd_args(cmd_args)
                self.preprocessor = preprocessor.Preprocessor(cmd_args.n_proc)
                cmd_args.input = self.preprocessor.preprocess(cmd_args)

# ------------------------------------------------- CROSS-VALIDATION --------------------------------------------------#
                # run cross-validation if not disabled
                if not cmd_args.pre_only:
                    self.perform_cross_validation(
                        cmd_args=cmd_args
                    )

            # only run cross-validation if preprocessing is disabled
            else:
                # some cmd arguments can only be set during preprocessing. they are incoded in feature files.
                # restore them and log updated cmd args to console and file
                cmd_args = io.update_cmd_args(cmd_args)
                logger.log_cmd_args(cmd_args)

                self.perform_cross_validation(
                    cmd_args=cmd_args
                )

# ----------------------------------------------------- EVALUATION ----------------------------------------------------#
        # hacky for final evaluation. there, trainer is set to None
        else:
            if cmd_args.clf != "ASC":
                self.predictor = predictor.Predictor(
                    clf_name=cmd_args.clf,
                    n_jobs=cmd_args.n_proc,
                    n_estimators=250
                )
            else:
                self.predictor = autoskl_predictor.AutosklPredictor(
                    [int(b) for b in cmd_args.auto_skl],
                    n_jobs=cmd_args.n_proc,
                    output=cmd_args.output
                )
            self.perform_final_evaluation(
                cmd_args=cmd_args
            )

# ______________________________________________________________________________________________________________________
    def __init__(self, trainer_type):
        self.trainer = trainer_type
        self.preprocessor = None
        self.predictor = None
