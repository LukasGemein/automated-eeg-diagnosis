# README #

This repository is a mess. Sorry about that. Cleanup is in progress.

### What is this repository for? ###

This repository holds code that implements a machine learning pipeline with the purpose of doing pathological vs. non-pathological EEG recording classification. The data for this project originates from the [TUH Abnormal EEG Corpus](https://www.isip.piconepress.com/projects/tuh_eeg/html/downloads.shtml).

### How do I get set up? ###

Requirements  
\- python 3.5.x  
\- scikit-learn  
\- numpy  
\- mne  
\- pyeeg  
etc...  

Run with:  
python3 auto-eeg-main.py <path to normal train data> <path to abnormal train data> <output directory> [options]

There are many options that can be inspected using command line argument --help/-h. 

Further information is yet to come.