import numpy as np
import logging
import os


# TODO: include caller module name instead of module name in logging message


# ______________________________________________________________________________________________________________________
def create_loggers(output, verbosity):
    """ creates two logging handlers that write to console and to .log file respectively
    :param output: output directory
    :param verbosity: logging verbosity level from 0-50
    :return:
    """
    name = output.split('/')[-1]
    if name == '':
        name = output.split('/')[-2]
    logname = os.path.join(output, name + '.log')
    # formatter = logging.Formatter("[%(levelname)s] (%(module)s:%(lineno)d) %(message)s")
    formatter = logging.Formatter("[%(levelname)s] %(message)s")
    logging.basicConfig(level=verbosity)

    rootlogger = logging.getLogger()
    rootlogger.handlers = []

    # add a logger that logs to console
    streamhandler = logging.StreamHandler()
    streamhandler.setFormatter(formatter)
    rootlogger.addHandler(streamhandler)

    # add a logger that logs to file
    #filhandler = logging.FileHandler(logname)
    #filhandler.setFormatter(formatter)
    #rootlogger.addHandler(filhandler)


# ______________________________________________________________________________________________________________________
def log_cmd_args(cmd_args, module_name=''):
    """ logs the cmd arguments
    :param cmd_args: of type argparse.Namespace holding all cmd arguments and their value
    :return:
    """
    if module_name != "":
        module_name = '['+module_name+']'
    logging.info('{}\t\tWill use the following parameters ..'.format(module_name))
    indent = 15
    for arg in sorted(vars(cmd_args)):
        value = getattr(cmd_args, arg)
        if arg == "elecs":  # electrodes have leading spaces, remove them
            value = [elec.strip() for elec in value]
        if type(value) == list:
            if arg == "input":  # this is a list of directories which is too long to fit in one line. split it up
                for i, elem in enumerate(value):
                    if i == 0:
                        logging.info("{}\t\t{}:{}{},".format(module_name, arg, (indent - len(arg)) * ' ', elem))
                    else:
                        logging.info(("{}\t\t{}{}".format(module_name, (indent + 1) * ' ', elem)))
            else:
                value = [str(v) for v in value]
                logging.info("{}\t\t{}:{}{}".format(module_name, arg, (indent - len(arg)) * ' ', ', '.join(value)))
        else:
            logging.info("{}\t\t{}:{}{}".format(module_name, arg, (indent - len(arg)) * ' ', getattr(cmd_args, arg)))


# ______________________________________________________________________________________________________________________
def log_times(preprocessing_times_lists, generation_times_lists, module_name=""):
    """ logs the times needed to do preprocessing / feature generation
    :param preprocessing_times_lists: list of loading and clean times for processed edf files
    :param generation_times_lists: list of feature generation times for domains cwt, dwt, fft, patient, pyeeg, sync, time
    :return:
    """
    if module_name != "":
        module_name = '['+module_name+']'
    [load_times, clean_times] = preprocessing_times_lists
    logging.info("{}\t\tLoading takes in average: {:.3f}s".format(module_name, np.mean(load_times)))
    logging.info("{}\t\tCleaning takes in average: {:.3f}s".format(module_name, np.mean(clean_times)))

    [c_times, d_times, f_times, p_times, py_times, s_times, t_times] = generation_times_lists
    mean_times = [np.mean(times) for times in generation_times_lists if times]
    logging.info("{}\t\tGeneration takes in average: {:.3f}s".format(module_name, np.sum(mean_times)))
    if c_times:
        logging.info("{}\t\t\tCwt features take in average: {:.3f}s".format(module_name, np.mean(c_times)))
    if d_times:
        logging.info("{}\t\t\tDwt features take in average: {:.3f}s".format(module_name, np.mean(d_times)))
    if f_times:
        logging.info("{}\t\t\tFFT features take in average: {:.3f}s".format(module_name, np.mean(f_times)))
    if p_times:
        logging.info("{}\t\t\tPatient features take in average: {:.3f}s".format(module_name, np.mean(p_times)))
    if py_times:
        logging.info("{}\t\t\tPyeeg features take in average: {:.3f}s".format(module_name, np.mean(py_times)))
    if s_times:
        logging.info("{}\t\t\tSync features take in average: {:.3f}s".format(module_name, np.mean(s_times)))
    if t_times:
        logging.info("{}\t\t\tTime features take in average: {:.3f}s".format(module_name, np.mean(t_times)))


# ______________________________________________________________________________________________________________________
def log_results(results):
    logging.info('\t\t\tMean accuracy: {} ({})'.format(results[0][1], results[0][2]))
    logging.info('\t\t\tMean precision: {} ({})'.format(results[1][1], results[1][2]))
    logging.info('\t\t\tMean recall: {} ({})'.format(results[2][1], results[2][2]))
    logging.info('\t\t\tMean F1-score: {} ({})'.format(results[3][1], results[3][2]))


# ______________________________________________________________________________________________________________________
def debug(msg, module_name=""):
    if module_name != "":
        module_name = '['+module_name+']'
    logging.debug(module_name+msg)


# ______________________________________________________________________________________________________________________
def info(msg, module_name=""):
    if module_name != "":
        module_name = '[' + module_name + ']'
    logging.info(module_name+msg)


# ______________________________________________________________________________________________________________________
def warning(msg, module_name=""):
    if module_name != "":
        module_name = '[' + module_name + ']'
    logging.warning(module_name+msg)


# ______________________________________________________________________________________________________________________
def error(msg, module_name=""):
    if module_name != "":
        module_name = '[' + module_name + ']'
    logging.error(module_name+msg)
