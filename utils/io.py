import numpy as np
import logging
import h5py
import glob
import json
import csv
import mne
import os
import re

from utils import logger


class FileReader:
    # TODO: instead of building this index every time, store it somewhere and read it
    def natural_key(self, file_name):
        """ provides a human-like sorting key of a string """
        key = [int(token) if token.isdigit() else None for token in re.split(r'(\d+)', file_name)]
        return key

    def session_key(self, file_name):
        """ sort the file name by session """
        return re.findall(r'(s\d*)_', file_name)

    def time_key(self, file_name):
        """ provides a time-based sorting key """
        splits = file_name.split('/')
        [date] = re.findall(r'(\d{4}_\d{2}_\d{2})', splits[-2])
        date_id = [int(token) for token in date.split('_')]
        recording_id = self.natural_key(splits[-1])
        session_id = self.session_key(splits[-2])

        return date_id + session_id + recording_id

    def read_all_file_names(self, path, extension, key="time"):
        """ read all files with specified extension from given path
        :param path: parent directory holding the files directly or in subdirectories
        :param extension: the type of the file, e.g. '.txt' or '.edf'
        :param key: the sorting of the files. natural e.g. 1, 2, 12, 21 (machine 1, 12, 2, 21) or by time since this is
        important for cv. time is specified in the edf file names
        """
        file_paths = glob.glob(path + '**/*' + extension, recursive=True)
        if key == "time":
            sorting_key = self.time_key  
        else:
            sorting_key = self.natural_key
        file_names = sorted(file_paths, key=sorting_key)

        if len(file_names) == 0:
            logger.error("Something went wrong. Found no {} files in {}".format(extension, path))

        return file_names
    
    def read_stuff(self, path, key="natural", regex=".*"):
        file_names = []
        for file in os.listdir(path):
            if re.findall(
                pattern=regex,
                string=file
            ):
                file_names.append(os.path.join(path, file))
                
        if key == "time":
            sorting_key = self.time_key  
        else:
            sorting_key = self.natural_key
        file_names = sorted(file_names, key=sorting_key)

        if len(file_names) == 0:
            logger.error("Something went wrong. Found no files in {}".format(path))

        return file_names

class EdfLoader:
    def parse_sex_and_age(self, file_path):
        """ parse sex and age of patient from the patient_id in the header of the edf file
        :param file_path: path of the recording
        :return: sex (0=M, 1=F) and age of patient
        """
        f = open(file_path, 'rb')
        content = f.read(88)
        f.close()

        patient_id = content[8:88].decode('ascii')

        [age] = re.findall("Age:(\d+)", patient_id)
        [sex] = re.findall("\s(\w)\s", patient_id)

        if sex == 'M':
            sex_id = 0
        else:  # sex == 'F' or sex == 'X'
            sex_id = 1

        return np.array([abs(int(age)), sex_id])

    def get_info_with_mne(self, file_path):
        """ read info from the edf file without loading the data. loading data is done in multiprocessing since it takes
        some time. getting info is done before because some files had corrupted headers or weird sampling frequencies
        that caused the multiprocessing workers to crash. therefore get and check e.g. sampling frequency and duration
        beforehand
        :param file_path: path of the recording file
        :return: file name, sampling frequency, number of samples, number of signals, signal names, duration of the rec
        """
        edf_file = mne.io.read_raw_edf(file_path, verbose='error')

        # some recordings have a very weird sampling frequency. check twice before skipping the file
        sampling_frequency = int(edf_file.info['sfreq'])
        n_samples = edf_file.n_times
        signal_names = edf_file.ch_names
        n_signals = len(signal_names)
        # some weird sampling frequencies are at 1 hz or below, which results in division by zero
        duration = n_samples / max(sampling_frequency, 1)

        return edf_file, sampling_frequency, n_samples, n_signals, signal_names, duration

    def read_raw_edf(self, file_path):
        return mne.io.read_raw_edf(file_path, verbose='error', preload=True)


class Hdf5Writer:
    def write_hdf5(self, features, in_dir, cmd_args):
        """ writes features to hdf5 file
        :param features: a matrix holding a feature vector for every recordings (maybe someday for every time window of
        every recording)
        :param in_dir: input directory used to extract the name if the class
        :param cmd_args: used to include important information in the file name s.a. window, windowsize etc
        :return: the name of the feature file
        """
        dir_splits = in_dir.split('/')
        data_set = dir_splits[-5]
        train_or_eval = dir_splits[-2]

        file_name = os.path.join(cmd_args.output, data_set, train_or_eval,
                                 '_'.join([data_set, '-'.join([cmd_args.window,
                                                               str(cmd_args.windowsize)+'s',
                                                               str(cmd_args.overlap)+'%']),
                                           cmd_args.bands.replace(',', '-')])) + '.hdf5'

        logging.info("\t\tWriting features to {}.\n".format(file_name))

        hdf5_file = h5py.File(file_name, 'w')
        hdf5_file.create_dataset('data', features.shape, data=features)
        hdf5_file.close()

        return file_name

    def create_hdf5_file(self, in_dir, cmd_args, domain):
        """
        :param in_dir:
        :param cmd_args:
        :param domain:
        :return:
        """
        dir_splits = in_dir.split('/')
        data_set = dir_splits[-5]
        train_or_eval = dir_splits[-2]
        cut = str(cmd_args.cut) + 's' if cmd_args.cut != 'none' else cmd_args.cut

        directory = os.path.join(cmd_args.output, data_set, train_or_eval,)
        # creates names of the pattern:
        #   normalv1.1.1_fft_120s_boxcar-2s_0-4-8-13-18-24-30-60-120-False.hdf5
        file_name = os.path.join(
            directory,
            '_'.join([data_set,
                      domain,
                      cmd_args.agg,
                      cut,
                      '-'.join(
                          [
                              cmd_args.window,
                              str(cmd_args.windowsize)+'s',
                              str(cmd_args.t_overlap)+'%'
                          ]
                      ),
                      '-'.join(['-'.join(cmd_args.bands),
                                str(cmd_args.f_overlap) + '.hdf5'])
                      ]))

        if os.path.exists(file_name):
            os.remove(file_name)

        return file_name

    def write_domain_hdf5_successively(self, file_path, features, rec_id):
        """ writes features to hdf5 file
        :param: file_path: the path where to write the features to
        :param features: a matrix holding a feature vector for every recordings (maybe someday for every time window of
        every recording)
        :param rec_id: the time-based id of the recording
        :return: the name of the feature file
        """
        file_path = os.path.join(self.working_dir, file_path)
        logger.debug("{}".format(features.shape))
        hdf5_file = h5py.File(file_path, 'a')
        try:
            hdf5_file.create_dataset(str(rec_id), features.shape, data=features)
        except RuntimeError:
            logger.error("Could not write features, entry {} already exists in {}".format(rec_id, file_path))
        hdf5_file.close()

        splits = file_path.split('/')[:-1]
        return '/'.join(splits)

    def write_domain_hdf5(self, features, in_dir, cmd_args, domain):
        """ writes features to hdf5 file
        :param features: a matrix holding a feature vector for every recordings (maybe someday for every time window of
        every recording)
        :param in_dir: input directory used to extract the name if the class
        :param cmd_args: used to include important information in the file name s.a. window, windowsize etc
        :return: the name of the feature file
        """
        dir_splits = in_dir.split('/')
        data_set = dir_splits[-5]
        train_or_eval = dir_splits[-2]
        cut = str(cmd_args.cut) + 's' if cmd_args.cut != 'none' else cmd_args.cut

        directory = os.path.join(cmd_args.output, data_set, train_or_eval,)
        # creates names of the pattern:
        #   normalv1.1.1_fft_120s_boxcar-2s_0-4-8-13-18-24-30-60-120-False.hdf5
        file_name = os.path.join(
            directory,
            '_'.join([data_set,
                      domain,
                      cmd_args.agg,
                      cut,
                      '-'.join(
                          [
                              cmd_args.window,
                              str(cmd_args.windowsize)+'s',
                              str(cmd_args.t_overlap)+'%'
                          ]
                      ),
                      '-'.join(['-'.join(cmd_args.bands),
                                str(cmd_args.f_overlap) + '.hdf5'])
                      ]))

        logging.info("\t\t\tWriting {} features".format(domain))
        hdf5_file = h5py.File(file_name, 'w')
        for rec_id, rec_features in enumerate(features):
            hdf5_file.create_dataset(str(rec_id), rec_features.shape, data=rec_features)
        hdf5_file.close()

        return directory

    def check_out(self):
        if self.working_dir and not os.path.exists(self.working_dir):
            os.makedirs(self.working_dir)

    def __init__(self, working_dir=""):
        # TODO: instead of creating the hdf 5 file and assembling the file name again when writing, store it as a member
        self.working_dir = working_dir
        self.check_out()


class Hdf5Reader:
    def read_domain_hdf5(self, file_path, crop=None):
        """ read the feature file with the specified file path
        :param file_path: path of the file
        :return: ndarray of the data of that file
        """
        if not os.path.exists(file_path):
            logging.fatal("Cannot read feature file {}.".format(file_path))
            exit()

        hdf5_file = h5py.File(file_path, 'r')
        stored_features = []
        # go through rec_ids in order
        for rec_id in range(len(hdf5_file.keys())):
            # crop can be either None or an integer
            arr = hdf5_file.get(str(rec_id))[:crop]
            stored_features.append(np.array(arr))
        hdf5_file.close()

        return np.stack(stored_features)

    def read_domain_hdf5_successively(self, file_path, dataset, crop=None):
        """ read the feature file with the specified file path
        :param file_path: path of the file
        :return: ndarray of the data of that file
        """
        file_path = os.path.join(self.working_dir, file_path)
        if not os.path.exists(file_path):
            logging.fatal("Cannot read feature file {}.".format(file_path))
            exit()

        hdf5_file = h5py.File(file_path, 'r')
        if dataset not in hdf5_file.keys():
            return None

        if crop:
            stored_features = np.array(hdf5_file[dataset][:crop])
        else:
            stored_features = np.array(hdf5_file[dataset])
        hdf5_file.close()

        return stored_features

    def __init__(self, working_dir=""):
        self.working_dir = working_dir


class JsonIO(object):
    def store(self, to_store, path):
        with open(os.path.join(path, "features.json"), "w") as json_file:
            json.dump(
                to_store,
                json_file,
                indent=4,
                sort_keys=True
            )

    def load(self, path):
        with open(os.path.join(path, "features.json"), "r") as json_file:
            loaded = json.load(json_file)

        return loaded

    def __init__(self):
        pass


# ______________________________________________________________________________________________________________________
def write_window_counts_successively(output, in_dir, window_count):
    dir_splits = in_dir.split('/')
    data_set = dir_splits[-5]
    train_or_eval = dir_splits[-2]
    file_name = os.path.join(output, data_set, train_or_eval, "window.counts")

    with open(file_name, 'a') as f:
        f.write(str(window_count))
        f.write('\n')


# ______________________________________________________________________________________________________________________
def delete_previous_runs_info(in_dir, cmd_args):
    dir_splits = in_dir.split('/')
    data_set = dir_splits[-5]
    train_or_eval = dir_splits[-2]
    file_name = os.path.join(cmd_args.output, data_set, train_or_eval, "window.counts")
    if os.path.exists(file_name):
        os.remove(file_name)

    file_name = os.path.join(cmd_args.output, data_set, train_or_eval, "recordings.list")
    if os.path.exists(file_name):
        os.remove(file_name)


# ______________________________________________________________________________________________________________________
def write_recording_names_successively(output, in_dir, recording_name):
    """
    :param output:
    :param in_dir:
    :param recording_name:
    :return:
    """
    dir_splits = in_dir.split('/')
    data_set = dir_splits[-5]
    train_or_eval = dir_splits[-2]
    file_name = os.path.join(output, data_set, train_or_eval, "recordings.list")
    recording_name = '/'.join(recording_name.split('/')[-3:])

    with open(file_name, 'a') as f:
        f.write(recording_name)
        f.write('\n')


# ______________________________________________________________________________________________________________________
def read_window_counts(directory):
    name = "window.counts"
    path = os.path.join(directory, name)

    if not os.path.exists(path):
        return None
    else:
        with open(path, 'r') as f:
            window_counts = f.readlines()
            window_counts = [int(window_count.strip()) for window_count in window_counts]

        return np.asarray(window_counts)


# ______________________________________________________________________________________________________________________
def read_recording_names(directory):
    """ reads the names of all recordings of which features were computed from file. this is used to allow backtracking
    of misclassified files after cv
    :param directory: in the directory exists a list that holds the recording names
    :return: dictionary holding a list of file names for every class
    """
    name = "recordings.list"
    path = os.path.join(directory, name)

    if not os.path.exists(path):
        return None
    else:
        with open(path, 'r') as f:
            recording_names = f.readlines()
            recording_names = [recording_name.strip() for recording_name in recording_names]

    return np.asarray(recording_names)


# ______________________________________________________________________________________________________________________
def write_feature_labels(output, feature_labels):
    """ writes the feature of the computed features to file
    :param output: output directory
    :param feature_labels: a list of labels that identify the features
    """
    with open(os.path.join(output, 'features.list'), 'w') as out_file:
        out_file.write('\n'.join(feature_labels))


# ______________________________________________________________________________________________________________________
def read_feature_labels(output):
    """ reads the list of features that were computed in the preprocessing step from file. this is used to allow picking
    subsets of these features for training
    :param output: directory where the list was stored to
    :return: ndarray of the feature labels
    """
    path = os.path.join(output, 'features.list')
    if not os.path.exists(path):
        logging.warning("Cannot read feature labels. Path/File does not exist.")
        return None
    else:
        with open(path, 'r') as in_file:
            feature_labels = in_file.readlines()
        feature_labels = [feature_label.strip() for feature_label in feature_labels]

    return np.asarray(feature_labels)


# ______________________________________________________________________________________________________________________
def assemble_outdir_name(cmd_args):
    dir_name = ''
    for in_dir in cmd_args.input:
        cl = in_dir.split('/')[-5]
        dir_name += cl
        dir_name += '_'
    dir_name += cmd_args.agg
    dir_name += '_'
    dir_name += str(cmd_args.cut) + 's_'
    dir_name += '-'.join([cmd_args.window, str(cmd_args.windowsize) + 's', str(cmd_args.t_overlap) + '%'])
    dir_name += '_'
    dir_name += '-'.join(cmd_args.bands)
    dir_name += '-' + str(cmd_args.f_overlap)
    return os.path.join(cmd_args.output, dir_name)


# ______________________________________________________________________________________________________________________
def check_out(out_dir, in_dirs=None):
    """ check if the specified output directory exists. if not, create it as well as a subdirectory for all classes
    :param out_dir: output directory
    :param in_dirs: input directory holding the name of the classes
    """
    # create output directory
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    if in_dirs is not None:
        # for every class create a train and an eval subdirectory
        for in_dir in in_dirs:
            dir_splits = in_dir.split('/')
            data_set = dir_splits[-5]
            train_or_eval = dir_splits[-2]
            path = os.path.join(out_dir, data_set)
            if not os.path.exists(path):
                os.makedirs(path)

            path = os.path.join(out_dir, data_set, train_or_eval)
            if not os.path.exists(path):
                os.makedirs(path)


# ______________________________________________________________________________________________________________________
def write_results(location, classes, results):
    """ writes the results to file. this is mean accuracy, std accuracy, mean precision, std precision, mean recall,
    std recall, mean f1 and std f1 averaged over all folds
    :param location: the output directory
    :param classes: list of feature file paths
    :param results: results as a list of lists. inner lists are accuracies, mean accuracy, std accuracy, ...
    """
    check_out(location)
    # these are tuples consisting of list of values, mean of the values and std the values
    [accuracies, precisions, recalls, f1scores] = results

    file_name = classes[0].split('/')[-3]
    with open(os.path.join(location, file_name + '.results'), 'w') as out_file:
        # for class_id, class_ in enumerate(classes):
            # out_file.write(file_name[class_id].split('_')[0] + '\n')
            # out_file.write(infos)
            # out_file.write(' '.join([str(header_item) for header_item in headers[class_id]]))
            # out_file.write('\n\n')

        out_file.write("Mean accuracy is {:.2f} with a std of ({:.2f}).\n".format(accuracies[1], accuracies[2]))
        out_file.write('\n'.join([str(accuracy) for accuracy in accuracies[0]]) + '\n')

        # with open("elec-exp.txt", "a+") as f:
        #     f.write("{:.2f} {:.2f}\n".format(accuracies[1], accuracies[2]))

        if len(classes) == 2:
            out_file.write("\nMean precision is {:.2f} with a std of ({:.2f}).\n".format(precisions[1], precisions[2]))
            out_file.write('\n'.join([str(precision) for precision in precisions[0]]) + '\n')

            out_file.write("\nMean recall is {:.2f} with a std of ({:.2f}).\n".format(recalls[1], recalls[2]))
            out_file.write('\n'.join([str(recall) for recall in recalls[0]]) + '\n')

            out_file.write("\nMean f1 score is {:.2f} with a std of ({:.2f}).\n".format(f1scores[1], f1scores[2]))
            out_file.write('\n'.join([str(f1score) for f1score in f1scores[0]]) + '\n')


# ______________________________________________________________________________________________________________________
def write_classifier_output(location, folds, labels, predictions, class_probs, names=None):
    """ writes the classifier cv predictions to file. that is, for every fold store the recordings that were predicted,
    their label and prediction and the probabilities to one row.
    :param location: directory where to save the output
    :param folds: number of cv folds
    :param labels: the labels of the predicted recordings
    :param predictions: the predictions of the recordings that should equal the labels
    :param class_probs: the probabilities of the different classes
    :param names: the names of the predicted recordings
    """
    with open(os.path.join(location, '-'.join(["classifier", "fold", "predictions"]) + '.txt'), 'w') as out_file:
        out_file.write("_"*120+'\n')
        for fold in range(folds):
            out_file.write("fold " + str(fold+1) + ':\n')
            out_file.write("{:50} {:<12} {:<12} {:<9} {:<9}\n".format("recording", "prediction", "label", "class 0",
                                                                      "class 1"))
            fold_labels, fold_predictions, fold_class_probs = labels[fold], predictions[fold], class_probs[fold]

            if names is not None and len(names) != 0:
                fold_names = np.hstack(names[fold])
            else:
                fold_names = len(fold_predictions) * ['']

            for pred_lab_tuple in zip(fold_names, fold_predictions, fold_labels, fold_class_probs[:, 0],
                                      fold_class_probs[:, 1]):
                (name, pred, label, prob1, prob2) = pred_lab_tuple
                out_file.write("{:50} {:<12} {:<12} {:<9.2f} {:<9.2f}\n".format(name, pred, label, prob1, prob2))
            out_file.write('\n')


# ______________________________________________________________________________________________________________________
def write_to_google_result_tracking(classes, n_recs, agg, window, w_size, t_overlap, clf, n_est, domain, feats, elecs,
                                    N, bands, f_overlap, scale, folds, autoskl, results, notes='', notes2='',
                                    location=None, no_up=False):
    """ automatically update the goolge spreadsheet that tracks the results """
    # a row looks like this:
    # classes: class_0, class_1
    # recs: n_recs_class_0, n_recs_class_1
    # err: n_err_class_0, n_err_class_1
    # -
    # window: the window function being used
    # size: the size of the time window
    # overlap: overlap of time windows
    # -
    # clf: the classifier being used
    # est: number of estimators
    # -
    # features: a list of features being used
    # elecs: a list of electrodes being used
    # N: the number of features being used
    # bands: the frequency bands being used
    # overlap: overlap of frequency bands
    # scale/norm: whether and how features are scaled / normalized
    # -
    # folds: the number of cv folds
    # mean acc:
    # std acc:
    # mean prec:
    # std prec:
    # mean rec:
    # std rec:
    # mean f1:
    # std f1:
    # notes1:
    # notes2

    # classes = [class_.split('/')[-4].split('v')[0] for class_ in classes]
    classes = ', '.join(classes)
    n_recs = ', '.join([str(n_rec) for n_rec in n_recs])
    w_size = w_size
    t_overlap = t_overlap
    clf = clf
    n_est = n_est
    domain = ', '.join(domain)
    feats = ', '.join(feats)
    subset = ';\n'.join([domain, feats])
    elecs = ', '.join(elecs)
    # N = str(N)
    # bands = ', '.join(bands)
    # convert to int
    f_overlap = "1" if f_overlap else "0"
    autoskl = [str(auto) for auto in autoskl]
    autoskl = ', '.join(autoskl)
    if notes == '':
        notes = autoskl
    else:
        notes2 = autoskl
    # notes = ', '.join(notes)
    folds = folds
    [accs, precs, recs, f1s] = results
    mean_accs = "{:.2f}".format(accs[1])
    std_accs = "{:.2f}".format(accs[2])
    mean_precs = "{:.2f}".format(precs[1])
    std_precs = "{:.2f}".format(precs[2])
    mean_recs = "{:.2f}".format(recs[1])
    std_recs = "{:.2f}".format(recs[2])
    mean_f1s = "{:.2f}".format(f1s[1])
    std_f1s = "{:.2f}".format(f1s[2])

    # worksheet_name = '_'.join([class_.split('/')[-4] for class_ in classes])

    if not no_up:
        # try to upload to google doc
        outh_file = "client_secret_774608351368-1rhj46mkk760tjrd5k40u82d89uqd9vc.apps.googleusercontent.com.json"
        if not os.path.exists(outh_file):
            logging.warning("Cannot upload the data to google result_tracking spreadsheet. Are you authorized?")

        else:
            import pygsheets
            gc = pygsheets.authorize(outh_file=outh_file)
            sh = gc.open("experiment_tracking")

            wks = sh.worksheet_by_title(window)
            wks.append_table(values=[classes, n_recs, agg, window, w_size, t_overlap, clf, n_est, subset, elecs, N,
                                     bands, f_overlap, scale, folds, mean_accs, std_accs, mean_precs, std_precs,
                                     mean_recs, std_recs, mean_f1s, std_f1s, notes, notes2])

    # additionally write to csv file
    with open(os.path.join(location, "experiments_"+window+".csv"), 'a+') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter=';')
        csv_writer.writerow([classes, n_recs, agg, window, w_size, t_overlap, clf, n_est, subset, elecs, N, bands,
                             f_overlap, scale, folds, mean_accs, std_accs, mean_precs, std_precs, mean_recs,
                             std_recs, mean_f1s, std_f1s, notes, notes2])


# ______________________________________________________________________________________________________________________
def update_cmd_args(cmd_args):
    """ updates the cmd arguments s.t. the automatic entries of google spreadsheet are correct. i.e. when running the
    pipeline with 'no-pre' option, it would use the default values for window, window_size etc. this function overrides
    these values with the actual values as given by the feature file name
    :param cmd_args: dictionary of cmd arguments
    :return: updated cmd arguemnts including the information stored in feature file name
    """
    windows, window_sizes, all_bands, domains, inputs, cut_times, f_overlaps, aggs = [], [], [], [], [], [], [], []
    t_overlaps = []
    for in_dir in cmd_args.input:
        edf_reader = EdfReader()
        feature_in_files = edf_reader.read_all_file_names(in_dir, extension='.hdf5', key='natural')
        for feature_in_file in feature_in_files:
            file_name = feature_in_file.split('/')[-1]
            [class_, domain, agg, cut_time, window_info, band_info] = file_name.split('_')
            f_overlap = band_info.split('-')[-1].split('.')[0]
            bands = band_info.split('-')[:-1]

            window, window_size, t_overlap = window_info.split('-')
            window_size = window_size[:-1]
            t_overlap = t_overlap[:-1]

            if cut_time != 'none' and cut_time != 'None':
                cut_time = cut_time[:-1]

            domains.append(domain)
            train_or_eval = feature_in_file.split('/')[-2]
            in_file = os.path.join(cmd_args.output, class_, train_or_eval)
            if in_file not in inputs:
                inputs.append(in_file)

            windows.append(window)
            window_sizes.append(window_size)
            all_bands.append(', '.join(bands))
            cut_times.append(cut_time)
            f_overlaps.append(f_overlap)
            aggs.append(agg)
            t_overlaps.append(t_overlap)

    if not (len(np.unique(windows)) == 1 and
            len(np.unique(window_sizes)) == 1 and
            len(np.unique(all_bands)) == 1 and
            len(np.unique(f_overlaps)) == 1 and
            len(np.unique(cut_times)) == 1 and
            len(np.unique(aggs)) == 1 and
            len(np.unique(t_overlaps)) == 1):
        logging.error("You are trying to train on features that were computed with unequal cut time, window function, "
                      "window size, time or frequency overlap or frequency bands.")
        print(np.unique(windows), np.unique(window_sizes), np.unique(all_bands), np.unique(f_overlaps),
              np.unique(cut_times), np.unique(aggs))
        exit()

    cmd_args.window = windows[0]
    cmd_args.windowsize = window_sizes[0]
    cmd_args.bands = all_bands[0]
    cmd_args.cut = cut_times[0]
    cmd_args.f_overlap = f_overlaps[0]
    # cmd_args.agg = aggs[0]
    cmd_args.t_overlap = t_overlaps[0]
    # np.unique sorts the lists!
    # cmd_args.domain = np.unique(domains)
    cmd_args.input = inputs

    return cmd_args
