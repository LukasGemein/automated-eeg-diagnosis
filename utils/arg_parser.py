import textwrap as _textwrap
import argparse
import json

from feature_generation import feature_generator
from windowing import splitter
from cleaning import cleaner

# TODO: store available domains, windows and electrodes in json file and remove imports below
# TODO: add subparser to make auto_skl argument mandatory for auto_eeg_autoskl_main.py


# ______________________________________________________________________________________________________________________
class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
    # this makes the help look much nicer since it allows for a higher line width
    def _split_lines(self, text, width):
        text = self._whitespace_matcher.sub(' ', text).strip()
        return _textwrap.wrap(text, 120)


# ______________________________________________________________________________________________________________________
class ArgParser(argparse.ArgumentParser):

    def add_args(self):
        # non-optional
        self.add_argument('input', type=str, nargs='+',
                          help='path to parent directory of normal / abnormal raw data/feature files if preprocessing '
                               'is skipped')
        self.add_argument('output', type=str, help='output directory')

        # optional
        self.add_argument('--agg', type=str, default="median", choices=["mean", "median", "var", "none"],
                          help='how to aggregate features over the dimension of time windows. default median')

        self.add_argument('--auto_skl', '--autoskl', type=str, nargs=2, metavar='', default=None,
                          help='specifies the total and run time budget of autosklearn.')

        self.add_argument('--bands', '--band', type=str, metavar='', nargs='+',
                          help="frequency bands used in feature computation, e.g. '0, 2, 4' -> 0-2, 2-4. "
                               "default: '0 2 4 8 13 18 24 30 60 120'",
                          default=["0", "2", "4", "8", "13", "18", "24", "30", "60", "120"])

        self.add_argument('--clf', '--classifier', type=str, metavar='', default='RF',
                          choices=['RF', 'LR', 'SVM', 'AB', 'NB', 'KNN', 'GP', 'QDA', 'DT', 'GB'],
                          help='the classifier of choice. choose between: '
                               + ', '.join(['RF', 'LR', 'SVM', 'AB', 'NB', 'KNN', 'GP', 'QDA', 'DT', 'GB']))

        self.add_argument('--cut', type=int, default=None, metavar='',
                          help="enables usage of only the first 'cut' seconds of the EEG signals. default: None")

        self.add_argument('--domains', '--domain', type=str, nargs='+', metavar='', default=['all'],
                          choices=feature_generator.DOMAINS + ['all'],
                          help='list of features to be computed / used for classification if preprocessing is skipped. '
                               'choose between: ' + ', '.join(feature_generator.DOMAINS) + ', all')

        self.add_argument('--elecs', '--elec', type=str, nargs='+', metavar='', default=['all'],
                          help='list of electrodes to be used for feature computation / prediction of preprocessing '''
                               'is skipped. choose between: ' + ', '.join(cleaner.ELECTRODES) + ', all.')

        self.add_argument('--feats', '--feat', type=str, nargs='+', metavar='', default=['all'],
                          help='list of features used for classification. does not restrict computation of features '
                               'during preprocessing. take a look at file "features.list" for choices.')

        self.add_argument('--filters', type=int, nargs='+', metavar='', default=[0, -1],
                          help='the filters from the common spatial patterns to be picked as features')

        self.add_argument('--f_overlap', '--foverlap', action='store_true',
                          help='whether or not the frequency bands should overlap by 50%%')

        self.add_argument('--no-pre', '--nopre', action='store_true',
                          help='skips the preprocessing step. activate this when features are already available.')

        self.add_argument('--n-proc', '--nproc', type=int, default=1, metavar='',
                          help='number of processes used to read data and compute features')

        self.add_argument('--no-up', '--noup', action='store_true',
                          help='use this option if you do NOT want to upload the results to google spreadsheet.')

        self.add_argument('--pre-only', '--preonly', action='store_true',
                          help='set this if you want to compute features only. no predictions.')

        self.add_argument('--subset', type=int, default=None, metavar='',
                          help='if a performance estimate on a random subset of the data is desired')

        self.add_argument('--t_overlap', '--toverlap', type=int, default=0, metavar='',
                          help='percentage of overlap of time windows')

        self.add_argument('--verbosity', type=int, default=20, metavar='', choices=[0, 10, 20, 30, 40, 50],
                          help='verbosity level of logging: ' + ', '.join(str(s) for s in [0, 10, 20, 30, 40, 50]))

        self.add_argument('--visualize', type=str, nargs='+', metavar='', default=['train', 'pred', 'post'],
                          help='specify what steps of the pipeline should be visualized. choose between: ' +
                               ', '.join(['all', 'pre', 'train', 'pred', 'post', 'none']))

        self.add_argument('-w', '--window', type=str, default='boxcar', choices=splitter.WINDOWS, metavar='',
                          help='window function to improve fourier transform. choose between: '
                               + ', '.join(splitter.WINDOWS))

        self.add_argument('-ws', '--windowsize', '--window-size', type=float, default=2.0, metavar='',
                          help='length of the window in seconds')

# ______________________________________________________________________________________________________________________
    def write_to_json(self, cmd_args, unknwon):
        return
        # with open("parsed_args.json", "w") as json_file:
        #     json.dump(vars(cmd_args), json_file, indent=4, sort_keys=True)

        # with open("unknown_args.json", "w") as json_file:
        #     json.dump(vars(unknwon), json_file, indent=4, sort_keys=True)

# ______________________________________________________________________________________________________________________
    def replace_all_with_values(self, cmd_args):
        # make sure cmd args electrodes and domains get translated from 'all' to actual values as well as sort domains
        if 'all' in cmd_args.elecs:
            cmd_args.elecs = list(cleaner.ELECTRODES)
        if 'all' in cmd_args.domains:
            cmd_args.domains = feature_generator.DOMAINS
        else:
            cmd_args.domains = sorted(cmd_args.domains)
        return cmd_args

# ______________________________________________________________________________________________________________________
    def parse_known_args(self, args=None, namespace=None):
        cmd_args, unknown = argparse.ArgumentParser.parse_known_args(self)
        cmd_args = self.replace_all_with_values(cmd_args)

        self.write_to_json(cmd_args, unknown)
        return cmd_args, unknown

# ______________________________________________________________________________________________________________________
    def __init__(self):
        argparse.ArgumentParser.__init__(self, formatter_class=LineWrapRawTextHelpFormatter)

        self.add_args()
