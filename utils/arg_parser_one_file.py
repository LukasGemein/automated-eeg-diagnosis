import textwrap as _textwrap
import argparse
import json


# ______________________________________________________________________________________________________________________
class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
    # this makes the help look much nicer since it allows for a higher line width
    def _split_lines(self, text, width):
        text = self._whitespace_matcher.sub(' ', text).strip()
        return _textwrap.wrap(text, 120)


# ______________________________________________________________________________________________________________________
class ArgParser(argparse.ArgumentParser):

    def add_args(self):
        # non-optional
        self.add_argument('input', type=str,
                          help='path to parent directory of raw data')

        self.add_argument('output', type=str, help='output directory')

        # optional
        self.add_argument('--agg', type=str, default="median", choices=["mean", "median", "var", "none", "None"],
                          help='how to aggregate features over the dimension of time windows. default median')

        self.add_argument('--bands', '--band', type=str, metavar='', nargs='+',
                          help="frequency bands used in feature computation, e.g. '0, 2, 4' -> 0-2, 2-4. "
                               "default: '0 2 4 8 13 18 24 30'",
                               # "default: '0 2 4 8 13 18 24 30 60 120'",
                          # default=["0", "2", "4", "8", "13", "18", "24", "30", "60", "120"])
                          default=["0", "2", "4", "8", "13", "18", "24", "30", "50"])

        self.add_argument('--cut', type=int, default=None, metavar='',
                          help="enables usage of only the first 'cut' seconds of the EEG signals. default: None")

        self.add_argument('--domains', '--domain', type=str, nargs='+', metavar='', default=['all'],
                          choices=self.domains + ['all'],
                          help='list of features to be computed / used for classification if preprocessing is skipped. '
                               'choose between: ' + ', '.join(self.domains) + ', all')

        self.add_argument('--elecs', '--elec', type=str, nargs='+', metavar='', default=['all'],
                          help='list of electrodes to be used for feature computation / prediction of preprocessing '''
                               'is skipped. choose between: ' + ', '.join(self.elecs) + ', all.')

        self.add_argument('--f_overlap', '--foverlap', action='store_true',
                          help='whether or not the frequency bands should overlap by 50%%')

        self.add_argument('--verbosity', type=int, default=20, metavar='', choices=[0, 10, 20, 30, 40, 50],
                          help='verbosity level of logging: ' + ', '.join(str(s) for s in [0, 10, 20, 30, 40, 50]))

        self.add_argument('-w', '--window', type=str, default='boxcar', metavar='', choices=self.windows,
                          help='window function to improve fourier transform-based spectral estimation')

        self.add_argument('-ws', '--windowsize', '--window-size', type=float, default=2.0, metavar='',
                          help='length of the window in seconds')

# ______________________________________________________________________________________________________________________
    def write_to_json(self, cmd_args, unknwon):
        return
        # with open("parsed_args.json", "w") as json_file:
        #     json.dump(vars(cmd_args), json_file, indent=4, sort_keys=True)

        # with open("unknown_args.json", "w") as json_file:
        #     json.dump(vars(unknwon), json_file, indent=4, sort_keys=True)

# ______________________________________________________________________________________________________________________
    def replace_all_with_values(self, cmd_args):
        # make sure cmd args electrodes and domains get translated from 'all' to actual values as well as sort domains
        if 'all' in cmd_args.elecs:
            cmd_args.elecs = self.elecs
        if 'all' in cmd_args.domains:
            cmd_args.domains = self.domains
        else:
            cmd_args.domains = sorted(cmd_args.domains)
            cmd_args.elecs = sorted(cmd_args.elecs)
        return cmd_args

# ______________________________________________________________________________________________________________________
    def parse_known_args(self, args=None, namespace=None):
        cmd_args, unknown = argparse.ArgumentParser.parse_known_args(self)
        cmd_args = self.replace_all_with_values(cmd_args)

        self.write_to_json(cmd_args, unknown)
        return cmd_args, unknown

# ______________________________________________________________________________________________________________________
    def __init__(self, elecs, domains, windows):
        argparse.ArgumentParser.__init__(self, formatter_class=LineWrapRawTextHelpFormatter)
        self.elecs = elecs
        self.domains = domains
        self.windows = windows

        self.add_args()
