from multiprocessing import Pool
from functools import partial
import numpy as np
import unittest

# TODO: make chunksize an initialization argument?
# TODO: maybe chunksize has again to be moved out of the class to split the in_q_list before running things in parallel
# TODO: since results will all be kept in memory until all files are processed. might be too much


class ParallelExecutor(object):
    """ a class to parallel a task. takes the number of processes during initialization. for execution it expects
     a list of items, a function to be applied to these items as well as additional arguments to that function.
     keeps order of items in the """

    def decouple(self, list_of_tuples):
        """ returned from the parallel execution is a tuple in form of (id, data). decouple those tuples
        :param list_of_tuples:
        :return: ndarray of ids and ndarray of data
        """
        # TODO: do not sort? some things do not have to be sorted
        sorted_list_of_tuples = sorted(list_of_tuples)
        ids = np.array([x[0] for x in sorted_list_of_tuples])
        data = [x[1] for x in sorted_list_of_tuples]
        return ids, data

    def find_suitable_chunksize(self, n_input_elems):
        chunksize = max(self.n_proc, int(n_input_elems/10))
        chunksize = min(chunksize, n_input_elems)
        return chunksize

    def run(self, in_q_list, parallel_function, *args, **kwargs):
        """ process items in the in_q_list in parallel computing parallel_function and keeping the order
        :param in_q_list: a list of items to be processed
        :param parallel_function: the function to be called upon every item
        :param args: additional arguments to the parallel_function
        :param kwargs: additional arguments to the parallel_function
        :return: results of applying parallel function to every item sorted by item id
        """
        in_q_list = [(item_id, item) for item_id, item in enumerate(in_q_list)]

        assert callable(parallel_function)
        initialized_parallel_function = partial(parallel_function, *args, **kwargs)

        pool = Pool(processes=self.n_proc)
        results = pool.imap_unordered(
            initialized_parallel_function, in_q_list,
            chunksize=self.find_suitable_chunksize(len(in_q_list))
        )  # assuming in_q_list >> n_proc
        pool.close()
        pool.join()

        # the parallel function does not necessarily need to return sth
        # so check if there is something to return
        results = list(results)
        if results and results[0] is not None:
            result_ids, results = self.decouple(results)
            return result_ids, results

    def __init__(self, n_proc):
        self.n_proc = n_proc


def worker(item, a):
    print(a)
    i, v = item
    return i, v


class TestParallelExecutor(unittest.TestCase):
    def parallel_function(self, item):
        test_id, test_item = item
        import time
        time.sleep(3-test_item)
        return test_id, test_item

    # unfortuantely, this is not wokring. parallel function is never executed, test is never finished
    # def test_run(self):
    #     pe = ParallelExecutor(1)
    #     # ids, data = pe.run(
    #     #     in_q_list=np.arange(2),
    #     #     parallel_function=self.parallel_function,
    #     # )
    #     in_q_list = [1, 2, 3, 4, 5]
    #     ids, vs = pe.run(in_q_list=in_q_list, parallel_function=worker, a="hi")
    #     self.assertSequenceEqual(
    #         ids.tolist(),
    #         np.arange(len(ids)).tolist()
    #     )

    def test_find_suitable_chunksize(self):
        pe = ParallelExecutor(16)
        chunksize = pe.find_suitable_chunksize(1361)
        self.assertEqual(chunksize, 136)
        chunksize = pe.find_suitable_chunksize(150)
        self.assertEqual(chunksize, 16)
        chunksize = pe.find_suitable_chunksize(10)
        self.assertEqual(chunksize, 10)

    def test_decouple(self):
        an_object = object()
        a_list = ["a", "b", "c"]
        a_dictionary = {"test": True}
        an_integer = 5

        pe = ParallelExecutor(2)
        ids, data = pe.decouple([
            (2, a_list),
            (0, a_dictionary),
            (4, an_integer),
            (1, an_object)
        ])
        self.assertEqual(type(ids), np.ndarray)
        self.assertEqual(type(data), list)
        self.assertSequenceEqual(
            ids.tolist(),
            [0, 1, 2, 4]
        )
        self.assertEqual(
            data,
            [a_dictionary, an_object, a_list, an_integer]
        )


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestParallelExecutor)
    unittest.TextTestRunner(verbosity=2).run(suite)
