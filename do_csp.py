from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.preprocessing import StandardScaler
from numpy.core.numerictypes import typecodes
from sklearn.metrics import accuracy_score
from os import path, makedirs
from sklearn.svm import SVC
from scipy import linalg
import numpy as np
# import pyriemann
import scipy
import sys

from utils import logger, arg_parser, parallel_executor
from utils.io import EdfReader, Hdf5Reader, Hdf5Writer
from windowing import splitter
from cleaning import cleaner

SEED = 4316932


# use this?
# class SignalAndTarget(object):
#     def __init__(self, X, y):
#         self.X = X
#         self.y = y


# TODO: how to speed this up. takes forever, even on cluster with 16 cores in parallel

class CspFilterer(object):
    """ """
    def apply_csp_fast_decorator(self, raw_cleaned, columns, bands, cl, sp):
        signals = raw_cleaned.get_data()
        sfreq = raw_cleaned.info["sfreq"]
        # TODO: store band signals, since they can be used in computation of covariance matrices?
        logger.debug("applying filter of shape {} to signals of shape {}".format(self.csp.shape, signals.shape))
        band_filtered = []
        for band_id, band in enumerate(bands):
            lower, upper = band
            band_signals = cl.band_filter_to_specific_band(signals, sfreq, lower, upper)  # (n_chls x n_times)
            epo = sp.split(band_signals, sfreq, weight=False)  # (n_epochs x n_channels x n_times)

            filtered = self.apply_csp_fast(epo=epo, filt=self.csp[band_id], columns=columns)
            band_filtered.append(filtered)  # (n_epochs x n_columns x n_times)

        band_filtered = np.asarray(band_filtered)  # (n_bands x n_epochs x n_columns x n_times)

        # if self.agg:
        #     band_filtered = self.agg(band_filtered, axis=1)
        logger.debug("this shape {} is fine".format(band_filtered.shape))
        return band_filtered

    def apply_csp_fast(self, epo, filt, columns):
        """
        :param columns: the common spatial filters to be picked from the projection matrix filt
        :return: the signals in the specified frequency bands filtered by the selceted common spatial patterns from
        projection matrix filt. has shape: n_bands x n_epochs x n_columns x n_times
        """
        # take all the filters
        if columns is not None:
            selected_filters = filt[:, columns]  # (n_channels x n_columns)
        else:
            selected_filters = filt  # (n_channels x n_channels)
        filtered = []
        for trial_i in range(len(epo)):
            # (n_times x n_channels) dot (n_channels x n_colums)
            this_filtered = np.dot(epo[trial_i].T, selected_filters)  # (n_times x n_columns)
            filtered.append(this_filtered.T)  # (n_columns x n_times)

        return filtered

    def apply_csp_var_log_decorator(self, raw_cleaned, columns, bands, agg, sp, cl):
        # TODO: remove columns argument and apply all filters? could select filters when performing cv / training
        # n_bands x n_epochs x n_columns x n_times
        csp_filtered = self.apply_csp_fast_decorator(raw_cleaned=raw_cleaned,
                                                     columns=columns,
                                                     bands=bands,
                                                     cl=cl,
                                                     sp=sp)

        var_logged_band_csp_filtered = []
        for band_id, band in enumerate(bands):
            var_logged_band_csp_filtered.append(self.apply_csp_var_log(csp_filtered[band_id]))
        var_logged_band_csp_filtered = np.asarray(var_logged_band_csp_filtered)  # n_bands x n_epochs x n_columns
        logger.debug("var logged band csp filtered {}".format(var_logged_band_csp_filtered.shape))
        var_logged_band_csp_filtered = agg(var_logged_band_csp_filtered, axis=1)  # n_bands x n_columns
        logger.debug("final shape after aggregation {}".format(var_logged_band_csp_filtered.shape))
        return var_logged_band_csp_filtered

    def apply_csp_var_log(self, csp_filtered):
        """ apply the common spatial pattern filter to a signal
        :return: the log variance of signals in the specified frequency bands filtered by the selected common spatial
        patterns from projection matrix filters
        """
        # n_epochs x _n_columns
        log_vars = [np.log(np.var(trial, axis=-1)) for trial in csp_filtered]
        return log_vars

    def parallel_application_of_csp(self, item, csp_filterer, cl, sp, filters, agg):
        edf_id, edf_file = item
        raw_cleaned = cl.clean(edf_file)
        features = csp_filterer.apply_csp_var_log_decorator(raw_cleaned=raw_cleaned,
                                                            columns=filters,
                                                            bands=cl.get_bands(),
                                                            cl=cl,
                                                            sp=sp,
                                                            agg=agg)
        return edf_id, features.ravel()

    def __init__(self, csp):
        self.csp = csp


class CspComputer(object):
    def calculate_csp_decorator(self, c1, c2, bands):
        """
        :param c1:
        :param c2:
        :param bands:
        :return:
        """
        band_vs, band_as, band_ds = [], [], []
        for band_id, band in enumerate(bands):
            band_v, band_a, band_d = self.calculate_csp(c1[band_id], c2[band_id])
            band_vs.append(band_v)
            band_as.append(band_a)
            band_ds.append(band_d)

        return np.array(band_vs), np.array(band_as), np.array(band_ds)

    def calculate_csp(self, c1, c2):
        """ use robin/lukas'/joos' implementation to compute the common spatial pattern given two covariance matrices.
        they were computed in a previous run.
        :param c1: normal covariance matrix (n_bands x n_elecs x n_elecs)
        :param c2: abnormal covariance matrix (n_bands x n_elecs x n_elecs)
        :return: filters
        """
        # solution of csp objective via generalized eigenvalue problem
        # in matlab the signature is v, d = eig(a, b)
        d, v = linalg.eigh(c2, c1 + c2)
        d = d.real
        # make sure the eigenvalues and -vectors are correctly sorted
        indx = np.argsort(d)
        # reverse
        indx = indx[::-1]
        d = d.take(indx)
        v = v.take(indx, axis=1)

        # Now compute patterns
        # old pattern computation
        # a = sp.linalg.inv(v).transpose()
        c_avg = (c1 + c2) / 2.0

        # compare
        # https://github.com/bbci/bbci_public/blob/c7201e4e42f873cced2e068c6cbb3780a8f8e9ec/processing/proc_csp.m#L112
        # with W := v
        v_with_cov = np.dot(c_avg, v)
        source_cov = np.dot(np.dot(v.T, c_avg), v)

        a = linalg.solve(source_cov.T, v_with_cov.T).T
        return v, a, d


class CovarianceComputer(object):
    def _matrix_operator(self, Ci, operator):
        """matrix equivalent of an operator."""
        if Ci.dtype.char in typecodes['AllFloat'] and not np.isfinite(Ci).all():
            raise ValueError("Covariance matrices must be positive definite. Add regularization to avoid this error.")
        eigvals, eigvects = scipy.linalg.eigh(Ci, check_finite=False)
        eigvals = np.diag(operator(eigvals))
        Out = np.dot(np.dot(eigvects, eigvals), eigvects.T)
        return Out

    def sqrtm(self, Ci):
        """Return the matrix square root of a covariance matrix defined by :
        .. math::
                \mathbf{C} = \mathbf{V} \left( \mathbf{\Lambda} \\right)^{1/2} \mathbf{V}^T
        where :math:`\mathbf{\Lambda}` is the diagonal matrix of eigenvalues
        and :math:`\mathbf{V}` the eigenvectors of :math:`\mathbf{Ci}`
        :param Ci: the coavriance matrix
        :returns: the matrix square root
        """
        return self._matrix_operator(Ci, np.sqrt)

    def logm(self, Ci):
        """Return the matrix logarithm of a covariance matrix defined by :
        .. math::
                \mathbf{C} = \mathbf{V} \log{(\mathbf{\Lambda})} \mathbf{V}^T
        where :math:`\mathbf{\Lambda}` is the diagonal matrix of eigenvalues
        and :math:`\mathbf{V}` the eigenvectors of :math:`\mathbf{Ci}`
        :param Ci: the coavriance matrix
        :returns: the matrix logarithm
        """
        return self._matrix_operator(Ci, np.log)

    def expm(self, Ci):
        """Return the matrix exponential of a covariance matrix defined by :
        .. math::
                \mathbf{C} = \mathbf{V} \exp{(\mathbf{\Lambda})} \mathbf{V}^T
        where :math:`\mathbf{\Lambda}` is the diagonal matrix of eigenvalues
        and :math:`\mathbf{V}` the eigenvectors of :math:`\mathbf{Ci}`
        :param Ci: the coavriance matrix
        :returns: the matrix exponential
        """
        return self._matrix_operator(Ci, np.exp)

    def invsqrtm(self, Ci):
        """Return the inverse matrix square root of a covariance matrix defined by :
        .. math::
                \mathbf{C} = \mathbf{V} \left( \mathbf{\Lambda} \\right)^{-1/2} \mathbf{V}^T
        where :math:`\mathbf{\Lambda}` is the diagonal matrix of eigenvalues
        and :math:`\mathbf{V}` the eigenvectors of :math:`\mathbf{Ci}`
        :param Ci: the coavriance matrix
        :returns: the inverse matrix square root
        """
        isqrt = lambda x: 1. / np.sqrt(x)
        return self._matrix_operator(Ci, isqrt)

    def _get_sample_weight(self, sample_weight, data):
        """Get the sample weights.
        If none provided, weights init to 1. otherwise, weights are normalized.
        """
        if sample_weight is None:
            sample_weight = np.ones(data.shape[0])
        if len(sample_weight) != data.shape[0]:
            raise ValueError("len of sample_weight must be equal to len of data.")
        sample_weight /= np.sum(sample_weight)
        return sample_weight

    def mean_riemann(self, covmats, tol=10e-9, maxiter=50, init=None, sample_weight=None):
        """Return the mean covariance matrix according to the Riemannian metric.
        The procedure is similar to a gradient descent minimizing the sum of
        riemannian distance to the mean.
        .. math::
                \mathbf{C} = \\arg\min{(\sum_i \delta_R ( \mathbf{C} , \mathbf{C}_i)^2)}  # noqa
        :param covmats: Covariance matrices set, Ntrials X Nchannels X Nchannels
        :param tol: the tolerance to stop the gradient descent
        :param maxiter: The maximum number of iteration, default 50
        :param init: A covariance matrix used to initialize the gradient descent. If None the Arithmetic mean is used
        :param sample_weight: the weight of each sample
        :returns: the mean covariance matrix
        """
        # init
        sample_weight = self._get_sample_weight(sample_weight, covmats)
        Nt, Ne, Ne = covmats.shape
        if init is None:
            C = np.mean(covmats, axis=0)
        else:
            C = init
        k = 0
        nu = 1.0
        tau = np.finfo(np.float64).max
        crit = np.finfo(np.float64).max
        # stop when J<10^-9 or max iteration = 50
        while (crit > tol) and (k < maxiter) and (nu > tol):
            k = k + 1
            C12 = self.sqrtm(C)
            Cm12 = self.invsqrtm(C)
            J = np.zeros((Ne, Ne))

            for index in range(Nt):
                tmp = np.dot(np.dot(Cm12, covmats[index, :, :]), Cm12)
                J += sample_weight[index] * self.logm(tmp)

            crit = np.linalg.norm(J, ord='fro')
            h = nu * crit
            C = np.dot(np.dot(C12, self.expm(nu * J)), C12)
            if h < tau:
                nu = 0.95 * nu
                tau = h
            else:
                nu = 0.5 * nu

        return C

    def compute_cov_matrix_decorator(self, raw_cleaned, bands, cl, sp, average_trial_covariance=True):
        signals = raw_cleaned.get_data()
        sfreq = raw_cleaned.info["sfreq"]
        # TODO: store band signals, since they can be used in application of cps?
        covar_matrix_by_band = np.ndarray(shape=(len(bands), len(signals), len(signals)))
        for band_id, band in enumerate(bands):
            lower, upper = band
            band_signals = cl.band_filter_to_specific_band(signals, sfreq, lower, upper)  # n_chnnls x n_times

            if average_trial_covariance:
                band_signals = sp.split(band_signals, sfreq, weight=False)  # n_epochs x n_channels x n_times
            cov_matrix = self.compute_cov_matrix(epo=band_signals, average_trial_covariance=average_trial_covariance)
            covar_matrix_by_band[band_id] = cov_matrix

        return covar_matrix_by_band  # n_bands x n_channels x n_channels

    def compute_cov_matrix(self, epo, average_trial_covariance):
        """ compute covariance of all electrodes for the current recording
        :param average_trial_covariance: whether or not to compute covariance matrix as mean over all trials
        :return: ndarray with cov matrix in shape of n_bands x n_elecs x n_elecs
        """
        # compute covariance matrix either on 'trials', here time windows of the signals or on the whole signals
        if average_trial_covariance:
            # cov_matrix = np.mean([np.cov(x) for x in epo], axis=0)  # n_channels x n_channels
            covs = np.array([np.cov(x) for x in epo])
            cov_matrix = np.mean(covs, axis=0)
            # cov_matrix = self.mean_riemann(covs)
        else:
            cov_matrix = np.cov(epo)  # n_channels x n_channels

        return cov_matrix

    def parallel_computation_of_covariance_matrices(self, item, cl, sp):
        edf_id, edf_file = item
        raw_cleaned = cl.clean(edf_file)
        covariance = self.compute_cov_matrix_decorator(raw_cleaned=raw_cleaned,
                                                       bands=cl.get_bands(),
                                                       sp=sp,
                                                       cl=cl)
        return edf_id, covariance

    def compute_covariances_of_both_classes(self, classes, output, all_edf_files, pe, cl, sp):
        h5_writer = Hdf5Writer(output)
        covariances_files = []
        for class_id, class_ in enumerate(classes):
            # generate covariance matrices of recordings if the file does not yet exist
            covariances_file = "cov_{}_{}.h5".format(class_, "train")
            covariances_files.append(covariances_file)
            logger.info("checking for {}: {}".format(path.join(output, covariances_file),
                                                     path.exists(path.join(output, covariances_file))))
            if not path.exists(path.join(output, covariances_file)):
                logger.info("computing")
                cov_ids, covs = pe.run(in_q_list=all_edf_files[class_id],
                                       parallel_function=self.parallel_computation_of_covariance_matrices,
                                       cl=cl, sp=sp)
                covs = np.array(covs)

                for i, cov in enumerate(covs):
                    h5_writer.write_domain_hdf5_successively(covariances_file, cov, cov_ids[i])
                logger.info("{} covariances done".format(class_))
        return covariances_files


# class PyRiemann(object):
#     def cross_validate_decorator(self, covariances_files):
#         h5_reader = Hdf5Reader()
#         covariances = []
#         for covariances_file in covariances_files:
#             cov = h5_reader.read_domain_hdf5(covariances_file)
#             covariances.append(cov)
# 
#         X = np.concatenate(covariances, axis=0)
#         y = []
#         for i in range(len(covariances)):
#             y.extend(len(covariances[i])*[i])
# 
#         # TODO: do not predict proba, but generate labels for test cov matrices.
#         # TODO: combine those labels for every band, s.t. one label per recordings remains.
#         band_accs = []
#         for band_id in range(X.shape[1]):
#             # print(np.isfinite(X[:, band_id]).all())
#             band_accs.append(self.cross_validate(X[:, band_id], y))
#         return np.array(band_accs)
# 
#     def run(self, X, y):
#         return self.cross_validate(self.estimate_covariances(X, y), y)
# 
#     def estimate_covariances(self, X, y=None):
#         cov = pyriemann.estimation.Covariances().fit_transform(X)
#         logger.info("pyriemann covs done")
#         return cov
# 
#     def cross_validate(self, cov, y):
#         mdm = pyriemann.classification.MDM()
#         acc = cross_val_score(mdm, cov, y, cv=StratifiedKFold(n_splits=10))
#         return acc


# ______________________________________________________________________________________________________________________
def read_covariances_of_class_and_fold(edf_file_ids, covariances_file, out_dir):
    covariances = []
    h5_reader = Hdf5Reader(out_dir)
    for edf_id in edf_file_ids:
        covariance = h5_reader.read_domain_hdf5_successively(covariances_file, str(edf_id))
        covariances.append(covariance)

    covariances = np.array(covariances)
    logger.info("read covariances shape {}".format(covariances.shape))
    # TODO: use Riemannian mean here?
    covariances = np.mean(covariances, axis=0)
    logger.info("agged covariances shape {}".format(covariances.shape))
    return covariances


# ______________________________________________________________________________________________________________________
def parallel_cleaning(item, cl):
    edf_id, edf_file = item
    raw_cleaned = cl.clean(edf_file)
    return edf_id, raw_cleaned


# ______________________________________________________________________________________________________________________
def parallel_cleaning_filtering(item, cl):
    edf_id, edf_file = item
    raw_cleaned = cl.clean(edf_file)
    filtered = []
    for band_id, (low, high) in enumerate(cl.get_bands()):
        band_filtered = cl.band_filter_to_specific_band2(raw_cleaned, low, high)
        print(band_filtered.get_data().shape, sys.getsizeof(band_filtered))
        filtered.append(band_filtered.get_data())

    return edf_id, np.array(filtered)


# ______________________________________________________________________________________________________________________
def band_filter_edfs_of_both_classes(all_edf_files, pe, cl, output):
    for class_id, class_edf_files in enumerate(all_edf_files):
        edf_ids, band_filtered = pe.run(in_q_list=class_edf_files,
                                        parallel_function=parallel_cleaning_filtering,
                                        cl=cl)

        # h5_writer = Hdf5Writer()
        # for edf_id, edf_file in enumerate(class_edf_files):
        #     h5_writer.write_domain_hdf5_successively(path.join(output, str(class_id)+'_cleaned_filtered.h5'),
        #                                              band_filtered[edf_ids[edf_id]], edf_id)


# ______________________________________________________________________________________________________________________
def clean_edfs_of_both_classes(all_edf_files, pe, cl, output):
    # clean all recordings of both classes
    for class_id, class_edf_files in enumerate(all_edf_files):
        edf_ids, raw_cleaned = pe.run(in_q_list=class_edf_files,
                                      parallel_function=parallel_cleaning,
                                      cl=cl)

        # # write edfs with prefix _cleaned.edf to same location as original edf files
        for edf_id, edf_file in enumerate(class_edf_files):
            # create same subdirs in output directory
            print('/'.join(edf_file.split('/')[-9:-1]))
            sub_dir_path = '/'.join(edf_file.split('/')[-9:-1])
            file_name = path.split(edf_file)[1]
            file_name = file_name[:-4]+"_cleaned.edf"
            new_path = path.join(output, sub_dir_path)
            print(new_path)
            new_file_path = path.join(new_path, file_name)
            makedirs(new_path, exist_ok=True)
            # file_name = edf_files[edf_ids[edf_id]].split('/')[-1]
            raw_cleaned[edf_ids[edf_id]].save(new_file_path, verbose='error')


# ______________________________________________________________________________________________________________________
def train_edf_ids(train_ids, train_labels, test_ids, test_labels):
    # read the entries wrt cv fold from covariance matrices
    normal_train_edf_ids = train_ids[train_labels == 0]
    normal_test_edf_ids = test_ids[test_labels == 0]
    offset = len(normal_train_edf_ids) + len(normal_test_edf_ids)

    # the ids are subsequent. so fix ids of 2nd class by subtracting amount of first class labels
    abnormal_train_edf_ids = train_ids[train_labels == 1]
    abnormal_train_edf_ids = [edf_id - offset for edf_id in abnormal_train_edf_ids]
    return normal_train_edf_ids, abnormal_train_edf_ids


# ______________________________________________________________________________________________________________________
def read_rec_names_of_both_classes(classes, inputs, limit=None):
    # 1: read all files in the specified paths and compute covariance matrices for all input classes.
    # store them to file
    all_edf_files = []
    edf_reader = EdfReader()
    for class_id, class_ in enumerate(classes):
        edf_files = edf_reader.read_all_file_names(inputs[class_id], '.edf')
        all_edf_files.append(edf_files[:limit])

    return all_edf_files


# ______________________________________________________________________________________________________________________
def create_labels(all_edf_files):
    labels_flat, all_edf_files_flat = [], []
    for class_id, edf_files in enumerate(all_edf_files):
        labels_flat.extend(len(edf_files) * [class_id])
        all_edf_files_flat.extend(edf_files)
    labels_flat = np.array(labels_flat)
    all_edf_files_flat = np.array(all_edf_files_flat)
    return labels_flat, all_edf_files_flat


# ______________________________________________________________________________________________________________________
def perform_csp_cv(cmd_args, all_edf_files, labels, covariances_files, pe, cl, sp):
    # 2: create cv splits and read covariance matrices of each split. use train split covariances to generate
    # the csp. apply the csp to train as well as test data. perform cross-validation
    cv_accuracies = []
    h5_writer = Hdf5Writer(cmd_args.output)
    csp_computer = CspComputer()
    # csp = pyriemann.spatialfilters.CSP(nfilter=4, metric="euclid", log=True)
    skf = StratifiedKFold(n_splits=10)
    for fold_id, (train_ids, test_ids) in enumerate(skf.split(all_edf_files, labels)):
        logger.info("fold {}".format(fold_id))

        train_edfs = all_edf_files[train_ids]
        train_labels = labels[train_ids]
        test_edfs = all_edf_files[test_ids]
        test_labels = labels[test_ids]

        normal_train_edf_ids, abnormal_train_edf_ids = train_edf_ids(train_ids, train_labels, test_ids, test_labels)
        c1 = read_covariances_of_class_and_fold(normal_train_edf_ids, covariances_files[0], cmd_args.output)
        c2 = read_covariances_of_class_and_fold(abnormal_train_edf_ids, covariances_files[1], cmd_args.output)

        print(c1.shape, c2.shape, len(train_labels))
        # X = np.concatenate(c1, c2, axis=1)
        #
        # csp = csp.fit(X=X, y=y)
        # v, patterns = csp.filters_, csp.patterns_

        v, a, d = csp_computer.calculate_csp_decorator(c1, c2, cl.get_bands())
        # store csps for later visualization
        h5_writer.write_domain_hdf5_successively("csp"+str(fold_id)+".h5", v, 0)
        logger.info("computed csp")

        csp_filterer = CspFilterer(v)
        te_ids, test = pe.run(in_q_list=test_edfs, parallel_function=csp_filterer.parallel_application_of_csp,
                              cl=cl, sp=sp, filters=cmd_args.filters, agg=cmd_args.agg, csp_filterer=csp_filterer)
        test = np.array(test)
        logger.debug("test shape {}".format(test.shape))

        tr_ids, train = pe.run(in_q_list=train_edfs, parallel_function=csp_filterer.parallel_application_of_csp,
                               cl=cl, sp=sp, filters=cmd_args.filters, agg=cmd_args.agg, csp_filterer=csp_filterer)
        train = np.array(train)
        logger.debug("train shape {}".format(train.shape))

        # scale to zero mean and unit variance based on train data. apply to train and test data
        scaler = StandardScaler()
        scaler = scaler.fit(train)
        train = scaler.transform(train)
        test = scaler.transform(test)

        # create a support vector machine, train and predict
        clf = SVC(kernel='rbf', random_state=SEED, probability=True)
        clf.fit(train, train_labels)
        predicitions = clf.predict(test)
        accuracy = accuracy_score(test_labels, predicitions)
        logger.info("acc: {}".format(accuracy))
        cv_accuracies.append(accuracy)

    logger.info("mean cv acc: {}".format(np.mean(cv_accuracies)))


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    logger.create_loggers(cmd_args.output, cmd_args.verbosity)
    logger.log_cmd_args(cmd_args)

    # 0: parse cmd args and set up pe, cl and sp
    # TODO: parse this from input directory path
    classes = ["normal", "abnormal"]
    cmd_args.agg = getattr(np, cmd_args.agg) if cmd_args.agg != "none" else None

    # this is the girl who who is great at handling several things at the same time
    pe = parallel_executor.ParallelExecutor(cmd_args.n_proc)
    # this is the guy who likes dirty things
    cl = cleaner.Cleaner(bands=cmd_args.bands,
                         elecs=cmd_args.elecs,
                         band_overlap=cmd_args.f_overlap,
                         cut_time=cmd_args.cut)
    # this is the guy with a giant axe
    sp = splitter.Splitter(overlap=cmd_args.t_overlap,
                           window=cmd_args.window,
                           window_length=cmd_args.windowsize)

    all_edf_files = read_rec_names_of_both_classes(classes, cmd_args.input, limit=None)
    # TODO: first clean all the recordings and save them to file?
    # clean_edfs_of_both_classes(all_edf_files, pe, cl, cmd_args.output)
    # band_filter_edfs_of_both_classes(all_edf_files, pe, cl, cmd_args.output)
    # exit()
    covariance_computer = CovarianceComputer()
    covariances_files = covariance_computer.compute_covariances_of_both_classes(classes, cmd_args.output,
                                                                                all_edf_files, pe, cl, sp)

    # pyr = PyRiemann()
    # acc = pyr.cross_validate_decorator([path.join(cmd_args.output, s) for s in covariances_files])
    # logger.info("mean cv acc: {}".format(acc.mean()))

    labels_flat, all_edf_files_flat = create_labels(all_edf_files)
    perform_csp_cv(cmd_args, all_edf_files_flat, labels_flat, covariances_files, pe, cl, sp)


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    parser = arg_parser.ArgParser()
    cmd_arguments, not_known = parser.parse_known_args()
    main(cmd_arguments, not_known)
