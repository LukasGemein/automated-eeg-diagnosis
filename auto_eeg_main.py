#!/usr/bin/env python3.5
from datetime import datetime, date

from utils import arg_parser, logger
from training import trainer
import pipeline


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    today, now = date.today(), datetime.time(datetime.now())
    logger.create_loggers(cmd_args.output, cmd_args.verbosity)
    logger.info('\tStarted on {} at {}.'.format(today, now))

    if not_known:
        logger.error('\tFollowing parameters could not be interpreted: {}.'.format(not_known))

    # test numbers of estimators
    # n_estimators_to_test = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 250, 500, 1000, 2000]
    # for n_estimators in n_estimators_to_test:
    #     print("n_estimators: {}".format(n_estimators))
    #     pl = pipeline.Pipeline(
    #         trainer.Trainer(
    #             clf_name=cmd_args.clf,
    #             n_jobs=cmd_args.n_proc,
    #             agg=cmd_args.agg,
    #             n_estimators=n_estimators
    #         )
    #     )
    #     pl.run(cmd_args)

    # test number of pca components
    # n_pca_components_to_test = [
    #     1,    3,    5,    7,    9,   11,   13,   15,   17,   19,   21,
    #     23,   25,   27,   29,   31,   33,   35,   37,   39,   41,   43,
    #     45,   47,   49,   51,   53,   55,   57,   59,   61,   63,   65,
    #     100, 1000, 2500, 9788]
    #
    # for n_components in n_pca_components_to_test:
    #     print("n_components: {}".format(n_components))
    #     pl = pipeline.Pipeline(
    #         trainer.Trainer(
    #             clf_name=cmd_args.clf,
    #             n_jobs=cmd_args.n_proc,
    #             agg=cmd_args.agg,
    #             n_estimators=250,
    #             pca_comp=n_components
    #         )
    #     )
    #     pl.run(cmd_args)

    # create ml pipeline and run it
    pl = pipeline.Pipeline(
        trainer.Trainer(
            clf_name=cmd_args.clf,
            n_jobs=cmd_args.n_proc,
            n_estimators=250,
        )
    )
    pl.run(cmd_args)

    logger.info('\tFinished on {} at {}.\n\n'.format(date.today(), datetime.time(datetime.now())))


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    parser = arg_parser.ArgParser()
    cmd_arguments, unknown = parser.parse_known_args()
    main(cmd_arguments, unknown)
