from scipy import linalg
import numpy as np

from feature_generation import abstract_feature_generator


class CSPFeatureGenerator(abstract_feature_generator.AbstractFeatureGenerator):
    """ computes features in the frequency domain implemented in features_frequency using fourier transform """
    def get_feature_labels(self):
        """
        :return: list of feature labels of the form <csp>_<lower-upperHz>_<channel surrogate>
        """
        feature_labels = []
        for electrode_surrogate in self.electrodes:
            for band_id, band in enumerate(self.bands):
                lower, upper = band
                label = '_'.join([self.domain, str(lower) + '-' + str(upper) + 'Hz', 'SE' + str(electrode_surrogate)])
                feature_labels.append(label)

        return feature_labels

    def get_feature_names(self):
        """
        :return: basically a list with shortened names from above in the form <csp>
        """
        return [feat for feat in self.csp_feats]

    def compute_cov_matrix_decorator(self, raw_cleaned, cl, sp, average_trial_covariance=True):
        signals = raw_cleaned.get_data()
        # TODO: store band signals, since they can be used in application of cps?
        covar_matrix_by_band = []
        for band_id, band in enumerate(self.bands):
            lower, upper = band
            band_signals = cl.band_filter_to_specific_band(
                signals=signals,
                sfreq=self.sfreq,
                lower=lower,
                upper=upper
            )  # n_chnnls x n_times

            if average_trial_covariance:
                band_signals = sp.split(
                    signals=band_signals,
                    sfreq=self.sfreq,
                    weight=False
                )  # n_epochs x n_channels x n_times
            cov_matrix = self.compute_cov_matrix(
                epochs=band_signals,
                average_trial_covariance=average_trial_covariance
            )
            covar_matrix_by_band.append(cov_matrix)

        return np.array(covar_matrix_by_band)  # n_bands x n_channels x n_channels

    def compute_cov_matrix(self, epochs, average_trial_covariance=True):
        """ compute covariance of all electrodes for the current recording
        :param average_trial_covariance: whether or not to compute covariance matrix as mean over all trials
        :return: ndarray with cov matrix in shape of n_bands x n_elecs x n_elecs
        """
        # compute covariance matrix either on 'trials', here time windows of the signals or on the whole signals
        if average_trial_covariance:
            covs = np.array([np.cov(x) for x in epochs])
            cov_matrix = np.mean(covs, axis=0)
            # cov_matrix = self.mean_riemann(covs)
        else:
            cov_matrix = np.cov(epochs)  # n_channels x n_channels

        return cov_matrix

    def calculate_csp_decorator(self, covar_matrix1, covar_matrix2, freq_bands):
        """
        :param covar_matrix1: normal covariance matrix (n_bands x n_elecs x n_elecs)
        :param covar_matrix2: abnormal covariance matrix (n_bands x n_elecs x n_elecs)
        :param freq_bands: frequency bands
        :return: csp filters, csp patterns and xxx in all freq bands
        """
        band_vs, band_as, band_ds = [], [], []
        for band_id, freq_band in enumerate(freq_bands):
            band_v, band_a, band_d = self.calculate_csp(
                covar_matrix1[band_id],
                covar_matrix2[band_id]
            )
            band_vs.append(band_v)
            band_as.append(band_a)
            band_ds.append(band_d)

        return np.array(band_vs), np.array(band_as), np.array(band_ds)

    def calculate_csp(self, covar_matrix1, covar_matrix2):
        """ use robin/lukas'/joos' implementation to compute the common spatial pattern given two covariance matrices.
        they were computed in a previous run.
        :param covar_matrix1: normal covariance matrix (n_bands x n_elecs x n_elecs)
        :param covar_matrix2: abnormal covariance matrix (n_bands x n_elecs x n_elecs)
        :return: filters
        """
        # solution of csp objective via generalized eigenvalue problem
        # in matlab the signature is v, d = eig(a, b)
        d, v = linalg.eigh(covar_matrix2, covar_matrix1 + covar_matrix2)
        d = d.real
        # make sure the eigenvalues and -vectors are correctly sorted
        indx = np.argsort(d)
        # reverse
        indx = indx[::-1]
        d = d.take(indx)
        v = v.take(indx, axis=1)

        # Now compute patterns
        # old pattern computation
        # a = sp.linalg.inv(v).transpose()
        c_avg = (covar_matrix1 + covar_matrix2) / 2.0

        # compare
        # https://github.com/bbci/bbci_public/blob/c7201e4e42f873cced2e068c6cbb3780a8f8e9ec/processing/proc_csp.m#L112
        # with W := v
        v_with_cov = np.dot(c_avg, v)
        source_cov = np.dot(np.dot(v.T, c_avg), v)

        a = linalg.solve(source_cov.T, v_with_cov.T).T
        return v, a, d

    def generate_features(self, windows, cl, sp, average_trial_covariance=True):
        """ computes all frequency domain features as implemented in module features_frequency
        :param windows: ndarray with split eeg data in shape of n_windows x n_elecs x n_samples_in_window
        :return: ndarray of features in shape [n_windows x] n_elecs x n_bands x n_freq_features
        """
        if not self.covariance_matrices:
            cov_matrix_by_band = self.compute_cov_matrix_decorator(
                raw_cleaned=windows,
                average_trial_covariance=average_trial_covariance,
                cl=cl,
                sp=sp
            )
            return cov_matrix_by_band

        else:
            [covar_matrix1, covar_matrix2] = self.covariance_matrices
            csp_feats = self.calculate_csp_decorator(
                covar_matrix1=covar_matrix1,
                covar_matrix2=covar_matrix2,
                freq_bands=cl.get_bands()
            )

            # aggregate over the dimension of epochs
            if self.agg_mode:
                csp_feats = self.agg_mode(csp_feats, axis=0)

            return csp_feats

    def __init__(self, domain, elecs, agg, sfreq, bands, filters, covariance_matrices=None):
        abstract_feature_generator.AbstractFeatureGenerator.__init__(self, domain=domain, electrodes=elecs, agg_mode=agg)
        self.csp_feats = ["csp"]
        self.bands = bands
        self.filters = filters
        self.sfreq = sfreq

        self.covariance_matrices = covariance_matrices
