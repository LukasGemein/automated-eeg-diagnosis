import numpy as np

from feature_generation import abstract_feature_generator


class MetaFeatureGenerator(abstract_feature_generator.AbstractFeatureGenerator):
    """ computes features in the time domain implemented in features_time """

    def get_feature_labels(self):
        """
        :return: list of feature labels of the form <domain>_<feature>_<channel>
        """
        return self.get_feature_names()

    def get_feature_names(self):
        """
        :return: basically a list with shortened names from above in the form <domain>_<name>
        """
        return [self.domain + '_' + feat for feat in self.patient_feats]

    def generate_features(self, edf_file):
        """ computes all time domain features specified by self.time_feats and implemented in features_time.py
        :param edf_file: file path to edf which is not loaded and cleaned (and does not have to be)
        :return: patient features
        """
        tmp_file = edf_file.split('.')[-2]
        tokens = tmp_file.split('_')
        age = int(tokens[tokens.index("age") + 1])
        gender = tokens[tokens.index("gender") + 1]
        # 0 is male, everything else will be female
        gender = 0 if gender == 'M' else 1

        patient_feats = np.array([age, gender])
        return patient_feats

    def __init__(self, elecs, agg, domain="meta"):
        abstract_feature_generator.AbstractFeatureGenerator.__init__(
            self, domain=domain, electrodes=elecs, agg_mode=agg)
        self.patient_feats = ["age", "gender"]
