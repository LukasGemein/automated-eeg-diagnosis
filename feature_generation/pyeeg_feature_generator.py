import numpy as np

from feature_generation import abstract_feature_generator
from feature_generation import features_pyeeg as pyeeg


# TODO: improve pyeeg feature implementation. found two more sources using pyeeg as basis
# https://github.com/gilestrolab/pyrem/blob/master/src/pyrem/univariate.py
# https://github.com/akloster/python-mindwave/blob/master/mindwave/pyeeg.py
# TODO: who vectorized the functions. also, right now with error-corrected pyeeg
# TODO: implementation, fisher information and svd_entropy are the same
class PyeegFeatureGenerator(abstract_feature_generator.AbstractFeatureGenerator):
    """ computes features in time and frequency domain implemented in features_pyeeg using fourier transform """

    def get_feature_labels(self):
        """
        :return: list of feature labels of the form <domain>_<feature>_[<lower-upperHz>_]<channel>
        """
        feature_labels = []
        for pyeeg_feat in self.pyeeg_feats:
            if pyeeg_feat == "power" or pyeeg_feat == 'power_ratio':
                for band_id, band in enumerate(self.bands):
                    lower, upper = band
                    for electrode in self.electrodes:
                        label = '_'.join([self.domain, pyeeg_feat, str(lower) + '-' + str(upper)
                                          + 'Hz', str(electrode)])
                        feature_labels.append(label)
            else:
                for electrode in self.electrodes:
                    label = '_'.join([self.domain, pyeeg_feat, str(electrode)])
                    feature_labels.append(label)
        return feature_labels

    def get_feature_names(self):
        """
        :return: basically a list with shortened names from above in the form <domain>_<name>
        """
        return [self.domain + '_' + feat for feat in self.pyeeg_feats]

    def generate_features(self, weighted_windows, windows, axis=2):
        """ computes all pyeeg domain features
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :param windows: ndarray with split eeg data in shape of n_windows x n_elecs x n_samples
        :return: ndarray with pyeeg features in shape of n_windows x n_elecs x n_bands * 2  + 6
        """
        n_windows, n_elecs, n_samples_in_window = windows.shape
        # computes 7*n_elec + 2*n_bands*n_elecs values
        results = np.apply_along_axis(
            func1d=pyeeg.bin_power,
            axis=axis,
            arr=weighted_windows,
            Band=self.bands,
            Fs=self.sfreq
        )
        # n_windows x n_elecs x n_freq_feats, n_bands
        results = np.swapaxes(results, 1, 3)
        power, power_ratio = results[:, :, 0, :], results[:, :, 1, :]

        petrosian_fractal_dimension = np.apply_along_axis(
            func1d=pyeeg.pfd,
            axis=axis,
            arr=windows
        )

        results = np.apply_along_axis(
            func1d=pyeeg.hjorth,
            axis=axis,
            arr=windows
        )
        mobility, complexity = results[:, :, 0], results[:, :, 1]

        spectral_entropy = np.apply_along_axis(
            func1d=pyeeg.spectral_entropy,
            axis=axis,
            arr=weighted_windows,
            Band=self.bands,
            Fs=self.sfreq
        )

        fisher_information = np.apply_along_axis(
            func1d=pyeeg.fisher_info,
            axis=axis,
            arr=windows,
            Tau=self.TAU,
            DE=self.DE,
            W=self.W
        )

        hurst = np.apply_along_axis(
            func1d=pyeeg.hurst,
            axis=axis,
            arr=windows,
        )

        lyapunov = np.apply_along_axis(
            func1d=pyeeg.LLE,
            axis=axis,
            arr=windows,
            tau=self.TAU,
            n=self.n,
            T=self.T,
            fs=self.sfreq
        )

        # data has to be at least 512 samples long. why?
        detrended_fluctuation = np.apply_along_axis(
            func1d=pyeeg.dfa,
            axis=axis,
            arr=windows,
        )

        # svd_entropy = np.apply_along_axis(
        #     func1d=pyeeg.svd_entropy,
        #     axis=axis,
        #     arr=windows,
        #     Tau=self.TAU,
        #     DE=self.DE
        # )

        higuchi_fractal_dim = np.apply_along_axis(
            func1d=pyeeg.hfd,
            axis=axis,
            arr=windows,
            Kmax=self.Kmax
        )

        power = power.reshape(n_windows, -1)
        power_ratio = power_ratio.reshape(n_windows, -1)
        # expand dimensions for concatenation purposes
        complexity = complexity.reshape(n_windows, -1)
        fisher_information = fisher_information.reshape(n_windows, -1)
        mobility = mobility.reshape(n_windows, -1)
        petrosian_fractal_dimension = petrosian_fractal_dimension.reshape(n_windows, -1)
        spectral_entropy = spectral_entropy.reshape(n_windows, -1)
        hurst = hurst.reshape(n_windows, -1)
        lyapunov = lyapunov.reshape(n_windows, -1)
        detrended_fluctuation = detrended_fluctuation.reshape(n_windows, -1)
        #svd_entropy = svd_entropy.reshape(n_windows, -1)
        higuchi_fractal_dim = higuchi_fractal_dim.reshape(n_windows, -1)

        pyeeg_features = np.concatenate((
            power,
            power_ratio,
            spectral_entropy,
            complexity,
            detrended_fluctuation,
            fisher_information,
            higuchi_fractal_dim,
            hurst,
            lyapunov,
            mobility,
            petrosian_fractal_dimension,
            #svd_entropy
        ), axis=-1)

        if self.agg_mode:
            pyeeg_features = self.agg_mode(pyeeg_features, axis=0)

        return pyeeg_features

    def __init__(self, domain, elecs, agg, bands, sfreq):
        abstract_feature_generator.AbstractFeatureGenerator.__init__(self, domain=domain, electrodes=elecs, agg_mode=agg)
        pyeeg_freq_feats = [
            "power",
            "power_ratio",
            "spectral_entropy"
        ]
        pyeeg_time_feats = [
            "complexity",
            "detrended_fluctuation",
            "fisher_info",
            "higuchi_fractal_dimension",
            "hurst",
            "lyapunov",
            "mobility",
            "petrosian_fractal_dimension",
            #"svd_entropy"
        ]
        self.pyeeg_feats = pyeeg_freq_feats + pyeeg_time_feats
        self.bands = bands
        self.sfreq = sfreq

        self.Kmax = 3
        self.n = 4
        self.T = 1
        self.TAU = 4
        self.DE = 10
        self.W = None
