from scipy import linalg
import numpy as np
import pywt
import os

from feature_generation import features_time_naive as features_time
from feature_generation import frequency_feature_generator
from feature_generation import features_pyeeg as pyeeg
from feature_generation import pyeeg_feature_generator
from feature_generation import time_feature_generator
from feature_generation import features_frequency
from feature_generation import features_wavelets
from feature_generation import features_sync
from utils.io import Hdf5Reader, Hdf5Writer
from utils import logger

DOMAINS = sorted([
    # 'csp',
    'cwt',
    'dwt',
    'fft',
    'patient',
    #'pyeeg',
    'sync',
    'time',
])


# TODO: test wt ratio feature
# TODO: check order of dwt and cwt levels
# TODO: reimplement pyeeg features?
# TODO: boundedvariation


class FeatureGenerator(object):
    """ """

# ______________________________________________________________________________________________________________________
    def generate_cwt_features(self, weighted_windows, sfreq):
        """
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :param sfreq: the sampling frequency of the signals
        :return:
        """
        (n_windows, n_elecs, n_samples_in_window) = weighted_windows.shape

        # that would be soooo nice... but its not working
        # coefficients = np.apply_along_axis(pywt.cwt, 2, weighted_windows, scales=self.cwt_scales250,
        #                                    wavelet=self.cwt_wavelet, sampling_period=1./sfreq)

        coefficients = np.ndarray(shape=(n_windows, n_elecs, len(self.cwt_scales250), n_samples_in_window))
        for window_id, window in enumerate(weighted_windows):
            for elec_id, elec in enumerate(window):
                coef, freqs = pywt.cwt(
                    data=elec,
                    scales=self.cwt_scales250,
                    wavelet=self.cwt_wavelet,
                    sampling_period=1./sfreq
                )
                coefficients[window_id][elec_id] = coef

        cwt_feats = np.ndarray(shape=(n_windows, n_elecs, len(self.wt_feats), len(self.cwt_scales250)))
        for wt_feat_id, wt_feat_name in enumerate(self.wt_feats):
            if wt_feat_name == "ratio":
                powers = cwt_feats[:, :, self.wt_feats.index("power")]
                # divide the power by the sum of powers in each band to gain power ratio feature
                feats = powers / np.sum(powers, axis=2, keepdims=True)
            else:
                func = getattr(features_wavelets, wt_feat_name)
                feats = func(coefficients, axis=3)
            cwt_feats[:, :, wt_feat_id] = feats

        cwt_feats = np.reshape(cwt_feats, (n_windows, n_elecs, len(self.cwt_scales250) * len(self.wt_feats)))

        if self.agg:
            cwt_feats = self.agg(cwt_feats, axis=0)

        return cwt_feats

# ______________________________________________________________________________________________________________________
    def generate_dwt_features(self, weighted_windows):
        """ computes all dwt domain features
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :return: ndarray of features in shape n_windows x n_elecs x n_features x n_bands
        """
        (n_windows, n_elecs, n_samples_in_window) = weighted_windows.shape
        dwt_feats = np.ndarray(shape=(n_windows, n_elecs, len(self.wt_feats), self.dwt_level + 1))
        # given a sampling frequency of 250 hz we will use a level 6 dwt, since
        # 125 - 65.5 - 32.75 - 16.375 - 8.1875 - 4.09375 - (new) 2.046875
        multi_level_coeffs = pywt.wavedec(weighted_windows, wavelet=self.dwt_wavelet, level=self.dwt_level, axis=2)

        for wt_feat_id, wt_feat_name in enumerate(self.wt_feats):
            if wt_feat_name == "ratio":
                powers = dwt_feats[:, :, self.wt_feats.index("power")]
                # divide the power by the sum of powers in each band to gain power ratio feature
                ratio = powers / np.sum(powers, axis=2, keepdims=True)
                dwt_feats[:, :, wt_feat_id] = ratio
            else:

                func = getattr(features_wavelets, wt_feat_name)
                for level_id, level_coeffs in enumerate(multi_level_coeffs):
                    level_feats = func(level_coeffs, axis=2)
                    dwt_feats[:, :, wt_feat_id, level_id] = level_feats

        dwt_feats = np.reshape(dwt_feats, (n_windows, n_elecs, (self.dwt_level + 1) * len(self.wt_feats)))

        if self.agg:
            dwt_feats = self.agg(dwt_feats, axis=0)

        return dwt_feats

# ______________________________________________________________________________________________________________________
    def generate_freq_features(self, weighted_windows, sfreq):
        """ computes all frequency domain features as implemented in module features_frequency
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :param sfreq: the sampling frequency of the signals
        :return: ndarray of features in shape n_windows x n_elecs x n_bands x n_freq_features
        """
        (n_windows, n_elecs, n_samples_in_window) = weighted_windows.shape
        freq_feats = np.ndarray(shape=(n_windows, n_elecs, len(self.bands), len(self.freq_feats)))

        windows_amplitudes = np.abs(np.fft.rfft(weighted_windows, axis=2))
        (n_windows, n_elecs, n_freqbins) = windows_amplitudes.shape
        freq_bin_size = sfreq / n_samples_in_window

        for freq_feat_id, freq_feat_name in enumerate(self.freq_feats):
            func = getattr(features_frequency, freq_feat_name)
            # amplitudes shape: windows x electrodes x frequencies
            amplitude_features = np.ndarray(shape=(n_windows, n_elecs, len(self.bands)))
            for band_id, (lower, upper) in enumerate(self.bands):
                lower_bin, upper_bin = int(lower / freq_bin_size), int(upper / freq_bin_size)
                band_amplitudes = windows_amplitudes[:, :, lower_bin:upper_bin]

                band_ampitude_features = func(band_amplitudes, axis=2)
                amplitude_features[:, :, band_id] = band_ampitude_features

            freq_feats[:, :, :, freq_feat_id] = amplitude_features

        if self.agg:
            freq_feats = self.agg(freq_feats, axis=0)

        return freq_feats

# ______________________________________________________________________________________________________________________
    def generate_patient_features(self, age, gender):
        """ computes all patient domain features
        :param age: the age of the patient
        :param gender: the gender of the patient
        :return: ndarray with patient features in shape of n_patient_features x ,
        """
        # this is never called. age and gender are already available in preprocessor, s.t. setup of multiprocessing and
        # loading of edf files is skipped for patient domain
        patient_feats = np.ndarray(shape=(len(self.patient_feats), ))
        patient_feats[0] = age
        patient_feats[1] = gender

        return patient_feats

# ______________________________________________________________________________________________________________________
    def generate_pyeeg_features(self, weighted_windows, windows, sfreq):
        """ computes all pyeeg domain features
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :param windows: ndarray with split eeg data in shape of n_windows x n_elecs x n_samples
        :param sfreq: the sampling frequency of the signals
        :return: ndarray with pyeeg features in shape of n_windows x n_elecs x n_bands * 2  + 6
        """
        # computes 7*n_elec + 2*n_bands*n_elecs values
        results = np.apply_along_axis(pyeeg.bin_power, 2, weighted_windows, Band=self.bands, Fs=sfreq)
        pwr, pwrr = results[:, :, 0, :], results[:, :, 1, :]
        pfd = np.apply_along_axis(pyeeg.pfd, 2, windows)
        results = np.apply_along_axis(pyeeg.hjorth, 2, windows)
        mblt, cmplxt = results[:, :, 0], results[:, :, 1]
        se = np.apply_along_axis(pyeeg.spectral_entropy, 2, weighted_windows, self.bands, sfreq)
        TAU, DE, W = 4, 10, None
        svd = np.apply_along_axis(pyeeg.svd_entropy, 2, windows, Tau=TAU, DE=DE, W=W)
        fi = np.apply_along_axis(pyeeg.fisher_info, 2, windows, Tau=TAU, DE=DE, W=W)

        # expand dimensions for concatenation purposes
        cmplxt = np.expand_dims(cmplxt, 2)
        fi = np.expand_dims(fi, 2)
        mblt = np.expand_dims(mblt, 2)
        pfd = np.expand_dims(pfd, 2)
        se = np.expand_dims(se, 2)
        svd = np.expand_dims(svd, 2)
        pyeeg_features = np.concatenate((pwr, pwrr, cmplxt, fi, mblt, pfd, se, svd), axis=-1)

        if self.agg:
            pyeeg_features = self.agg(pyeeg_features, axis=0)

        return pyeeg_features

# ______________________________________________________________________________________________________________________
    def generate_sync_features(self, signals, sfreq):
        """ computes all sync domain features implemented in module features_time
        :param signals: ndarray with raw, unfiltered and unsplit eeg signals (get band filtered during computation
        :param sfreq: the sampling frequency of the signals
        :return: ndarray with eeg sync features in shape of n_bands x n_elecs*(n_elecs-1)/2
        """
        phase_lock_values = features_sync.compute_features_sync(
            signals,
            agg=self.agg,
            splitter=self.splitter,
            sfreq=sfreq,
            bands=self.bands,
            cleaner=self.cleaner,
        )

        return phase_lock_values

# ______________________________________________________________________________________________________________________
    def apply_csp_var_log(self, signals, sfreq, filters, columns):
        """ apply the common spatial pattern filter to a signal
        :param signals: n_channels x n_times
        :param sfreq: sampling frequency of the current recording
        :param filters: n_bands x n_channels x n_channels
        :param columns: the columns to pick from the common spatial pattern filter
        :return: the log variance of signals in the specified frequency bands filtered by the selected common spatial
        patterns from projection matrix filters
        """
        # TODO: remove columns argument and apply all filters? could select filters when performing cv / training
        # n_bands x n_epochs x n_columns x n_times
        csp_filtered = self.apply_csp_fast(signals, sfreq, filters, columns)

        var_logged_band_csp_filtered = []
        for band_id, band in enumerate(self.bands):
            # n_epochs x _n_columns
            log_vars = [np.log(np.var(trial, axis=-1)) for trial in csp_filtered[band_id]]
            var_logged_band_csp_filtered.append(log_vars)

        # n_bands x n_epochs x n_columns
        var_logged_band_csp_filtered = np.asarray(var_logged_band_csp_filtered)
        logger.debug("var logged band csp filtered {}".format(var_logged_band_csp_filtered.shape))

        # n_bands x n_columns
        var_logged_band_csp_filtered = self.agg(var_logged_band_csp_filtered, axis=1)
        logger.debug("final shape after aggregation {}".format(var_logged_band_csp_filtered.shape))
        return var_logged_band_csp_filtered

# ______________________________________________________________________________________________________________________
    def apply_csp_fast(self, signals, sfreq, filt, columns):
        """
        :param signals: n_channels x n_times
        :param sfreq: sampling frequency of the signals
        :param filt: n_bands x n_channels x n_channels
        :param columns: the common spatial filters to be picked from the projection matrix filt
        :return: the signals in the specified frequency bands filtered by the selceted common spatial patterns from
        projection matrix filt. has shape: n_bands x n_epochs x n_columns x n_times
        """
        logger.debug("applying filter of shape {} to signals of shape {}".format(filt.shape, signals.shape))
        band_filtered = []
        for band_id, band in enumerate(self.bands):
            lower, upper = band
            band_signals = self.cleaner.band_filter_to_specific_band(signals, sfreq, lower, upper)  # (n_chls x n_times)
            epo = self.splitter.split(band_signals, sfreq, weight=False)  # (n_epochs x n_channels x n_times)

            # take all the filters
            if columns is not None:
                f = filt[band_id][:, columns]  # (n_channels x n_columns)
            else:
                f = filt[band_id]

            filtered = []
            for trial_i in range(len(epo)):
                # (n_times x n_channels) dot (n_channels x n_colums)
                this_filtered = np.dot(epo[trial_i].T, f)  # (n_times x n_columns)
                filtered.append(this_filtered.T)  # (n_columns x n_times)

            band_filtered.append(filtered)  # (n_epochs x n_columns x n_times)

        band_filtered = np.asarray(band_filtered)  # (n_bands x n_epochs x n_columns x n_times)

        # if self.agg:
        #     band_filtered = self.agg(band_filtered, axis=1)
        logger.debug("this shape {} is fine".format(band_filtered.shape))

        return band_filtered, ((band_filtered ** 2).mean(axis=-1))

# ______________________________________________________________________________________________________________________
    def compute_csp(self, band_c1s, band_c2s):
        """ use robin/joos implementation to compute the common spatial pattern given two covariance matrices. they were
        computed in a previous run.
        :param band_c1s: average normal covariance matrix (n_bands x n_elecs x n_elecs)
        :param band_c2s: aberage abnormal covariance matrix (n_bands x n_elecs x n_elecs)
        :return: filters
        """
        band_vs, band_as, band_ds = [], [], []
        for band_id, band in enumerate(self.bands):
            # n_elecs x n_elecs
            c1, c2 = band_c1s[band_id], band_c2s[band_id]
            # solution of csp objective via generalized eigenvalue problem
            # in matlab the signature is v, d = eig(a, b)
            d, v = linalg.eigh(c2, c1 + c2)
            d = d.real
            # make sure the eigenvalues and -vectors are correctly sorted
            indx = np.argsort(d)
            # reverse
            indx = indx[::-1]
            d = d.take(indx)
            v = v.take(indx, axis=1)

            # Now compute patterns
            # old pattern computation
            # a = sp.linalg.inv(v).transpose()
            c_avg = (c1 + c2) / 2.0

            # compare
            # https://github.com/bbci/bbci_public/blob/c7201e4e42f873cced2e068c6cbb3780a8f8e9ec/processing/proc_csp.m#L112
            # with W := v
            v_with_cov = np.dot(c_avg, v)
            source_cov = np.dot(np.dot(v.T, c_avg), v)

            a = linalg.solve(source_cov.T, v_with_cov.T).T
            band_vs.append(v)
            band_as.append(a)
            band_ds.append(d)

        # (n_bands x n_channels x n_channels), (n_bands x n_channels x n_channels), (n_bands x n_channels)
        return np.asarray(band_vs), np.asarray(band_as), np.asarray(band_ds)

# ______________________________________________________________________________________________________________________
    def compute_cov_matrix(self, signals, sfreq, average_trial_covariance=False):
        """ compute covariance of all electrodes for the current recording
        :param signals: ndarray with raw, unfiltered and unsplit eeg signals (get band filtered during computation)
        :param sfreq: the sampling frequency of the signals
        :param average_trial_covariance: whether or not to compute covariance matrix as mean over all trials
        :return: ndarray with cov matrix in shape of n_bands x n_elecs x n_elecs
        """
        covar_matrix_by_band = np.ndarray(shape=(len(self.bands), len(signals), len(signals)))
        for band_id, band in enumerate(self.bands):
            lower, upper = band
            band_signals = self.cleaner.band_filter_to_specific_band(signals, sfreq, lower, upper)  # n_chnnls x n_times

            # compute covariance matrix either on 'trials', here time windows of the signals or on the whole signals
            if average_trial_covariance:
                band_signals = self.splitter.split(band_signals, sfreq, weight=False)  # n_epochs x n_channels x n_times
                cov_matrix = np.mean([np.cov(x) for x in band_signals], axis=0)  # n_channels x n_channels
            else:
                cov_matrix = np.cov(band_signals)  # n_channels x n_channels
            covar_matrix_by_band[band_id] = cov_matrix

        return covar_matrix_by_band  # n_bands x n_channels x n_channels

# ______________________________________________________________________________________________________________________
    def generate_csp_features(self, signals, sfreq):
        """ hack for covariance matrices / csp computation / application
        :param signals: ndarray with raw, unfiltered and unsplit eeg signals (get band filtered during computation
        :param sfreq:
        :return:
        """
        # if the common spatial pattern was already computed, use it
        if os.path.exists("csp.h5"):
            logger.debug("found csp.h5")
            h5_reader = Hdf5Reader()
            # creates an entry for every 'edf file'. there is just one entry. pick it
            band_vs = h5_reader.read_domain_hdf5("csp.h5")[0]
            features = self.apply_csp_var_log(signals, sfreq, band_vs, columns=self.csp_filters)

        # if common spatial pattern is not yet computed but covariance matrices exist, compute it
        else:
            if os.path.exists("cov_normal.h5") and os.path.exists("cov_abnormal.h5"):
                logger.debug("found cov_normal.h5 and cov_abnormal.h5")
                h5_reader = Hdf5Reader()
                # n_files x n_bands x n_channels x n_channels
                band_c1s = h5_reader.read_domain_hdf5("cov_normal.h5")
                band_c2s = h5_reader.read_domain_hdf5("cov_abnormal.h5")

                # average covariances over recordings: n_bands x n_channels x n_channels
                band_c1s = np.mean(band_c1s, axis=0)
                band_c2s = np.mean(band_c2s, axis=0)

                # v: n_bands x n_channels x n_channels
                band_vs, band_as, band_ds = self.compute_csp(band_c1s, band_c2s)
                h5_writer = Hdf5Writer()
                h5_writer.write_domain_hdf5_successively("csp.h5", band_vs, "0")
                logger.debug("wrote csp to file")
                logger.debug("filter shape {}".format(band_vs.shape))
                # directly use and apply csp
                features = self.apply_csp_var_log(signals, sfreq, band_vs, columns=self.csp_filters)
                logger.debug("after application of csp features have shape {}".format(features.shape))

            # if common spatial pattern is not yet computed and covariances do not exist, compute them
            else:
                logger.debug("found nothing. computing covariance matrices")
                cov_matrices = self.compute_cov_matrix(signals, sfreq)  # n_bands x n_channels x n_channels
                logger.debug("covariance matrices have shape {}".format(cov_matrices.shape))
                features = cov_matrices

        return features

# ______________________________________________________________________________________________________________________
    def generate_time_features(self, windows):
        """ computes all time domain features specified by self.time_feats and implemented in features_time.py
        :param windows: ndarray with split eeg data in shape of n_windows x n_elecs x n_samples_in_window
        :return: ndarray with eeg time features in shape of n_windows x n_elecs x n_time_features
        """
        (n_windows, n_elecs, n_samples_in_window) = windows.shape
        time_feats = np.ndarray(shape=(n_windows, n_elecs, len(self.time_feats)))
        for time_feat_id, time_feat_name in enumerate(self.time_feats):
            func = getattr(features_time, time_feat_name)
            time_feats[:, :, time_feat_id] = func(windows, -1)

        if self.agg:
            time_feats = self.agg(time_feats, axis=0)

        return time_feats

# ______________________________________________________________________________________________________________________
    def generate_features(self, raw_cleaned):
        """ computes features. returns one feature vector per recording
        :param raw_cleaned: mne.io.Raw holding all relevant information of that recording
        :return: features of the specified domain computed on the current recording
        """
        sfreq = raw_cleaned.info["sfreq"]
        # TODO: check this scaling
        signals = raw_cleaned.get_data()  # * 1E+06  # convert to microvolts
        # print(np.mean(signals, axis=0))
        windows = self.splitter.split(signals, sfreq, weight=False)
        (n_windows, n_elecs, n_samples_in_window) = windows.shape
        weighted_windows = self.splitter.apply_window_function(windows, n_samples_in_window)

        if self.cwt_feat_flag:
            cwt_features = self.generate_cwt_features(weighted_windows, sfreq)
            features = cwt_features

        if self.dwt_feat_flag:
            dwt_features = self.generate_dwt_features(weighted_windows)
            features = dwt_features

        if self.freq_feat_flag:
            freq_feats = self.generate_freq_features(weighted_windows, sfreq)
            # ffg = frequency_feature_generator.FrequencyFeatureGenerator(domain="fft", elecs=self.electrodes,
            #                                                             agg=self.agg, bands=self.bands, sfreq=sfreq)
            # freq_feats, generation_time = ffg.generate_features(weighted_windows)
            features = freq_feats

        # this is not needed
        # if self.patient_feat_flag:
        #     patient_feats, generation_time = self.generate_patient_features(rec.age, rec.sex)
        #     features = patient_feats

        if self.pyeeg_feat_flag:
            # pyeeg_feats = self.generate_pyeeg_features(weighted_windows, windows, sfreq)

            pfg = pyeeg_feature_generator.PyeegFeatureGenerator(domain="pyeeg", elecs=self.electrodes,
                                                                agg=self.agg, bands=self.bands, sfreq=sfreq)
            pyeeg_feats = pfg.generate_features(weighted_windows, windows)
            features = pyeeg_feats

        if self.sync_feat_flag:
            sync_feats = self.generate_sync_features(signals, sfreq)
            features = sync_feats

        if self.time_feat_flag:
            time_feats = self.generate_time_features(windows)
            # tfg = time_feature_generator.TimeFeatureGenerator(domain="time", elecs=self.electrodes, agg=self.agg)
            # time_feats, generation_time = tfg.generate_features(windows)
            features = time_feats

        if self.csp_feat_flag:
            f = self.generate_csp_features(signals, sfreq)
            features = f

        return features

# ______________________________________________________________________________________________________________________
    def get_feature_labels(self):
        """ create a list of feature labels that can be used to identify the feature values later on """
        if self.feature_labels is None:
            feature_labels = list()

            # electrode names
            if self.electrodes is not None:
                electrodes = self.electrodes
            else:
                electrodes = np.linspace(1, 21, 21)
                electrodes = ['ch'+str(electrode) for electrode in electrodes]

            # cwt features
            if self.cwt_feat_flag:
                cwt_levels = ["a8", "d8", "d7", "d6", "d5", "d4", "d3", "d2", "d1"]
                for electrode in electrodes:
                    for cwt_feat in self.wt_feats:
                        if cwt_feat != 'ratio':
                            for level in cwt_levels:
                                feature_labels.append('_'.join(['cwt', cwt_feat, level, str(electrode)]))

                        else:
                            for level_id in range(len(cwt_levels)-1):
                                feature_labels.append(
                                    '_'.join(['cwt', cwt_feat, str(cwt_levels[level_id]) + '-' +
                                              str(cwt_levels[level_id + 1]), str(electrode)]))

            # dwt features
            if self.dwt_feat_flag:
                dwt_levels = ["a5", "d5", "d4", "d3", "d2", "d1"]
                for electrode in electrodes:
                    for dwt_feat in self.wt_feats:
                        if dwt_feat != 'ratio':
                            for level in dwt_levels:
                                feature_labels.append('_'.join(['dwt', dwt_feat, level, str(electrode)]))

                        else:
                            for level_id in range(len(dwt_levels)-1):
                                feature_labels.append(
                                    '_'.join(['dwt', dwt_feat, str(dwt_levels[level_id]) + '-' +
                                              str(dwt_levels[level_id + 1]), str(electrode)]))

            # fft features
            if self.freq_feat_flag:
                for electrode in electrodes:
                    for band_id, band in enumerate(self.bands):
                        lower, upper = band
                        for freq_feat in self.freq_feats:
                            label = '_'.join(['fft', freq_feat, str(lower) + '-' + str(upper) + 'Hz', str(electrode)])
                            feature_labels.append(label)

            # patient features
            if self.patient_feat_flag:
                for patient_feat in self.patient_feats:
                    feature_labels.append('_'.join(['patient', patient_feat]))

            # pyeeg features
            if self.pyeeg_feat_flag:
                for electrode in electrodes:
                    for pyeeg_feat in self.pyeeg_feats:
                        if pyeeg_feat == "pwr" or pyeeg_feat == 'pwrr':
                            for band_id, band in enumerate(self.bands):
                                lower, upper = band
                                label = '_'.join(['pyeeg', pyeeg_feat, str(lower) + '-' + str(upper)
                                                  + 'Hz', str(electrode)])
                                feature_labels.append(label)
                        else:
                            label = '_'.join(['pyeeg', pyeeg_feat, str(electrode)])
                            feature_labels.append(label)

            # synchronicity features
            if self.sync_feat_flag:
                for electrode_id, electrode in enumerate(electrodes):
                    for electrode_id2 in range(electrode_id+1, len(electrodes)):
                        for band_id, band in enumerate(self.bands):
                            lower, upper = band
                            label = '_'.join(['sync', 'plv', str(lower) + '-' + str(upper) + 'Hz',
                                              str(electrode) + '-' + electrodes[electrode_id2]])
                            feature_labels.append(label)

            # time features
            if self.time_feat_flag:
                for electrode in electrodes:
                    for time_feat in self.time_feats:
                        label = '_'.join(['time', time_feat, str(electrode)])
                        feature_labels.append(label)

            self.feature_labels = np.asarray(feature_labels)

        return self.feature_labels

# ______________________________________________________________________________________________________________________
    def get_feature_names(self):
        feature_names = []
        if self.cwt_feat_flag:
            feature_names.extend(['cwt_' + feat for feat in self.wt_feats])
        if self.dwt_feat_flag:
            feature_names.extend(['dwt_' + feat for feat in self.wt_feats])
        if self.freq_feat_flag:
            feature_names.extend(['fft_' + feat for feat in self.freq_feats])
        if self.patient_feat_flag:
            feature_names.extend(['patient_' + feat for feat in self.patient_feats])
        if self.pyeeg_feat_flag:
            feature_names.extend(['pyeeg_' + feat for feat in self.pyeeg_feats])
        if self.sync_feat_flag:
            feature_names.extend(['sync_' + feat for feat in self.sync_feats])
        if self.time_feat_flag:
            feature_names.extend(['time_' + feat for feat in self.time_feats])

        return feature_names

# ______________________________________________________________________________________________________________________
    def __init__(self, cleaner, splitter, domains, agg, filters):
        # all the features that are implemented
        self.patient_feats = ["age", "sex"]
        self.freq_feats = sorted([feat_func for feat_func in dir(features_frequency) if not feat_func.startswith('_')])
        self.time_feats = sorted([feat_func for feat_func in dir(features_time) if not feat_func.startswith('_')])
        pyeeg_freq_feats = ["pwr", "pwrr", "se"]
        pyeeg_time_feats = ["cmplxt", "fi", "mblt", "pfd", "svd"]  # , "hrst", "lle"]  # , "dfas"]
        self.pyeeg_feats = pyeeg_freq_feats + pyeeg_time_feats
        self.sync_feats = ['plv']
        self.wt_feats = sorted([feat_func for feat_func in dir(features_wavelets) if not feat_func.startswith('_')] +
                               ["ratio"])

        # toggle computation of feature sub groups
        self.used_domains = DOMAINS if 'all' in domains else domains
        self.cwt_feat_flag = True if 'cwt' in domains or 'all' in domains else False
        self.dwt_feat_flag = True if 'dwt' in domains or 'all' in domains else False
        self.freq_feat_flag = True if 'fft' in domains or 'all' in domains else False
        self.patient_feat_flag = True if 'patient' in domains or 'all' in domains else False
        self.pyeeg_feat_flag = True if 'pyeeg' in domains or 'all' in domains else False
        self.sync_feat_flag = True if 'sync' in domains or 'all' in domains else False
        self.time_feat_flag = True if 'time' in domains or 'all' in domains else False

        self.csp_feat_flag = True if 'csp' in domains or 'all' in domains else False

        # aggregation mode
        self.agg = getattr(np, agg) if agg != "none" else None

        # scales are used to create pseudo frequencies 2, 4, 8, 13, 18, 24, 30, 60, 120
        self.cwt_scales250 = [101.5, 50.75, 25.375, 15.622, 11.282, 8.463, 6.77, 3.3854, 1.6927]
        # given 250 hz sampling frequency this results to 125 - 65.5 - 32.75 - 16.375 - 8.1875 - 4.09375 - (new)2.046875
        self.dwt_level = 6
        self.dwt_wavelet = 'db4'
        self.cwt_wavelet = 'morl'

        self.csp_filters = filters

        self.splitter = splitter
        self.cleaner = cleaner

        self.bands = self.cleaner.get_bands()

        # information that is needed by other objects / written to file
        self.electrodes = self.cleaner.elecs
        self.feature_labels = None
        self.feature_names = None

