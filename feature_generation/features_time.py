# Written by Manuel Blum
# Adapted by Lukas Gemein
import numpy as _np

# TODO: implement more features
# TODO: TAKE CARE! DIFF INSERTS A ROW OF NaNs!


# Imagine that you have zero cookies, and you split them evenly among 
# zero friends. How many cookies does each person get? See? It doesn't 
# make sense. And Cookie Monster is sad that there are no cookies, and 
# you are sad that you have no friends.


def _div0(x):
    x[x < 1e-9] = 1e-9
    return x


def mean(df, window_size):
    return df.rolling(window=window_size).mean()


def median(df, window_size):
    return df.rolling(window=window_size).median()


def variance(df, window_size):
    return df.rolling(window=window_size).var()


def line_length(df, window_size):
    return df.diff().abs().rolling(window=window_size).sum()


def minimum(df, window_size):
    return df.rolling(window=window_size).min()


def maximum(df, window_size):
    return df.rolling(window=window_size).max()


def skew(df, window_size):
    return df.rolling(window=window_size).skew()


def kurtosis(df, window_size):
    return df.rolling(window=window_size).kurt()


def energy(df, window_size):
    return (df * df).rolling(window=window_size).mean()


def complexity(df, window_size):
    diff1 = df.diff()
    diff2 = diff1.diff()
    sigma = df.rolling(window=window_size).std()
    sigma1 = diff1.rolling(window=window_size).std()
    sigma2 = diff2.rolling(window=window_size).std()
    return (sigma2 / _div0(sigma1)) / _div0((sigma1 / _div0(sigma)))


def mobility(df, window_size):
    return df.diff().rolling(window=window_size).std() / _div0(df.rolling(window=window_size).std())


def non_linear_energy(df, window_size):
    return (_np.square(df[1:-1]) - df[2:].values * df[:-2].values).rolling(window=window_size).mean()


# this takes by far the most time in computing the time features
def fractal_dimension(df, window_size):
    sum_of_distances = _np.sqrt(df.diff() * df.diff()).rolling(window=window_size).sum()
    max_dist = df.rolling(window=window_size).apply(lambda df: _np.max(_np.sqrt(_np.square(df - df[0]))))
    return _np.log10(sum_of_distances) / _div0(_np.log10(max_dist))


def zero_crossing(df, window_size):
    epsilon = 0.01
    norm = df - df.mean()
    return ((norm[:-5] < epsilon) & (norm[5:].values > epsilon)).rolling(window=window_size).sum()


def zero_crossing_derivative(df, window_size):
    epsilon = 0.01
    diff = df.diff()
    norm = diff - diff.mean()
    return ((norm[:-5] < epsilon) & (norm[5:].values > epsilon)).rolling(window=window_size).sum()
