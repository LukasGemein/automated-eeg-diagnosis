import numpy as _np


def entropy(band_amplitudes, axis=-1):
    squared = band_amplitudes * band_amplitudes
    return - _np.sum(squared * _np.log2(squared), axis=axis)


def maximum(band_amplitudes, axis=-1):
    return _np.max(band_amplitudes, axis=axis)


def mean(band_amplitudes, axis=-1):
    return _np.mean(band_amplitudes, axis=axis)


def minimum(band_amplitudes, axis=-1):
    return _np.min(band_amplitudes, axis=axis)


def power(band_amplitudes, axis=-1):
    return _np.sum(band_amplitudes * band_amplitudes, axis=axis)


def value_range(band_amplitudes, axis=-1):
    return _np.ptp(band_amplitudes, axis=axis)


def variance(band_amplitudes, axis=-1):
    return _np.var(band_amplitudes, axis=axis)
