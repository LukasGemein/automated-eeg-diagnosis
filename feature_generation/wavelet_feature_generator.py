from pywt._extensions._pywt import DiscreteContinuousWavelet, ContinuousWavelet, Wavelet, _check_dtype
from pywt._functions import integrate_wavelet, scale2frequency
import numpy as np
import pywt

from feature_generation import abstract_feature_generator
from feature_generation import features_wavelets


# TODO: compute wavelet transform on complete signal and split into windows afterwards?
class WaveletFeatureGenerator(abstract_feature_generator.AbstractFeatureGenerator):
    """ computes features in the time-frequency domain implemented in features_wavelets using wavelet transforms """

    def get_feature_labels(self):
        """
        :return: list of feature labels of the form <domain>_<feature>_<channel>
        """
        feature_labels = []
        for wt_feat in self.wt_feats:
            for level in self.levels:
                for electrode in self.electrodes:
                    # TODO: instead of levels use frequencies? self.freqs
                    feature_labels.append('_'.join([self.domain, wt_feat, level, str(electrode)]))
        return feature_labels

    def get_feature_names(self):
        """
        :return: basically a list with shortened names from above in the form <domain>_<feature>
        """
        return [self.domain + '_' + feat for feat in self.wt_feats]

    # taken from pywt and adapted to not compute and return frequencies
    # this is avialable in new, separate function
    # like this, it can be applied using numpy.apply_along_axis
    def pywt_cwt(self, data, scales, wavelet):
        """
        cwt(data, scales, wavelet)
        One dimensional Continuous Wavelet Transform.
        Parameters
        ----------
        data : array_like
            Input signal
        scales : array_like
            scales to use
        wavelet : Wavelet object or name
            Wavelet to use
        Returns
        -------
        coefs : array_like
            Continous wavelet transform of the input signal for the given scales
            and wavelet
        Notes
        -----
        Size of coefficients arrays depends on the length of the input array and
        the length of given scales.
        Examples
        --------
        """
        # accept array_like input; make a copy to ensure a contiguous array
        dt = _check_dtype(data)
        data = np.array(data, dtype=dt)
        if not isinstance(wavelet, (ContinuousWavelet, Wavelet)):
            wavelet = DiscreteContinuousWavelet(wavelet)
        if np.isscalar(scales):
            scales = np.array([scales])
        if data.ndim == 1:
            if wavelet.complex_cwt:
                out = np.zeros((np.size(scales), data.size), dtype=complex)
            else:
                out = np.zeros((np.size(scales), data.size))
            for i in np.arange(np.size(scales)):
                precision = 10
                int_psi, x = integrate_wavelet(wavelet, precision=precision)
                step = x[1] - x[0]
                j = np.floor(
                    np.arange(scales[i] * (x[-1] - x[0]) + 1) / (scales[i] * step))
                if np.max(j) >= np.size(int_psi):
                    j = np.delete(j, np.where((j >= np.size(int_psi)))[0])
                coef = - np.sqrt(scales[i]) * np.diff(
                    np.convolve(data, int_psi[j.astype(np.int)][::-1]))
                d = (coef.size - data.size) / 2.
                out[i, :] = coef[int(np.floor(d)):int(-np.ceil(d))]
            #        frequencies = scale2frequency(wavelet, scales, precision)
            #        if np.isscalar(frequencies):
            #            frequencies = np.array([frequencies])
            #        for i in np.arange(len(frequencies)):
            #            frequencies[i] /= sampling_period
            #        return (out, frequencies)
            return out
        else:
            raise ValueError("Only dim == 1 supportet")

    def pywt_freqs(self, wavelet, scales, sampling_period=1.):
        """ taken from pywt.cwt and split into independent function. if frequencies are desired, returns them wrt
         wavelet, scales and sampling period of the signal """
        if not isinstance(wavelet, (ContinuousWavelet, Wavelet)):
            wavelet = DiscreteContinuousWavelet(wavelet)
        precision = 10
        frequencies = scale2frequency(wavelet, scales, precision)
        if np.isscalar(frequencies):
            frequencies = np.array([frequencies])
        for i in np.arange(len(frequencies)):
            frequencies[i] /= sampling_period
        return frequencies

    def generate_cwt_features(self, weighted_windows):
        """
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :return: ndarray of features in shape n_windows x n_elecs x n_features x n_bands
        """
        (n_windows, n_elecs, n_samples_in_window) = weighted_windows.shape
        coefficients = np.apply_along_axis(
            func1d=self.pywt_cwt, axis=2, arr=weighted_windows, scales=self.scales,
            wavelet=self.wavelet)
        # n_windows x n_elecs x n_levels x n_coefficients
        coefficients = np.swapaxes(coefficients, 1, 2)
        coefficients = np.abs(coefficients)

        cwt_feats = np.ndarray(shape=(n_windows, len(self.wt_feats),  len(self.scales), n_elecs))
        for wt_feat_id, wt_feat_name in enumerate(self.wt_feats):
            if wt_feat_name == "ratio":
                powers = cwt_feats[:, self.wt_feats.index("power"), :, :]
                # divide the power by the sum of powers in each band to gain power ratio feature
                feats = powers / np.sum(powers, axis=1, keepdims=True)
            else:
                func = getattr(features_wavelets, wt_feat_name)
                feats = func(coefficients=coefficients, axis=3)
            cwt_feats[:, wt_feat_id, :, :] = feats

        cwt_feats = cwt_feats.reshape(n_windows, -1)
        if self.agg_mode:
            cwt_feats = self.agg_mode(cwt_feats, axis=0)

        return cwt_feats

    def generate_dwt_features(self, weighted_windows):
        """ computes all wt domain features
        :param weighted_windows: ndarray with split eeg data weighted by a window function in shape of n_windows x
                                 n_elecs x n_samples_in_window
        :return: ndarray of features in shape n_windows x n_elecs x n_features x n_bands
        """
        (n_windows, n_elecs, n_samples_in_window) = weighted_windows.shape
        n_levels = len(self.levels)
        dwt_feats = np.ndarray(
            shape=(n_windows, len(self.wt_feats), n_levels, n_elecs)
        )
        # given a sampling frequency of 250 hz we will use a level 6 dwt, since
        # 125 - 62.5 - 31.25 - 15.625 - 7.8125 - 3.90625 - 1.953125
        # 50 - 25 - 12.5 - 6.25 - 3.125
        # list of length n_bands of ndarray: x n_epochs x n_channels x n_band_coeffs
        multi_level_coeffs = pywt.wavedec(
            data=weighted_windows, wavelet=self.wavelet, level=n_levels-1, axis=2)

        for wt_feat_id, wt_feat_name in enumerate(self.wt_feats):
            # assumes that "power" feature was already computed. which should be the case, since features are iterated
            # alphabetically
            if wt_feat_name == "ratio":
                powers = dwt_feats[:, self.wt_feats.index("power"), :, :]
                # divide the power by the sum of powers in each band to gain power ratio feature
                ratio = powers / np.sum(powers, axis=1, keepdims=True)
                dwt_feats[:, wt_feat_id, :, :] = ratio
            else:
                func = getattr(features_wavelets, wt_feat_name)
                # TODO: use apply_along_axis here?
                for level_id, level_coeffs in enumerate(multi_level_coeffs):
                    level_coeffs = np.abs(level_coeffs)
                    level_feats = func(coefficients=level_coeffs, axis=2)
                    dwt_feats[:, wt_feat_id, level_id, :] = level_feats

        dwt_feats = dwt_feats.reshape(n_windows, -1)
        if self.agg_mode:
            dwt_feats = self.agg_mode(dwt_feats, axis=0)

        # n_elecs x n_levels * n_feats
        return dwt_feats

    def generate_features(self, weighted_windows):
        if self.domain == "cwt":
            features = self.generate_cwt_features(weighted_windows)
        else:  # self.domain == "dwt":
            features = self.generate_dwt_features(weighted_windows)
        return features

    def __init__(self, domain, elecs, agg, sfreq, wavelet, levels, scales):
        abstract_feature_generator.AbstractFeatureGenerator.__init__(
            self, domain=domain, electrodes=elecs, agg_mode=agg)
        self.wt_feats = sorted([
            feat_func
            for feat_func in dir(features_wavelets)
            if not feat_func.startswith('_')] + ["ratio"])
        self.wavelet = wavelet
        self.levels = levels
        self.sfreq = sfreq
        self.scales = scales

        if self.domain == "cwt":
            self.freqs = self.pywt_freqs(wavelet=self.wavelet, scales=self.scales, sampling_period=1./self.sfreq)
        elif self.domain == "dwt":
            self.freqs = [(self.sfreq / 2) / (2 ** i) for i in range(0, len(self.levels))]
