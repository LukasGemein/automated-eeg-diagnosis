from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import KFold
import multiprocessing as mp
from pomegranate import *
import numpy as np
import logging

from feature_space import hmm_feature_manager
from visualization import plotting
from utils import io

NORMAL_LABEL = 0
ABNORMAL_LABEL = 1


class HMMTrainer(object):
    """ makes use of hmms to train and predict samples. one hmm is created for every electrode and every class. """

# ______________________________________________________________________________________________________________________
    def classify_sample(self, normal_model, abnormal_model, sample):
        """ use the fitted hmms to score a sample. highest score wins """
        # tie breaking in favor of normal
        # return NORMAL_LABEL if normal_model.score(sample) >= abnormal_model.score(sample) else ABNORMAL_LABEL
        return NORMAL_LABEL if normal_model.log_probability(sample) >= abnormal_model.log_probability(sample) \
            else ABNORMAL_LABEL

# ______________________________________________________________________________________________________________________
    def predict(self, normal_model, abnormal_model, test_data):
        """ for every test fold classify the sample in each test fold, store the winning class
        :param normal_models: list of normal models fitted by electrode
        :param abnormal_models: list of abnormal models fitted by electrode
        :param test_data: matrix holding test fold data
        :return: array of predictions
        """
        predictions = []
        for test_sample_id, test_sample in enumerate(test_data):
            predictions.append(self.classify_sample(normal_model, abnormal_model, test_sample))
        return np.asarray(predictions)

# ______________________________________________________________________________________________________________________
    def train_test_split(self, n_samples):
        """ generate ids to split the input feature matrix in train and test subsets """
        ids = np.arange(n_samples)
        train_fold_ids, test_fold_ids = [], []
        kf = KFold(n_splits=self.n_folds)
        for train, test in kf.split(ids):
            train_fold_ids.append(train)
            test_fold_ids.append(test)

        return np.asarray(train_fold_ids), np.asarray(test_fold_ids)

# ______________________________________________________________________________________________________________________
    def scale_features(self, normal_train, abnormal_train, normal_test, abnormal_test):
        """ standardize features """
        # this is very ugly. improve this!
        normal_train_shape = normal_train.shape
        (n_files, n_windows, n_features) = normal_train.shape
        normal_train = np.reshape(normal_train, (n_files*n_windows, n_features))

        abnormal_train_shape = abnormal_train.shape
        (n_files, n_windows, n_features) = abnormal_train.shape
        abnormal_train = np.reshape(abnormal_train, (n_files*n_windows, n_features))

        normal_test_shape = normal_test.shape
        (n_files, n_windows, n_features) = normal_test.shape
        normal_test = np.reshape(normal_test, (n_files*n_windows, n_features))

        abnormal_test_shape = abnormal_test.shape
        (n_files, n_windows, n_features) = abnormal_test.shape
        abnormal_test = np.reshape(abnormal_test, (n_files*n_windows, n_features))

        # fit the scaler on the very first train fold data, transform all other
        if self.scaler_obj is None:
            self.scaler_obj = StandardScaler(copy=False)
            normal_train = self.scaler_obj.fit_transform(normal_train)
        else:
            normal_train = self.scaler_obj.transform(normal_train)
        abnormal_train = self.scaler_obj.transform(abnormal_train)
        normal_test = self.scaler_obj.transform(normal_test)
        abnormal_test = self.scaler_obj.transform(abnormal_test)

        # reshape back to 2D for electrode indexing in train models...
        normal_train = np.reshape(normal_train, normal_train_shape)
        abnormal_train = np.reshape(abnormal_train, abnormal_train_shape)
        normal_test = np.reshape(normal_test, normal_test_shape)
        abnormal_test = np.reshape(abnormal_test, abnormal_test_shape)

        return normal_train, abnormal_train, normal_test, abnormal_test

# ______________________________________________________________________________________________________________________
    def train_and_predict_fold(self, normal_train, abnormal_train, normal_test, abnormal_test):
        """ trains n_elecs models for both normal and abnormal train data. evaluates the test data by scoring each
        sample with the normal and abnormal model of the elctrode. highest score wins. computes metrics accuracy,
        precision, recall and f1 score
        :return: results in shape n_classes x n_metrics
        """
        # standardize features
        normal_train, abnormal_train, normal_test, abnormal_test = self.scale_features(normal_train, abnormal_train,
                                                                                       normal_test, abnormal_test)

        # train normal and abnormal gmm-hmm
        normal_model = self.train_one_hmm(normal_train)
        abnormal_model = self.train_one_hmm(abnormal_train)

        # predict normal and abnormal test samples and label them wrt. higher probability from which model they were
        # generated
        normal_predictions = self.predict(normal_model, abnormal_model, normal_test)
        abnormal_predictions = self.predict(normal_model, abnormal_model, abnormal_test)

        # predict test samples and evaluate them
        predictions = np.concatenate((normal_predictions, abnormal_predictions), axis=0)
        labels = len(normal_predictions) * [NORMAL_LABEL] + len(abnormal_predictions) * [ABNORMAL_LABEL]

        # for l, p in zip(labels, predictions):
        #     print("true", l, "pred", p)

        acc = accuracy_score(labels, predictions)
        prec = precision_score(labels, predictions)
        rec = recall_score(labels, predictions)
        f1 = f1_score(labels, predictions)
        cm = confusion_matrix(labels, predictions)

        return [acc, prec, rec, f1], cm

# ______________________________________________________________________________________________________________________
    def worker(self, in_q, out_q, func):
        while not in_q.empty():
            normal_train, abnormal_train, normal_test, abnormal_test, fold_id = in_q.get(timeout=1)
            out_q.put((func(normal_train, abnormal_train, normal_test, abnormal_test), fold_id))

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ store results returned from multiprocessing in correct order through fold_id """
        results = out_q.qsize() * [None]
        cms = out_q.qsize() * [None]
        while not out_q.empty():
            (fold_results, cm), fold_id = out_q.get()
            results[fold_id] = fold_results
            cms[fold_id] = cm
        return results, cms

# ______________________________________________________________________________________________________________________
    def cross_validate(self, feature_matrices, train_ids, test_ids):
        """ in every cv fold fit an hmm per electrode on train fold data and predict test fold data
        :param xxx_data: feature matrix
        :param xxx_train_fold_ids: list of indices to pick the train folds from the feature matrix
        :param xxx_test_fold_ids: list of indices to pick the test folds from the feature matrix
        """
        [normal_data, abnormal_data] = feature_matrices
        [normal_train_fold_ids, abnormal_train_fold_ids] = train_ids
        [normal_test_fold_ids, abnormal_test_fold_ids] = test_ids

        if self.parallel_folds:
            # set up multiprocessing
            manager = mp.Manager()
            in_q, out_q = manager.Queue(), manager.Queue()
            processes = np.ndarray(shape=(self.n_folds,), dtype=mp.Process)

            # create n_proc many jobs to be processed in parallel where every process runs a single of n_folds cv folds
            worked_folds = 0
            while worked_folds < self.n_folds:
                for process_id in range(worked_folds, min(worked_folds+self.n_proc, self.n_folds)):
                    # grab train and test data from feature matrices according to train and test fold ids
                    normal_train = normal_data[normal_train_fold_ids[process_id]]
                    abnormal_train = abnormal_data[abnormal_train_fold_ids[process_id]]

                    normal_test = normal_data[normal_test_fold_ids[process_id]]
                    abnormal_test = abnormal_data[abnormal_test_fold_ids[process_id]]

                    # cv is parallelised. every subprocess worker predicts a fold
                    in_q.put((normal_train, abnormal_train, normal_test, abnormal_test, process_id-worked_folds))
                    processes[process_id] = mp.Process(target=self.worker, args=(in_q, out_q, self.train_and_predict_fold))

                for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_proc]):
                    logging.info("\tTraining and predicting fold {}.".format(worked_folds+process_id+1))
                    process.start()

                for process in processes[worked_folds:worked_folds+self.n_proc]:
                    process.join()

                metrics, cms = self.catch_results(out_q)
                self.results[worked_folds:worked_folds+self.n_proc] = metrics
                self.confusion_matrices[worked_folds:worked_folds+self.n_proc] = cms
                worked_folds += self.n_proc

        else:
            for fold_id in range(self.n_folds):
                logging.info("\tTraining and predicting fold {}.".format(fold_id+1))
                normal_train = normal_data[normal_train_fold_ids[fold_id]]
                abnormal_train = abnormal_data[abnormal_train_fold_ids[fold_id]]

                normal_test = normal_data[normal_test_fold_ids[fold_id]]
                abnormal_test = abnormal_data[abnormal_test_fold_ids[fold_id]]

                result, cm = self.train_and_predict_fold(normal_train, abnormal_train, normal_test, abnormal_test)
                self.results[fold_id] = result
                self.confusion_matrices[fold_id] = cm

# ______________________________________________________________________________________________________________________
    def train(self, cmd_args):
        """ read features from hdf5 file, split into train and test folds and perform 10 fold cv """
        window_counts, min_n_windows = self.feature_manager.retrieve_window_counts(cmd_args.input)
        class_names, rec_names, feature_matrices = [], [], []
        # load features and retrieve information whether time windows were agged as well as recording names
        for in_dir_id, in_dir in enumerate(cmd_args.input):
            class_name, domains, per_domain_data, recording_names = self.feature_manager.\
                retrieve_data_from_feature_files(
                    cmd_args.output,
                    in_dir,
                    cmd_args.domains,
                )
            # from loaded data build feature matrices
            feature_matrix = self.feature_manager.build_feature_matrix(per_domain_data, domains)
            rec_names.append(recording_names)
            feature_matrices.append(feature_matrix)
            class_names.append(class_name)

        # prune feature matrices wrt cmd arguments
        feature_matrices = self.feature_manager.prune_feature_space_wrt_cmd_args(
            feature_matrices,
            cmd_args.feats,
            cmd_args.elecs
        )

        train_ids, test_ids = [], []
        # file_train_ids, file_test_ids = [], []
        for in_dir_id, in_dir in enumerate(cmd_args.input):
            train_fold_ids, test_fold_ids = self.train_test_split(len(feature_matrices[in_dir_id]))
            train_ids.append(train_fold_ids)
            test_ids.append(test_fold_ids)

        self.cross_validate(feature_matrices, train_ids, test_ids)

        # write the results
        self.write_results(cmd_args, class_names, len(feature_matrices[0]), len(feature_matrices[1]))

# ______________________________________________________________________________________________________________________
    def write_results(self, cmd_args, class_names, n_samples_class1, n_samples_class2):
        results = self.get_results()
        logging.info('\t\t\tMean accuracy: {} ({})'.format(results[0][1], results[0][2]))
        logging.info('\t\t\tMean precision: {} ({})'.format(results[1][1], results[1][2]))
        logging.info('\t\t\tMean recall: {} ({})'.format(results[2][1], results[2][2]))
        logging.info('\t\t\tMean F1-score: {} ({})'.format(results[3][1], results[3][2]))

        feature_labels = self.feature_manager.get_feature_labels()
        # upload the results to google spreadsheet
        io.write_to_google_result_tracking(
            class_names,
            [n_samples_class1, n_samples_class2],
            cmd_args.agg,
            cmd_args.window,
            cmd_args.windowsize,
            cmd_args.t_overlap,
            "HMM",
            str(self.n_components) + str(self.n_mix),
            cmd_args.domains,
            cmd_args.feats,
            cmd_args.elecs,
            len(feature_labels),
            cmd_args.bands,
            cmd_args.f_overlap,
            str(self.scaler_obj).split('(')[0],
            self.n_folds,
            '',
            results,
            str(self.n_mix) + ', ' + str(self.n_components),
            str(self.window_limit),
            location=cmd_args.output,
            no_up=cmd_args.no_up,
        )

        # write the results of cv
        io.write_results(cmd_args.output, cmd_args.input, results)

        # visualize the results
        if 'all' in cmd_args.visualize or 'train' in cmd_args.visualize:
            plotting.plot_cv_results(cmd_args.output, self.n_folds, self.get_results())
            self.confusion_matrices = np.stack(self.confusion_matrices)
            avg_confusion_matrix = np.mean(self.confusion_matrices, axis=0)
            plotting.plot_confusion_matrix(cmd_args.output, avg_confusion_matrix, classes=class_names)

# ______________________________________________________________________________________________________________________
    def get_results(self):
        """ compute mean and std of cv fold metrics accuracy, precision, recall and f1-score """
        results = self.results * 100
        mean_results = np.mean(results, axis=0)
        std_results = np.std(results, axis=0)

        # 0: accs, 1: precs, 2: recs, 3: f1s
        return [results[:, 0], mean_results[0], std_results[0]], \
               [results[:, 1], mean_results[1], std_results[1]], \
               [results[:, 2], mean_results[2], std_results[2]], \
               [results[:, 3], mean_results[3], std_results[3]]

# ______________________________________________________________________________________________________________________
    def check_gmm(self, gmm):
        """Check wether the parameters of a GMM are not Inf or NaN.
        """
        for component in gmm.distributions:
            for gaussian in component.distributions:
                if not np.all(np.invert(np.isnan(gaussian.parameters))):
                    return False
        return True

# ______________________________________________________________________________________________________________________
    def make_train_gmm(self, x):
        """Build and fit a Gaussian mixture model.

        .. note:: Gaussians are restricted to diagonal covariance matrix.
        """
        gmm = GeneralMixtureModel([
            IndependentComponentsDistribution([
                NormalDistribution(1, 1) for _ in range(x.shape[1])])
            for _ in range(self.n_mix)]
        )

        gmm.fit(x)

        # EM seldom fails to converge, just restart the process if it happens
        return gmm if self.check_gmm(gmm) else self.make_train_gmm(x)

# ______________________________________________________________________________________________________________________
    def secs2mat(self, sequences):
        """Take n roughly equally spaced observation of each sequence.

        :sequences [np.ndarray]:
            a list of observation sequences, sequences are stacked along the first
            axis.
        :n int:
            number of samples to extract from each sequence
        :return np.ndarray:
            The extracted samples in an array of shape
            (len(sequences), n) + shape(samples)
        :note:
            If the sequence is shorter than n, then some samples are duplicated.
            If sthe sequence is longer than n, it is split into n equal sections,
            and one sample is randomly taken from each.
        """
        dispatch = np.stack([np.rint(
            np.linspace(0, len(seq) - len(seq) / self.n_components - 1, self.n_components) +
            np.random.rand(self.n_components) * len(seq) / self.n_components).astype(np.int)
                             for seq in sequences])

        stacked = np.stack([seq[samples] for seq, samples in zip(sequences, dispatch)])
        return stacked

# ______________________________________________________________________________________________________________________
    def train_one_hmm(self, X):
        """ this code was taken from pomegranate github page
        :return:
        """
        # logging.info("\t\tPretraining GMM")
        # Build emission models with priors
        pretrain_samples = self.secs2mat(X)
        emission_priors = [self.make_train_gmm(pretrain_samples[:, s, :]) for s in range(self.n_components)]

        # Build HMM
        states = [State(dist, name="S" + str(i))
                  for i, dist in enumerate(emission_priors)]

        model = HiddenMarkovModel()
        model.add_states(*states)

        model.add_transition(model.start, states[0], 1)

        for i in range(len(states) - 1):  # Regular LR model
            model.add_transition(states[i], states[i], .8)
            model.add_transition(states[i], states[i + 1], .1)

        for i in range(len(states) - 2):  # Jumping connections
            model.add_transition(states[i], states[i + 2], .1)

        # transitions from initial and to final state
        model.add_transition(states[-1], states[-1], .9)
        model.add_transition(states[-1], model.end, .1)
        model.bake()

        obs_score = []
        # Fit model
        if self.parallel_folds:
            model.fit(X, stop_threshold=1000, min_iterations=3, max_iterations=50, verbose=True)
            for x in X:
                obs_score.append(model.log_probability(x))
        else:
            model.fit(X, stop_threshold=1000, min_iterations=3, max_iterations=50, verbose=True, n_jobs=self.n_proc)
            for x in X:
                obs_score.append(model.log_probability(x))
        print(np.mean(obs_score))
        return model

    # ______________________________________________________________________________________________________________________
    def __init__(self, n_folds=10, n_proc=1, agg='none', n_components=20, ltr=False, n_mix=20, parallel_folds=False,
                 window_limit=None):
        self.n_folds = n_folds
        self.n_proc = n_proc
        self.scaler_obj = None

        # hmm components and whether or not left to right
        self.n_components = n_components
        self.n_mix = n_mix
        self.ltr = ltr

        self.parallel_folds = parallel_folds
        self.window_limit = window_limit
        self.feature_manager = hmm_feature_manager.HMMFeatureManager(
            agg,
            window_limit
        )

        # 4 metrics: accuracy, precision, recall, f1
        self.results = np.ndarray(shape=(self.n_folds, 4), dtype=float)
        self.confusion_matrices = np.ndarray(shape=(self.n_folds, ), dtype=np.ndarray)
