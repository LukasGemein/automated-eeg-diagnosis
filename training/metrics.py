from sklearn.metrics import accuracy_score, roc_curve, roc_auc_score
import numpy as np

from training.experiment import chunks


# unfortunately, roc_curve and roc_auc_score do not follow y_pred, y_true convention
def roc_curve_decorator(y_pred, y_true):
    return roc_curve(y_score=y_pred[:, 1], y_true=y_true)


def roc_auc_score_decorator(y_pred, y_true):
    return roc_auc_score(y_score=y_pred[:, 1], y_true=y_true)


def get_majority_vote_over_window_labels_accuracy_score(
    window_count: int
):
    def majority_vote_over_window_labels_accuracy_score(
        y_true: list,                    # the actual labels
        y_pred: np.ndarray,              # predictions in form of [[p00, p01], [p10, p11], [p20, p21], ...]
        window_count: int=window_count,  # the number of time window predictions belonging to the same recording
        normalize: bool=True,            # whether to normalize the accuracy score
        sample_weight: np.ndarray=None   # individual sample weigths
    ):
        """given time window predictions as labels, compute the label of a recording as a majority vote of window labels"""
        if y_pred.ndim != 1:
            raise ValueError("Prediction probabilities instead of labels given.")
        # this is not really necessary, all the y_trues in a group should have the same label
        y_true_groups = chunks(y_true, window_count)
        y_true_labels = []
        for y_true_group in y_true_groups:
            label = y_true_group[-1]
            y_true_labels.append(label)

        # determine group label (=recording label) by majority vote over window labels
        y_pred_groups = chunks(y_pred, window_count)
        y_pred_labels = []
        for y_pred_group in y_pred_groups:
            ratio = np.mean(y_pred_group)
            label = 1 if ratio >= .5 else 0
            y_pred_labels.append(label)

        return accuracy_score(
            y_true=y_true_labels,
            y_pred=y_pred_labels,
            normalize=normalize,
            sample_weight=sample_weight
        )
    
    return majority_vote_over_window_labels_accuracy_score


def get_mean_log_prob_accuracy_score(
    window_count: int
):
    def mean_log_prob_accuracy_score(
        y_true: list,                    # the actual labels
        y_pred: np.ndarray,              # predictions in form of [[p00, p01], [p10, p11], [p20, p21], ...]
        window_count: int=window_count,  # the number of time window predictions belonging to the same recording
        normalize: bool=True,            # whether to normalize the accuracy score
        sample_weight: np.ndarray=None   # individual sample weigths
    ):
        """for every group, e.g. time windows that belong to the same recording, compute the mean log probability. 
        the final label for that recording is determined by the greater class mean log probability"""
        # make sure, there are no predictions with 0, since log(0) = -inf
        y_pred = np.array(y_pred, dtype=float)
        y_pred[y_pred==0]=.01
        if y_pred.ndim != 2:
            raise TypeError("Cannot compute mean log probs from labels!")

        # split the predictions according to the number of window counts belonging to the same recording
        y_pred_groups = chunks(y_pred, window_count)
        y_pred_labels = []
        # compute the mean log prbability of every group and assert label to recording according to larger mean log prob
        for y_pred_group in y_pred_groups:
            mean_log_prob = np.mean(np.log(y_pred_group), axis=0)
            p0, p1 = mean_log_prob
            label = 1 if p1 >= p0 else 0
            y_pred_labels.append(label)

        # reduce the true labels from the number of windows to the number of recordings
        y_true_groups = chunks(y_true, window_count)
        y_true_labels = []
        for y_true_group in y_true_groups:
            label = y_true_group[-1]
            y_true_labels.append(label)

        return accuracy_score(
            y_true=y_true_labels,
            y_pred=y_pred_labels,
            normalize=normalize,
            sample_weight=sample_weight
        )
    
    return mean_log_prob_accuracy_score