from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.model_selection import StratifiedKFold
import multiprocessing as mp
import numpy as np
import os

from utils import logger

SEED = 4316932


# TODO: add mapping of rec_name -> predictions, probabilities

class AutoSklearnTrainer(object):
    """
    """
# ______________________________________________________________________________________________________________________
    def get_results(self):
        """ compute mean and std of cv fold metrics accuracy, precision, recall and f1-score """
        results = self.results * 100
        mean_results = np.mean(results, axis=0)
        std_results = np.std(results, axis=0)

        # 0: accs, 1: precs, 2: recs, 3: f1s
        return [results[:, 0], mean_results[0], std_results[0]], \
               [results[:, 1], mean_results[1], std_results[1]], \
               [results[:, 2], mean_results[2], std_results[2]], \
               [results[:, 3], mean_results[3], std_results[3]]

# ______________________________________________________________________________________________________________________
#     def write_results(self, cmd_args, n_samples_class1, n_samples_class2, class_names):
#         """
#         :param cmd_args:
#         :param n_samples_class1:
#         :param n_samples_class2:
#         :param class_names:
#         :return:
#         """
#         results = self.get_results()
#         logger.info('\t\t\tMean accuracy: {} ({})'.format(results[0][1], results[0][2]))
#         logger.info('\t\t\tMean precision: {} ({})'.format(results[1][1], results[1][2]))
#         logger.info('\t\t\tMean recall: {} ({})'.format(results[2][1], results[2][2]))
#         logger.info('\t\t\tMean F1-score: {} ({})'.format(results[3][1], results[3][2]))
#
#         feature_labels = self.feature_manager.get_feature_labels()
#         # upload the results to google spreadsheet
#         io.write_to_google_result_tracking(
#             class_names,
#             [n_samples_class1, n_samples_class2],
#             cmd_args.agg,
#             cmd_args.window,
#             cmd_args.windowsize,
#             cmd_args.t_overlap,
#             self.clf_name,
#             '?',  # estimators
#             cmd_args.domains,  # domain
#             cmd_args.feats,  # feats
#             cmd_args.elecs,  # elecs
#             len(feature_labels),  # len of feature_labels
#             cmd_args.bands,
#             cmd_args.f_overlap,
#             '?',  # scaler
#             self.n_folds,
#             [self.total_budget, self.run_budget],
#             results,
#             '',  # '' '.join(re.split('\s{2,}', str(clf))),
#             self.clf_infos,
#             location=cmd_args.output,
#             no_up=cmd_args.no_up,
#         )
#
#         # write the results of cv
#         io.write_results(cmd_args.output, cmd_args.input, results)
#
#         # visualize the results
#         if 'all' in cmd_args.visualize or 'train' in cmd_args.visualize:
#             plotting.plot_cv_results(cmd_args.output, self.n_folds, self.get_results())
#             self.confusion_matrices = np.stack(self.confusion_matrices)
#             avg_confusion_matrix = np.mean(self.confusion_matrices, axis=0)
#             plotting.plot_confusion_matrix(cmd_args.output, avg_confusion_matrix, classes=class_names)

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ from each subprocess take the results from out_q """
        results = out_q.qsize() * [None]
        # clf_infos = out_q.qsize() * [None]
        cms = out_q.qsize() * [None]
        while not out_q.empty():
            fold_id, fold_results, cm = out_q.get()
            results[fold_id] = fold_results
            # clf_infos[fold_id] = clf_info
            cms[fold_id] = cm
        return results, cms

# ______________________________________________________________________________________________________________________
    def execute(self, in_q, out_q, out_dir):
        """ creates a subprocess to do one fold of cv
        :param in_q: multiprocessing queue that holds data for this cv fold as well as the fold id
        :param out_q: multiprocessing queue that is filled with fold id as well as results of that cv fold in terms of
        acc, prec, rec, f1 and confusion matrix
        :param out_dir: output directory
        """
        while not in_q.empty():
            i, train, labels_train, test, labels_test = in_q.get(timeout=1)
            # include in subprocesses, since top level include overrides logging preferences and suppresses all of this
            # codes logging
            import autosklearn.classification
            # this was taken from auto-sklearn website and adapted
            tmp_folder = os.path.join(out_dir, 'autoskl', 'tmp', str(i))
            output_folder = os.path.join(out_dir, 'autoskl', 'output', str(i))

            clf = autosklearn.classification.AutoSklearnClassifier(
                time_left_for_this_task=self.total_budget,
                per_run_time_limit=self.run_budget,
                ml_memory_limit=4096,
                tmp_folder=tmp_folder,
                output_folder=output_folder,
                delete_tmp_folder_after_terminate=False,
                delete_output_folder_after_terminate=False,
                seed=SEED,
            )
            clf.fit(train, labels_train)
            # clf.refit(x_train_fold, y_train_fold)
            predictions = clf.predict(test)

            acc = accuracy_score(labels_test, predictions)
            prec = precision_score(labels_test, predictions)
            rec = recall_score(labels_test, predictions)
            f1 = f1_score(labels_test, predictions)
            cm = confusion_matrix(labels_test, predictions)

            # out_q.put((i, [acc, prec, rec, f1], cm, clf.show_models()))
            out_q.put((i, [acc, prec, rec, f1], cm))

# ______________________________________________________________________________________________________________________
    def cross_validate(self, out_dir, feature_matrices):
        """ take the features and assemble them in a feature matrix. create stratified k-fold cv subsets of features
        and labels. execute cv in parallel, limit number of parallel executions by number min(folds, n_proc)
        :param out_dir: output directory
        :param feature_matrices: list of feature matrices of the given classes
        """
        # set up multiprocessing
        manager = mp.Manager()
        in_q, out_q = manager.Queue(maxsize=self.n_folds), manager.Queue(maxsize=self.n_folds)
        processes = np.ndarray(shape=(self.n_folds,), dtype=mp.Process)

        # stack feature matrices and labels for application of stratified k-fold splitter
        labels = []
        for class_id, feature_matrix in enumerate(feature_matrices):
            labels.extend(len(feature_matrix) * [class_id])
        labels = np.asarray(labels)
        feature_matrices = np.vstack(feature_matrices)

        # split into k stratified folds. store ids for usage in multiprocessing
        skf = StratifiedKFold(n_splits=self.n_folds)
        all_train_ids, all_test_ids = [], []
        for fold_id, (train_ids, test_ids) in enumerate(skf.split(feature_matrices, labels)):
            all_train_ids.append(train_ids)
            all_test_ids.append(test_ids)

        # parellalize cv folds. but only start that many processes as specified by n_proc
        # for each cv fold pick data, generate labels and hand it to a subprocess
        worked_folds = 0
        while worked_folds < self.n_folds:
            for process_id in range(worked_folds, min(worked_folds+self.n_jobs, self.n_folds)):
                train = feature_matrices[all_train_ids[process_id]]
                labels_train = labels[all_train_ids[process_id]]

                test = feature_matrices[all_test_ids[process_id]]
                labels_test = labels[all_test_ids[process_id]]

                in_q.put((process_id-worked_folds, train, labels_train, test, labels_test))
                processes[process_id] = mp.Process(target=self.execute, args=(in_q, out_q, out_dir))

            for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_jobs]):
                logger.info("\t\tTraining and predicting fold {}.".format(worked_folds+process_id))
                process.start()

            for process in processes[worked_folds:worked_folds+self.n_jobs]:
                process.join()

            # catch the results as computed by the subprocesses
            resutls, cms = self.catch_results(out_q)
            self.results[worked_folds:worked_folds+self.n_jobs] = resutls
            self.confusion_matrices[worked_folds:worked_folds+self.n_jobs] = cms
            # self.clf_infos[worked_folds:worked_folds+self.n_folds] = clf_infos
            worked_folds += self.n_jobs

# ______________________________________________________________________________________________________________________
    def train(self, feature_matrices, output):
        """ take the features computed in the preprocessing step. split them into train and test folds to perform CV.
        :param cmd_args: argparse namespace of command line arguments
        """
        self.cross_validate(output, feature_matrices)

        # write the results
        # self.write_results(cmd_args, len(feature_matrices[0]), len(feature_matrices[1]), class_names)

# ______________________________________________________________________________________________________________________
    def __init__(self, auto_skl, n_folds=10, n_jobs=1):
        self.n_folds = n_folds
        self.n_jobs = n_jobs
        self.clf_name = 'ASC'
        [self.total_budget, self.run_budget] = auto_skl

        # 4 metrics
        self.confusion_matrices = np.ndarray(shape=(self.n_folds, ), dtype=np.ndarray)
        self.clf_infos = np.ndarray(shape=(self.n_folds, ), dtype=np.ndarray)
        self.results = np.ndarray(shape=(self.n_folds, 4), dtype=float)
