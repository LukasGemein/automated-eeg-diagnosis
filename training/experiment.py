from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from multiprocessing import Manager, Process
from sklearn.metrics import accuracy_score
from sklearn.decomposition import PCA
from sklearn.pipeline import Pipeline
from typing import Union  #, Dict  # replace dict type hint by Dict[type, type]?
import pandas as pd
import numpy as np

# custom modules
from utils import logger

SEED=4316932
DEFAULT_EXPERIMENT_CLF={"RandomForest": RandomForestClassifier(n_estimators=100, random_state=SEED)}
DEFAULT_EVALUATION_PIPELINE={"RandomForestEvaluation": Pipeline([("RandomForest", RandomForestClassifier(n_estimators=100))])}

# TODO: move this to some kind of utility collection
@staticmethod
def chunks(listing, chunk_size):
    """Yield successive n-sized chunks from l"""
    # https://stackoverflow.com/questions/312443/how-do-you-split-a-list-into-evenly-sized-chunks
    for i in range(0, len(listing), chunk_size):
        yield listing[i:i + chunk_size]
        

# this is a hack to comunicate the number of repetitions to the CrossValidation class
class FakeCV():
    def __init__(self, n_splits):
        self.n_splits=n_splits
        
    def get_n_splits(self):
        return self.n_splits


#TODO: store results in pandas data frame for easy plotting?
class FinalEvaluation(object):
    def __init__(
        self,
        pipeline: dict=DEFAULT_EVALUATION_PIPELINE,
        n_repetitions: int=10,                       # number of final evaluations. important for classifiers that 
        scorers: dict={"accuracy": accuracy_score},  # {scorer name: scorer object} scorers to evaluate performance
                                                     # scorers have to take 'y_true' and 'y_pred' as input arguments
        return_train_score: bool=False,              # whether to return train score
        n_jobs: int=1,                               # the number of jobs to be run in parallel
        attributes_to_store: dict={},                # {name to store by: attribute name} attributes of a classifier to be included
        # TODO: add option to store the model?
        store_model: bool=False,                     # store the model
        task: str="classification"                   #
    ):

        # functionality of the 'Experiment' is used to run final evaluation
        self._experiment = CrossValidation(
            clfs=pipeline,
            scorers=scorers,
            return_train_score=return_train_score,
            n_jobs=n_jobs,
            attributes_to_store=attributes_to_store,
            store_model=store_model,
            # this is a hack to comunicate the number of repetitions to the CrossValidation class
            cv=FakeCV(n_splits=n_repetitions),
            task=task,
        )
        self._pipeline = pipeline
        self.results={}
        
    def run(
        self,
        X_train: np.ndarray,               # train data
        y_train: Union[np.ndarray, list],  # train targets
        X_eval: np.ndarray,                # test data
        y_eval: Union[np.ndarray, list],   # test targets
    ) -> dict:                             # {repetition_id: repetition results}
        results = self._experiment.run(
            X_train=X_train,
            y_train=y_train,
            X_eval=X_eval,
            y_eval=y_eval
        )
        self.results = results
        return self.results
    
    def extract_mean_score(
        self,
    ):
        return self._experiment.extract_mean_score()
        
        
class CrossValidation(object):
    """Experiment class to run several cross-validations. Let's you easily compare different classifiers with
    varying scalings with or without application of dimensionality reduction using principal component analysis"""
    def __init__(
        self,
        clfs: dict=DEFAULT_EXPERIMENT_CLF,           # {classifier name: classifier object} 
                                                     # classifier object has to implement 'fit' and 'predict' 
        cv=StratifiedKFold(n_splits=10),             # cross-validation object implementing 'split' and 'get_n_splits()'
        scorers: dict={"accuracy": accuracy_score},  # {scorer name: scorer object} scorers to evaluate performance
                                                     # scorers have to take 'y_true' and 'y_pred' as input arguments
        scalers: dict={},                            # {scaler name: scaler object} 
                                                     # scaler object has to implement 'fit' and 'transform'
        pca_thresh: float=0,                         # threshold of explained variance/number of components for the application of pca
                                                     # ignored if pca is False
        only_last_fold: bool=False,                  # whether to only run last fold of cross-validation
        return_train_score: bool=False,              # whether to return train score
        n_jobs: int=1,                               # the number of jobs to be run in parallel
        attributes_to_store: dict={},                # {name to store by: attribute name} attributes of a classifier to be included
        # TODO: add option to store the model?
        store_model: bool=False,                     # store the model
        task: str="classification"                   #
    ):
        self._clfs=clfs
        self._cv=cv
        self._scorers=scorers
        self._scalers=scalers
        self._pca_thresh=pca_thresh
        self._only_last_fold=only_last_fold
        self._return_train_score=return_train_score
        self._n_jobs=n_jobs
        self._attributes_to_store=attributes_to_store
        self._task = task
        self._seeds = []
        
        self.pipelines={}
        self.results={}
        self.results_df=pd.DataFrame()
        
    def run(
        self,
        X_train: np.ndarray,                           # training data
        y_train: Union[np.ndarray, list],              # training targets
        X_eval: Union[np.ndarray, None]=None,          # final evaluation data
        y_eval: Union[np.ndarray, list, None]=None     # final evaluation targets
    ) -> dict:                                         # {pipeline name: results} cross-validation results 
        """run a set of cross-validation experiments. that is: create pipelines of scalers, pca and classifiers;
        perform cross-validation with each of the pipelines; evaluate each of them with specified scorers"""
        # from clfs, scalers and pca create pipelines to be run
        pipelines = self._build_pipelines()
        logger.info("Running {} pipeline{} in total".format(
            len(pipelines), 
            "s" if len(pipelines) != 1 else ""
        ))

        # make sure y_train can be used for indexing
        y_train = np.array(y_train)
        if not self._only_last_fold:
            n_times_to_run = self._cv.get_n_splits()  
        else:
            n_times_to_run = 1
        logger.debug("runs: {}, only_last_fold: {}".format(n_times_to_run, self._only_last_fold))

        # run traditional cv
        if X_eval is None and y_eval is None:
            logger.info("Running cross-validation")
            for pipeline in pipelines:
                # create a name for the pipeline containing its steps
                pipeline_id = '_'.join(
                    [step_name for step_name, step in pipeline.steps][::-1]
                )
                self.pipelines[pipeline_id] = pipeline
                logger.info("Running {}".format(pipeline_id))

                # run in parallel
                # TODO: eliminate first condition? and 'always' run in parallel if more than 1 core is available?
                # TODO: check if model supports n_jobs argument? and use it?
                # TODO: never run in parallel?
                # TODO: use utils.paralel_executor?
                if (self._n_jobs >= n_times_to_run and n_times_to_run != 1):
                    run_function = self._run_in_parallel  
                else:
                    run_function = self._run_sequentially

                #run_function = self._run_sequentially
                pipeline_results = run_function(
                    X_train=X_train,
                    y_train=y_train,
                    pipeline=pipeline,
                )

                #self._log_pipeline_results(pipeline_results)
                self.results[pipeline_id] = pipeline_results
                logger.info("Done with {}".format(pipeline_id))

        # run final evaluation
        else:
            logger.info("X_eval and y_eval given: running final evaluation")
            # make sure y_eval can be used for indexing
            y_eval = np.array(y_eval)
            
            # set random state seeds for evaluation runs
            if not self._seeds:
                self._seeds = [np.random.randint(np.iinfo(np.uint32).max) for _ in range(n_times_to_run)]

            for pipeline in pipelines:
                pipeline_id = '_'.join(
                    [step_name for step_name, step in pipeline.steps][::-1]
                )
                pipeline_results = {}
                for i in range(n_times_to_run):
                    # make sure no seed is used for final evaluation, since then every repetition evaluates to the same score
                    # TODO: create 'meaningful' random states. random states based on increasing variables is bad practice
                    # TODO: also set random state of transformers?
                    if hasattr(pipeline.steps[-1][1], "random_state"):
                        pipeline.steps[-1][1].random_state = self._seeds[i]
                        logger.debug("Random state {}".format(pipeline.steps[-1][1].random_state))

                    repetition_results = self._run_once(
                        X_train=X_train,
                        y_train=y_train,
                        X_test=X_eval,
                        y_test=y_eval,
                        i=i,
                        pipeline=pipeline
                    )
                    pipeline_results[i] = repetition_results
            
                #self._log_pipeline_results(pipeline_results)
                self.results[pipeline_id] = pipeline_results
                logger.info("Done with {}".format(pipeline_id))
        return self.results
    
    # TODO: restructure this again, since when not using pipelines, every scaling would only need to be applied once?
    # TODO: save intermediate scaled / PCAed data?
    def _build_pipelines(
        self,
    ) -> list:  # pipelines consisting of scalers, pca and classifiers
        """For every given combination of arguments, create a pipeline of 
        transformers, where the last transformer is a classifer"""
        pipelines = []
        for clf_name, clf in self._clfs.items():
            # TODO: for every clf find the best working combination of
            # scaling and pca and then use some time budget to optimize
            # hyperparameters of that clf?
            if type(clf) is Pipeline:
                pipelines.append(clf)
                continue
                
            pipelines.append(
                Pipeline([
                    (clf_name, clf)
                ])
            )
            
            if self._scalers:
                for transformer_name, transformer in self._scalers.items():
                    pipelines.append(
                        Pipeline([
                            (transformer_name, transformer), 
                            (clf_name, clf)
                        ])
                    )
                    # pca only works correctly if features were scaled
                    if self._pca_thresh:
                        pca_obj = PCA(
                            n_components=self._pca_thresh,
                            random_state=SEED
                        )
                        pipelines.append(
                            Pipeline([
                                (transformer_name, transformer), 
                                ('PCA', pca_obj), 
                                (clf_name, clf)
                            ])
                        )
        return pipelines
        
    def _run_sequentially(
        self,
        X_train: np.ndarray,  # the data
        y_train: np.ndarray,  # the targets
        pipeline: Pipeline,   # transformers to be applied to X
    ) -> dict:                # {fold id: fold results}
        """run all cross-validation folds sequentially using and evaluating given pipeline using scorers"""
        results = {}
        for i, (train_i, test_i) in enumerate(self._cv.split(X_train, y_train)):
            if self._only_last_fold and i != self._cv.get_n_splits()-1:
                continue

            fold_results = self._run_once(
                i=i,
                X_train=X_train[train_i],
                X_test=X_train[test_i],
                y_train=y_train[train_i],
                y_test=y_train[test_i],
                pipeline=pipeline,
            )
            
            results[i] = fold_results
        return results
    
    def _run_in_parallel(
        self,
        X_train: np.ndarray,  # the data
        y_train: np.ndarray,  # the targets
        pipeline: Pipeline,   # transformers to be applied to X. last transformer is an estimator
    ) -> dict:                # {fold_id: fold results}
        """run all cross-validation folds in parallel using and evaluating given pipeline using scorers"""
        # TODO: switch to Pool?
        out_q = Manager().Queue()
        processes = []
        for i, (train_i, test_i) in enumerate(self._cv.split(X_train, y_train)):
            p = Process(
                target=self._run_once,
                args=(
                    i,
                    X_train[train_i], 
                    X_train[test_i], 
                    y_train[train_i], 
                    y_train[test_i], 
                    pipeline, 
                    out_q
                )
            )
            processes.append(
                p
            )

        # auto-sklearn uses two cores per model
        # make sure it does not exceed the given n_jobs limit
        #estimator_name, estimator = pipeline.steps[-1]
        #if type(estimator) is AutoSklearnClassifier:
        #    processing_chunks = chunks(
        #        l=processes, 
        #        n=int(n_jobs/2)  # is this still true? monitoring htop does not show this necessary
        #    )
#
        #    for chunk_id, processing_chunk in enumerate(processing_chunks):
        #        for p in processing_chunk:
        #            p.start()
#
        #        for p in processing_chunk:
        #            p.join()

        # all other models only use one core
        #else:
        for p in processes:
            p.start()

        for p in processes:
            p.join()

        results = {}
        while not out_q.empty():
            i, fold_results = out_q.get()
            results[i] = fold_results
        return results

    def _run_once(
        self,
        i: int,                      # fold number
        X_train: np.ndarray,         # training data
        X_test: np.ndarray,          # test data
        y_train: np.ndarray,         # train targets
        y_test: np.ndarray,          # test targets
        pipeline: Pipeline,          # transformers to be applied to X
        
        out_q: Manager().Queue()=[]  # multiprocessing queue if cv-folds are run in parallel
    ) -> Union[None, dict]:          # None if out_q is used, else pipeline evaluated on fold data given scorers
        """run cross-validation fold i with given train/test data and labels using the pipeline of transformers 
        and scorers for evaluation of performance"""

        logger.debug("running {} train {} test {}".format(i, X_train.shape, X_test.shape))

        fold_results = {}
        # apply all the transformers of the pipeline
        for transformer_name, transformer in pipeline.steps[:-1]:
            X_train = transformer.fit_transform(X_train)
            X_test = transformer.transform(X_test)

            # store transformer attributes
            if self._attributes_to_store:
                transformer_attributes = self._store_attributes(
                    obj=transformer
                )
                fold_results.update(transformer_attributes)

        # use the estimator of the pipeline for training/prediction
        estimator_name, estimator = pipeline.steps[-1]
        # if the setting is muticlass classification, run estimator as one vs all
        if len(np.unique(y_train)) > 2 and self._task in ["classification"]:
            logger.debug("This is a multiclass task.")
            from sklearn.multiclass import OneVsRestClassifier
            multiclass_estimator = OneVsRestClassifier(
                estimator=estimator,
                n_jobs=1,
            )
            estimator = multiclass_estimator

        estimator = estimator.fit(
            X=X_train, 
            y=y_train
        )

        # make predictions
        y_test_pred = estimator.predict(X=X_test)
        fold_results["y_test"] = y_test
        fold_results["y_test_pred"] = y_test_pred

        if hasattr(estimator, "predict_proba"):
            y_test_pred_proba = estimator.predict_proba(X=X_test)
            fold_results["y_test_pred_proba"] = y_test_pred_proba

        # svm specific. useful for roc curves
        elif hasattr(estimator, "decision_function"):
            y_test_score = estimator.decision_function(X=X_test)
            fold_results["y_test_score"] = y_test_score

        # estimate and store train performance
        if self._return_train_score:
            y_train_pred = estimator.predict(X=X_train)
            fold_results["y_train"] = y_train
            fold_results["y_train_pred"] = y_train_pred
        
            if hasattr(estimator, "predict_proba"):
                y_train_pred_proba = estimator.predict_proba(X=X_train)
                fold_results["y_train_pred_proba"] = y_test_pred_proba

            # svm specific. useful for roc curves
            elif hasattr(estimator, "decision_function"):
                y_train_score = estimator.decision_function(X=X_train)
                fold_results["y_train_score"] = y_test_score

        # Do not evaluate here?
        # evaluate the predictions wrt given scorers
        test_scores = self._evaluate(
            y_true=y_test,
            y_pred=y_test_pred,
            train_or_test="test"
        )
        fold_results.update(test_scores)
        if self._return_train_score:
            train_scores = self._evaluate(
                y_true=y_train,
                y_pred=y_train_pred,
                train_or_test="train"
            )
            fold_results.update(train_scores)

        # check for the attributes to store
        if self._attributes_to_store:
            estimator_attributes = self._store_attributes(
                obj=estimator
            )
            fold_results.update(estimator_attributes)

        # if running in parallel, append results to queue
        if out_q:
            out_q.put((
                i, 
                fold_results
            ))
        # else just return results
        else:
            return fold_results    
        
    def _store_attributes(
        self,
        obj     # an object whose attributes might be stored if specified
    ) -> dict:  # {name_to_store_by: attribute_value}
        """check for specified attributes in the object and store them if existing"""
        obj_attributes = {}
        for name_to_store_by, attribute_name in self._attributes_to_store.items():
            if hasattr(obj, attribute_name):
                attribute = getattr(obj, attribute_name)
                if callable(attribute):
                    attribute_value = attribute.__call__()
                else:
                    attribute_value = attribute
                obj_attributes[name_to_store_by] = attribute_value
        return obj_attributes
    
    def _evaluate(
        # TODO: simply return predictions to be evaluated after experiments?
        # TODO: or store predictions additionally?
        self,
        y_true: list,              # targets
        y_pred: list,              # predictions
        train_or_test: str="test"  # whether this is train or test evaluation
    ) -> dict:                     # {train_or_test_scorer_name: score}
        """given the scorers evaluate the predictions vs the targets"""
        results = {}
        # evaluate all the scorers on fold (train and) test data
        for scorer_name, scorer in self._scorers.items():
            # check whether the scorer can handle class probabilities
            # if not, cast probabilities to class labels
            key = '_'.join([train_or_test, scorer_name])
            try:
                results[key] = scorer(
                    y_pred=y_pred, 
                    y_true=y_true
                )
            except ValueError:
                logger.warning("Scorer {} cannot handle probabilities. Creating labels.".format(scorer_name))
                y_pred_labels = self._convert_probabilities_to_labels(
                    y=y_pred
                )
                results[key] = scorer(
                    y_pred=y_pred_labels,
                    y_true=y_true
                )
            except TypeError:
                logger.error("Scorer {} cannot handle class labels. Setting score to 0.".format(scorer_name))
                results[key] = 0
        return results
    
    def _convert_probabilities_to_labels(
        self,
        y: Union[list, np.ndarray],  # [[p_class_0_0, p_class_1_0], [p_class_0_1, ...] ...]
        thresh: float=.5             # threshold at which to convert probabilities to labels 
    ) -> list:                       # [label_class_0, label_class_0, ...]
        """if predict proba was selected and the specified scorer cannot handle probability inputs,
        transform the probabilities to class labels"""
        y_labels = np.array(y)[:, 1]
        y_labels[y_labels>=thresh] = 1
        y_labels[y_labels<thresh] = 0
        return y_labels
    
    # TODO: move this to logger?
    def _log_pipeline_results(
        self,
        results: dict  # cross-validation results of a pipeline
    ):
        """as information, output the current performance of a pipeline wrt. scorers"""
        for scorer_name in self._scorers.keys():
            score_name = "test_"+scorer_name
            mean_pipeline_score = self._extract_mean_pipeline_score(
                pipeline_results=results, 
                score_name=score_name
            )

            # this does not make sense for scorer confusion matrix
            logger.info("scored {} {}".format(
                score_name,
                mean_pipeline_score
            ))
    
    def _extract_mean_pipeline_score(
        self,
        pipeline_results: Pipeline,      # a pipeline of transformers, pca and classifier

        score_name: str="test_accuracy"  # the name of the score to be extracted
                                         # names are 'train_'/'test_' followed by score name as given for experiment 
    ) -> float:                          # mean cross-validation score of that pipeline
        """extract the average cross-validation score of the given pipeline wrt. speficied scoring"""
        pipeline_scores = []
        for fold_id, fold in pipeline_results.items():
            pipeline_scores.append(fold[score_name])
        return np.mean(pipeline_scores)
    
    def extract_mean_score(
        self,
        score_name: str="",    # the name of the score to be extracted
        train_or_test: str=""  # names are 'train_'/'test_' followed by score name as given for experiment 
    ) -> dict:                 # {pipeline name: {train_or_test: {score_name: average cv score}}}
        """extract the average cross-validation score of all pipelines wrt. speficied scoring"""
        results = {}
        if not train_or_test:
            train_or_test = ["train", "test"] if self._return_train_score else ["test"]
        else:
            train_or_test = [train_or_test]

        for pipeline_name, pipeline_results in self.results.items():
            results[pipeline_name] = {}
            if not score_name:
                scorers = self._scorers
            else:
                # this is the default scorer
                scorers = {"accuracy": "dummy"}
            
            for to_extract in train_or_test:
                for score_name, scorer in scorers.items():
                    score_name = '_'.join([to_extract, score_name])
                    results[pipeline_name][score_name] = self._extract_mean_pipeline_score(
                        pipeline_results=pipeline_results,
                        score_name=score_name
                    )
        return results
