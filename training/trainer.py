from sklearn.model_selection import StratifiedKFold, cross_validate
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
from sklearn.decomposition import PCA
import multiprocessing as mp
import numpy as np
import time

from utils import logger

SEED = 4316932
# SEED = np.random.RandomState()


# TODO: if doing regression, stratified kfold isnt possible
# TODO: also the metrics need to be adapted
class Trainer(object):
    """
    """
# ______________________________________________________________________________________________________________________
    def evaluate_fold(self, labels_train, train_predictions, labels_test, predictions, clf):
        # test data
        cm = confusion_matrix(
                y_true=labels_test,
                y_pred=predictions,
                labels=None
        )
        self.test_confusion_matrices.append(
            cm
        )

        tn, fp, fn, tp = cm.ravel()
        accuracy = (tp+tn)/(tp+fp+tn+fn)
        self.test_accuracies.append(
            accuracy
        )

        # train data
        cm = confusion_matrix(
                y_true=labels_train,
                y_pred=train_predictions,
                labels=None
        )
        tn, fp, fn, tp = cm.ravel()
        accuracy = (tp+tn)/(tp+fp+tn+fn)
        self.train_confusion_matrices.append(
            cm
        )
        self.train_accuracies.append(
            accuracy
        )

        # random forest specifics
        if self.clf_name == 'RF':
            self.importances.append(
                clf.feature_importances_
            )

# ______________________________________________________________________________________________________________________
    def apply_pca(self, train, test):
        # fit pca on first observed train data. transform all train and test data
        if self.pca_obj is None:
            pca_obj = PCA(n_components=self.pca_comp, copy=True, random_state=SEED)
            pca_obj.fit(train)
            train = pca_obj.transform(train)
            test = pca_obj.transform(test)
            self.pca_obj = pca_obj
        else:
            train = self.pca_obj.transform(train)
            test = self.pca_obj.transform(test)

        # pca_obj = PCA(n_components=self.pca_comp)
        # pca_obj.fit(train)
        # train = pca_obj.transform(train)
        # test = pca_obj.transform(test)
        # self.pca_obj = pca_obj

        return train, test, np.cumsum(self.pca_obj.explained_variance_ratio_)

# ______________________________________________________________________________________________________________________
    def scale_features(self, train, test):
        """ scales the features by their absolute maximum value. scaling is fit on train fold and afterwards also
        applied to test fold
        :param train: train fold
        :param test: test fold
        :return: scaled train and test fold
        """
        # fit scaler on first observed train data. transform all train and test data
        if self.scaler_obj is None:
            scaler = StandardScaler(copy=False)
            train = scaler.fit_transform(train)
            test = scaler.transform(test)
            self.scaler_obj = scaler
        else:
            train = self.scaler_obj.transform(train)
            test = self.scaler_obj.transform(test)

        return train, test, str(self.scaler_obj)

# ______________________________________________________________________________________________________________________
    def create_classifier(self):
        """ takes the classifier as specified by self.clf_name and creates the chosen classifier. choices are
        ['RF', 'SVM', 'AB', 'NB', 'KNN', 'GP', 'QDA', 'DT', 'GB']
        :return: the classifier object
        """
        # AdaBoost
        if self.clf_name == 'AB':
            from sklearn.ensemble import AdaBoostClassifier
            clf = AdaBoostClassifier(
                n_estimators=self.n_estimators,
                random_state=SEED,
            )

        # Logistic Regression
        elif self.clf_name == 'LR':
            from sklearn.linear_model import LogisticRegression
            clf = LogisticRegression(
                n_jobs=self.n_jobs,
                random_state=SEED
            )

        # Support Vector Machine
        elif self.clf_name == 'SVM':
            from sklearn.svm import SVC
            clf = SVC(
                kernel='rbf',
                random_state=SEED,
                probability=True,
                # recommendation by matthias to increase cache size
                cache_size=2048
            )

        # Naive Bayes
        elif self.clf_name == 'NB':
            from sklearn.naive_bayes import GaussianNB
            clf = GaussianNB()

        # K nearest neighbors
        elif self.clf_name == 'KNN':
            from sklearn.neighbors import KNeighborsClassifier
            clf = KNeighborsClassifier(
                n_jobs=self.n_jobs,
            )

        # Gaussian Process
        elif self.clf_name == 'GP':
            from sklearn.gaussian_process import GaussianProcessClassifier
            clf = GaussianProcessClassifier(
                random_state=SEED
            )

        # Decision Tree
        elif self.clf_name == 'DT':
            from sklearn.tree import DecisionTreeClassifier
            clf = DecisionTreeClassifier(
                random_state=SEED,
            )

        # Quadratic Discriminant Analysis
        elif self.clf_name == 'QDA':
            from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
            clf = QuadraticDiscriminantAnalysis()

        elif self.clf_name == 'GB':
            from sklearn.ensemble import GradientBoostingClassifier
            clf = GradientBoostingClassifier(
                n_estimators=self.n_estimators,
                random_state=SEED,
            )

        # Default: Random Forest
        else:
            from sklearn.ensemble import RandomForestClassifier
            clf = RandomForestClassifier(
                n_jobs=self.n_jobs,
                n_estimators=self.n_estimators,
                random_state=SEED,
                oob_score=False,
            )

        return clf

# ______________________________________________________________________________________________________________________
    def execute(self, in_q, out_q, clfs):
        """ creates a subprocess to do one fold of cv
        :param in_q: multiprocessing queue that holds data for this cv fold as well as the fold id
        :param out_q: multiprocessing queue that is filled with fold id as well as results of that cv fold in terms of
        acc, prec, rec, f1 and confusion matrix
        """
        while not in_q.empty():
            fold_id, train, labels_train, test, labels_test = in_q.get(timeout=1)
            train, test, scale_obj_str = self.scale_features(train, test)

            clf = clfs[fold_id]
            clf.fit(train, labels_train)
            predictions_train = clf.predict(train)
            predictions = clf.predict(test)

            cm = confusion_matrix(labels_test, predictions)
            tn, fp, fn, tp = cm.ravel()
            acc = (tp + tn) / (tp + fp + tn + fn)

            cm_train = confusion_matrix(labels_train, predictions_train)
            tn, fp, fn, tp = cm.ravel()
            acc_train = (tp + tn) / (tp + fp + tn + fn)

            out_q.put((fold_id, {"test": [acc, cm], "train": [acc_train, cm_train]}))

# ______________________________________________________________________________________________________________________
    def parallel_cross_validate(self, clfs, feature_matrices, labels):
        """ take the features and assemble them in a feature matrix. create stratified k-fold cv subsets of features
        and labels. execute cv in parallel, limit number of parallel executions by number min(folds, n_proc)
        :param feature_matrices: list of feature matrices of the given classes
        """
        # TODO: how to use parallel_executer here?
        manager = mp.Manager()
        in_q, out_q = manager.Queue(maxsize=self.n_folds), manager.Queue(maxsize=self.n_folds)
        processes = np.ndarray(shape=(self.n_folds,), dtype=mp.Process)

        feature_matrices = np.vstack(feature_matrices)

        # split into k stratified folds. store ids for usage in multiprocessing
        skf = StratifiedKFold(n_splits=self.n_folds)
        all_train_ids, all_test_ids = [], []
        for fold_id, (train_ids, test_ids) in enumerate(skf.split(feature_matrices, labels)):
            all_train_ids.append(train_ids)
            all_test_ids.append(test_ids)

        # parellalize cv folds. but only start that many processes as specified by n_proc
        # for each cv fold pick data, generate labels and hand it to a subprocess
        worked_folds = 0
        while worked_folds < self.n_folds:
            for process_id in range(worked_folds, min(worked_folds+self.n_jobs, self.n_folds)):
                train = feature_matrices[all_train_ids[process_id]]
                labels_train = labels[all_train_ids[process_id]]

                test = feature_matrices[all_test_ids[process_id]]
                labels_test = labels[all_test_ids[process_id]]

                in_q.put((process_id-worked_folds, train, labels_train, test, labels_test))
                processes[process_id] = mp.Process(target=self.execute, args=(in_q, out_q, clfs))

            for process_id, process in enumerate(processes[worked_folds:worked_folds+self.n_jobs]):
                logger.info("\t\tTraining and predicting fold {}.".format(worked_folds+process_id))
                process.start()

            for process in processes[worked_folds:worked_folds+self.n_jobs]:
                process.join()

            # catch the results as computed by the subprocesses
            train_results, train_cms, test_results, test_cms = self.catch_results(out_q)
            self.train_accuracies.extend(train_results)
            self.train_confusion_matrices.extend(train_cms)

            self.test_accuracies.extend(test_results)
            self.test_confusion_matrices.extend(test_cms)

            worked_folds += self.n_jobs

# ______________________________________________________________________________________________________________________
    def catch_results(self, out_q):
        """ from each subprocess take the results from out_q """
        train_results = out_q.qsize() * [None]
        test_results = out_q.qsize() * [None]
        train_cms = out_q.qsize() * [None]
        test_cms = out_q.qsize() * [None]
        while not out_q.empty():
            fold_id, fold_results_dict, cm = out_q.get()

            train_results[fold_id], train_cms[fold_id] = fold_results_dict["train"]
            test_results[fold_id], test_cms[fold_id] = fold_results_dict["test"]

        return train_results, train_cms, test_results, test_cms

# ______________________________________________________________________________________________________________________
    def create_labels(self, feature_matrices):
        labels = []
        for class_id, feature_matrix in enumerate(feature_matrices):
            labels.extend(len(feature_matrix) * [class_id])
        labels = np.array(labels)
        return labels

# ______________________________________________________________________________________________________________________
    def cross_validate(self, clf, feature_matrices, labels):
        """ perform stratified k fold cross-validation.
        :param feature_matrices: list of feature matrices of the different classes
        :return: the classifier object
        """
        logger.info("\t\tPerforming {}-fold cross-validation ..".format(self.n_folds))

        feature_matrices = np.vstack(feature_matrices)

        # TODO: store all clfs?
        skf = StratifiedKFold(n_splits=self.n_folds)

        # this is equivalent to manual cv apart from scaling. faster as well
        # cv_results = cross_validate(clf, feature_matrices, labels, cv=skf, n_jobs=self.n_jobs,
        #                             scoring=["accuracy"])
        # print(np.mean(cv_results["test_accuracy"]))
        # print(np.sum(cv_results["fit_time"]) + np.sum(cv_results["score_time"]))

        if type(clf) is str:
            clf = self.create_classifier()
        for fold_id, (train_ids, test_ids) in enumerate(skf.split(feature_matrices, labels)):
            train = feature_matrices[train_ids]
            labels_train = labels[train_ids]

            test = feature_matrices[test_ids]
            labels_test = labels[test_ids]

            train, test, scale_obj_str = self.scale_features(train, test)
            # train, test, pca_explained_var = self.apply_pca(train, test)
            # exp_variances.append(pca_explained_var[-1])

            # fit the classifier on train set and predict test set
            clf = clf.fit(train, labels_train)
            train_predictions = clf.predict(train)
            predictions = clf.predict(test)

            self.evaluate_fold(
                labels_train=labels_train,
                train_predictions=train_predictions,
                labels_test=labels_test,
                predictions=predictions,
                clf=clf
            )

            logger.info("\t\t\t{}/{} done".format(fold_id+1, self.n_folds))

# ______________________________________________________________________________________________________________________
    def train(self, feature_matrices):
        """ take the features computed in the preprocessing step. split them into train and test folds to perform CV.
        :param feature_matrices: list of feature matrices
        """
        start = time.time()
        # make use of several cores
        clf = self.create_classifier()
        labels = self.create_labels(feature_matrices)
        if hasattr(clf, "n_jobs"):
            self.cross_validate(
                clf=clf,
                feature_matrices=feature_matrices,
                labels=labels
            )
        # run cv folds in parallel
        else:
            self.parallel_cross_validate(
                clfs=[self.create_classifier() for _ in range(self.n_folds)],
                feature_matrices=feature_matrices,
                labels=labels
            )
        end = time.time()
        logger.info("\t\tNeeded {}s to do CV:".format(end-start))

# ______________________________________________________________________________________________________________________
    def __init__(self, clf_name, n_jobs, n_estimators, n_folds=10, pca_comp=None, clf=None):
        self.n_folds = n_folds
        self.n_jobs = n_jobs
        self.clf = clf
        self.clf_name = clf_name
        self.n_estimators = n_estimators

        if self.clf_name == 'RF':
            self.importances = []

        self.scaler_obj = None
        self.pca_obj = None
        self.pca_comp = pca_comp

        self.test_accuracies = []
        self.test_confusion_matrices = []
        self.train_accuracies = []
        self.train_confusion_matrices = []
