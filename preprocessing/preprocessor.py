import multiprocessing as mp
import numpy as np

from feature_generation import feature_generator
from utils import logger, io, parallel_executor
from windowing import splitter
from cleaning import cleaner

# TODO: maximally parallel by running whole pipeline for a single recording
# TODO: fix creation of output directory
# TODO: improve by not loading and cleaning for every domain


class Preprocessor(object):
    """ The preprocessor takes raw input data and returns features. Thereby it cleans and splits the data. The
    processing is parallelized, s.t. reading of data, cleaning, splitting, fourier transform, visualization and feature
    computation are faster.
    """
# ______________________________________________________________________________________________________________________
    def parallel_function(self, item):
        """ the function executed by every worker. they load, clean and generate features in parallel
        :param item: an item of the multiprocessing list which is a tuple, i.e. recording object and recording id
        :return: tuple of ndarray holding features of the recording and the recording id
        """
        edf_id, edf_file = item
        raw_cleaned = self.cleaner.clean(edf_file)
        features = self.feature_generator.generate_features(raw_cleaned)
        return edf_id, features

# ______________________________________________________________________________________________________________________
    def domain_routine(self, in_q_list, domain):
        """ for every domain, map items from the multiprocessing list to the available processes. they load and clean
        the data and generate features of the specified domain
        :param in_q_list: multiprocessing list that holds the edf file names to be processed
        :param domain: the specified domain
        :return: a list of tuples. tuples are ndarray holding features of the recording of the current domain, and the
        time-based id of the recording
        """
        # if we are interested in patient information such as gender and age, we do not need to load the edf
        if domain == "patient":
            edf_loader = io.EdfLoader()
            results = []
            for item in in_q_list:
                edf_id, edf_file = item
                sex, age = edf_loader.parse_sex_and_age(edf_file)
                results.append(( edf_id, np.asarray([[age, sex]]) ))  # TODO: changed return type of edf_loader.

        else:
            # pe = parallel_executor.ParallelExecutor(self.n_proc)
            # results = pe.run(in_q_list=in_q_list, parallel_function=self.parallel_function)
            # apply loading, cleaning and feature generation of the current domain to all edf files in the queue
            pool = mp.Pool(processes=self.n_proc)
            results = list(pool.imap_unordered(self.parallel_function, in_q_list))
            pool.close()
            pool.join()

        return results

# ______________________________________________________________________________________________________________________
    def chunks(self, listing):
        """ split a list into chuncks. chunks have maximally the size of 1/10. however if 1/10 is smaller than number
        of available processes, this number is chosen
        :param listing: a list of things to be split into chunks
        :return: the original list split into chunks
        """
        chunksize = max(self.n_proc, int(len(listing)/10))
        if chunksize > len(listing):
            chunksize = len(listing)
        chunksize = max(1, chunksize)

        return [listing[i:i + chunksize] for i in range(0, len(listing), chunksize)]

# ______________________________________________________________________________________________________________________
    def random_subset(self, edf_files, subset):
        """ if cmd_args.subset is set, don't read all the recordings but take a random subset of the specified size
        :param edf_files: list of all recordings on the hdd
        :param subset: the size of the subset
        :return: a list of randomly selected files of the specified size
        """
        ids = np.arange(len(edf_files))
        np.random.shuffle(ids)
        subset_ids = sorted(ids[:subset])

        return [edf_files[i] for i in subset_ids]

# ______________________________________________________________________________________________________________________
    def class_routine(self, in_dir, cmd_args):
        """ for every input class, i.e. normal and abnormal, set up multiprocessing, read files, load and clean and
        generate features of specified domains
        :param in_dir: the input directory of this class' raw eeg data to be processed
        :param cmd_args: argparse namespace of command line arguments
        :return: the path to the directory where the features were written to
        """
        logger.info("\tGenerating features of files in {}".format(in_dir))

        edf_reader = io.EdfReader()
        # read all edf files from directory
        complete_edf_files = edf_reader.read_all_file_names(in_dir, extension='.edf')
        if cmd_args.subset is not None:
            complete_edf_files = self.random_subset(complete_edf_files, cmd_args.subset)

        complete_in_q_list = [(i, edf_file) for i, edf_file in enumerate(complete_edf_files)]
        in_q_lists = self.chunks(complete_in_q_list)

        # remove info from previous runs if available and write rec names and rec window counts
        io.delete_previous_runs_info(in_dir, cmd_args)
        # for i, rec_name in enumerate(complete_edf_files):
        #     io.write_recording_names_successively(cmd_args.output, in_dir, rec_name)
        #     io.write_window_counts_successively(cmd_args.output, in_dir, window_counts[i])

        hdf5_paths = []
        h5_writer = io.Hdf5Writer()
        # process every chunk individually and write features to file
        n_files, ten_percent, n_files_done, tmp = len(complete_in_q_list), .1 * len(complete_in_q_list), 0, 0
        for chunk_id, in_q_list in enumerate(in_q_lists):
            for domain_id, domain in enumerate(cmd_args.domains):

                # TODO: change this
                # create a hdf5 file for the current domain to write features to successively
                if chunk_id == 0:
                    hdf5_paths.append(h5_writer.create_hdf5_file(in_dir, cmd_args, domain))
                    # print(hdf5_paths[-1])
                    # print(cmd_args.output)

                self.init_preprocessing_units(cmd_args, domain)
                results = self.domain_routine(in_q_list, domain)
                for i, data in sorted(results):
                    h5_writer.write_domain_hdf5_successively(hdf5_paths[domain_id], data, i)

                # it would be so nice to create a dataframe with rec names as index and feature labels as columns
                # but there are still some problems. one major problem is pandas can only handle 2D
                # io.write_features_to_h5(hdf5_paths[domain_id], domain, features, ids,
                #                         self.feature_generator.get_feature_labels(), self.recording_names[in_dir])

            # store processed files and after every 10 percent log progress
            n_files_done += len(in_q_list)
            if n_files_done - tmp >= ten_percent:
                logger.info("\t\tProcessed {} of {} files".format(n_files_done, n_files))
                tmp = n_files_done

        return hdf5_paths[-1]

# ______________________________________________________________________________________________________________________
    def init_preprocessing_units(self, cmd_args, domain):
        """ setting up preprocessing objects for multiprocessing use
        :param cmd_args: argparse namespace of command line arguments
        :param domain: the current domain
        """
        # make sure domain is a list
        if type(domain) is not list:
            domain = [domain]

        if self.splitter is None:
            self.splitter = splitter.Splitter(
                window=cmd_args.window,
                window_length=cmd_args.windowsize,
                overlap=cmd_args.t_overlap
            )

        self.cleaner = cleaner.Cleaner(
            cmd_args.bands,
            cut_time=cmd_args.cut,
            elecs=cmd_args.elecs,
            band_overlap=cmd_args.f_overlap,
        )
        self.feature_generator = feature_generator.FeatureGenerator(
            domains=domain,
            agg=cmd_args.agg,
            cleaner=self.cleaner,
            splitter=self.splitter,
            filters=[int(filt_id) for filt_id in cmd_args.filters]
        )

# ______________________________________________________________________________________________________________________
    def preprocess(self, cmd_args):
        """ Checks if all the EEG recordings of the input directories can be processed. For every processable recording
        an entry in the multiprocessing queue is inserted. Results (features) are written to .hdf5 files per input dir.
        :param cmd_args: argparse namespace of command line arguments
        :return: list of directories where features were written to
        """
        # TODO: this could possibly be solved using json?
        # THIS IS HACKY. I NEED A FEATURE GENERATOR HERE TO ASSEMBLE ALL LABELS AND WRITE THEM TO FILE
        self.init_preprocessing_units(cmd_args, cmd_args.domains)
        # create output directory and write labels of features to be generated
        io.check_out(cmd_args.output, cmd_args.input)
        io.write_feature_labels(cmd_args.output, self.feature_generator.get_feature_labels())

        # stores for every class the hdf5 file name where features are stored
        feature_directories, window_counts = [], []
        for in_dir_id, in_dir in enumerate(cmd_args.input):
            hdf5_path = self.class_routine(in_dir, cmd_args)

            # save feature directory
            feature_directories.append('/'.join(hdf5_path.split('/')[:-1]))

        return feature_directories

# ______________________________________________________________________________________________________________________
    def __init__(self, nproc=1):
        self.cleaner = None
        self.splitter = None
        self.feature_generator = None

        self.n_proc = nproc
