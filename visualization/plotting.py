import matplotlib as mpl
#mpl.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
from scipy.spatial.distance import squareform
from matplotlib import pyplot as plt
from numpy.random import RandomState
import numpy as np
# import dwtviz
import seaborn
import scipy
import pylab
import os

from utils import io
# TODO: make this a class and give it output location as a member?


ELECTRODE_NAMES = sorted(['A1', 'A2', 'C3', 'C4', 'CZ', 'F3', 'F4', 'F7', 'F8', 'FP1', 'FP2', 'FZ', 'O1', 'O2',
                          'P3', 'P4', 'PZ', 'T3', 'T4', 'T5', 'T6'])
ELECTRODE_LOCATIONS = {
    'PZ': [50, 30],
    'CZ': [50, 50],
    'FZ': [50, 70],

    'O1': [40, 10],
    'O2': [60, 10],

    'T5': [13, 25],
    'T6': [87, 25],

    'P3': [32, 30],
    'P4': [68, 30],

    'F3': [32, 70],
    'F4': [68, 70],

    'F7': [13, 75],
    'F8': [87, 75],

    'FP1': [40, 90],
    'FP2': [60, 90],

    'A1': [-7, 52],
    'T3': [11, 50],
    'C3': [30, 50],
    'C4': [70, 50],
    'T4': [89, 50],
    'A2': [107, 52],
}


# ______________________________________________________________________________________________________________________
def plot_rec_analysis(signal_processing_steps, sampling_freq, cmd_args=None):
    """ Plot raw signal, decomposed by freq band, cleaned signal, decomposed by band
    :param rec:
    :param cmd_args:
    :return:
    """
    # TODO: plot series of time windows to see how the different window functions change the overall signal
    # TODO: also try overlap
    # scale to microvolts
    ch = signal_processing_steps[0] * 1000000
    # take 2 seconds of the signal
    secs = 2

    sart_shift = 20
    ch_max = max(ch[sart_shift*sampling_freq:(sart_shift+secs)*sampling_freq]) * 1.1
    ch_min = min(ch[sart_shift*sampling_freq:(sart_shift+secs)*sampling_freq]) * 1.1

    n_figures = 7
    # this is the cleaned and cut signal
    ch_cleaned = signal_processing_steps[1]
    ch_ft = signal_processing_steps[2]
    spectrum_median = signal_processing_steps[3]
    # print(ch_ft.shape)

    # be aware of the start time shift
    # this is the original signal
    plt.subplot(n_figures, 1, 1)
    plt.plot(np.arange(sampling_freq*secs), ch[60*sampling_freq:(60+secs)*sampling_freq])
    plt.xticks(np.linspace(0, sampling_freq*secs, secs+1), np.arange(secs+1))
    plt.ylim([ch_min, ch_max])
    plt.xlabel('time [s]')
    plt.ylabel('amplitude')
    # plt.title("")
    plt.grid(True)

    # this is the spectrum of the signal
    plt.subplot(n_figures, 1, 2)
    plt.plot(np.arange(len(ch_ft)), np.abs(ch_ft))
    # plt.title("spectrum")
    plt.ylabel('amplitude')
    plt.xlabel("frenquency")
    x_tick_locations = [1, 4, 8, 12, 30, 60, 120]
    x_tick_locations2 = [int(tick/(1/secs)) for tick in x_tick_locations]
    x_tick_labels = [str(tick) for tick in x_tick_locations]
    plt.xticks(x_tick_locations2, x_tick_labels)
    plt.grid(True)

    # this is the cleaned signal
    plt.subplot(n_figures, 1, 3)
    plt.plot(np.arange(sampling_freq*secs), ch_cleaned[:sampling_freq*secs])
    plt.xticks(np.linspace(0, sampling_freq*secs, secs+1), np.arange(secs+1))
    plt.xlabel('time [s]')
    plt.ylabel('amplitude')
    plt.ylim([ch_min, ch_max])
    # plt.title("")
    plt.grid(True)

    # this is the spectrum of the signal
    plt.subplot(n_figures, 1, 4)
    plt.plot(np.arange(len(ch_ft)), np.abs(ch_ft))
    # plt.title("spectrum")
    plt.ylabel('amplitude')
    plt.xlabel("frenquency")
    x_tick_locations = [1, 4, 8, 12, 30, 60, 120]
    x_tick_locations2 = [int(tick/(1/secs)) for tick in x_tick_locations]
    x_tick_labels = [str(tick) for tick in x_tick_locations]
    plt.xticks(x_tick_locations2, x_tick_labels)
    plt.grid(True)

    # this is the zoomed-in spectrum of the signal
    plt.subplot(n_figures, 1, 5)
    zoomed_in = np.abs(ch_ft)[:int(30/(1/secs))]
    plt.plot(np.arange(len(zoomed_in)), zoomed_in)
    # plt.title("spectrum")
    plt.ylabel('amplitude')
    plt.xlabel("frenquency")
    plt.xticks(x_tick_locations2[:5], x_tick_labels[:5])
    plt.grid(True)

    # this is the reconstructed signal
    reconstructed_signal = np.fft.irfft(ch_ft)
    plt.subplot(n_figures, 1, 6)
    plt.plot(np.arange(len(reconstructed_signal)), reconstructed_signal)
    plt.xticks(np.linspace(0, sampling_freq*secs, secs+1), np.arange(secs+1))
    plt.ylabel('amplitude')
    plt.xlabel("frenquency")
    plt.grid(True)
    plt.ylim([ch_min, ch_max])

    # this is the reconstructed signal
    reconstructed_signal = np.fft.irfft(ch_ft)
    plt.subplot(n_figures, 1, 7)
    plt.plot(np.arange(2*len(reconstructed_signal)), np.hstack((reconstructed_signal, reconstructed_signal)))
    plt.xticks(np.linspace(0, sampling_freq*secs*2, 2*secs+1), np.arange(2*secs+1))
    plt.ylabel('amplitude')
    plt.xlabel("frenquency")
    plt.grid(True)
    plt.ylim([ch_min, ch_max])

    # edf_file_name = '/'.join(rec.name.split('/')[-4:])
    # path = os.path.join(cmd_args.output, edf_file_name.replace('edf', 'pdf').replace('/', '_'))
    # plt.savefig(path, bbox_inches="tight")

    plt.show()


# ______________________________________________________________________________________________________________________
def plot_cv_results(output, folds, results, title_=''):
    # import seaborn
    [accs, precs, recs, f1s] = results

    ind = np.arange(folds)  # the x locations for the groups
    width = 0.2  # the width of the bars

    # colors for plotting
    a = .7
    c1 = "hotpink"
    c2 = "deepskyblue"
    c3 = "chartreuse"
    c4 = "darkorange"

    # cv fold results as bars
    bar1 = plt.bar(ind, accs[0], width, color=c1, alpha=a, yerr=accs[2], error_kw=dict(ecolor='k', alpha=a))
    bar2 = plt.bar(ind + width, precs[0], width, color=c2, alpha=a, yerr=precs[2], error_kw=dict(ecolor='k', alpha=a))
    bar3 = plt.bar(ind + 2*width, recs[0], width, color=c3, alpha=a, yerr=recs[2], error_kw=dict(ecolor='k', alpha=a))
    bar4 = plt.bar(ind + 3*width, f1s[0], width, color=c4, alpha=a, yerr=f1s[2], error_kw=dict(ecolor='k', alpha=a))

    # mean cv results as hlines
    hl1 = plt.axhline(accs[1], color=c1, alpha=a)
    hl2 = plt.axhline(precs[1], color=c2, alpha=a)
    hl3 = plt.axhline(recs[1], color=c3, alpha=a)
    hl4 = plt.axhline(f1s[1], color=c4, alpha=a)

    # add some text for labels, title and axes ticks
    plt.xlabel("cv fold")
    plt.ylabel("score [%]")
    plt.yticks(np.linspace(0, 100, 21))
    plt.xticks(ind+2*width, [str(i+1) for i in ind])
    plt.ylim([50, 100])
    plt.xlim([-width, folds])

    # reorder the labels
    plt.legend([bar1, hl1, bar2, hl2, bar3, hl3, bar4, hl4], ["accuracy", "mean accuracy", "precision",
                                                              "mean precision", "recall", "mean recall", "f1-score",
                                                              "mean f1-score"], fontsize=8, ncol=4, loc='upper center')

    plt.tight_layout()
    plt.savefig(os.path.join(output, 'cv-results'+title_+'.pdf'), bbox_inches='tight')
    # plt.savefig(os.path.join(output, 'cv-results.png'), bbox_inches='tight', dpi=400)
    plt.show()
    plt.close('all')


# ______________________________________________________________________________________________________________________
def plot_spectrum(spectrum, rec, cmd_args, frequency_vector):
    f_size = 17
    plt.clf()
    plt.figure(figsize=(20, 12.5))

    color_map = plt.get_cmap('nipy_spectral')
    colors = [color_map(i) for i in np.linspace(0, 1, len(rec.signal_names))]

    # /media/lukas/0084DB135AE0B341/normal_abnormal/normal/tuh_eeg/v0.6.0/edf/004/00000692/s01_2010_10_27/a_1.edf
    class_name = rec.name.split('/')[-8]
    edf_file_name = '/'.join(rec.name.split('/')[-4:])
    plt.title(class_name + ', ' + edf_file_name + ', d=' + str(int(rec.duration)) + 's, ws=' + str(cmd_args.windowsize)
              + 's', fontsize=f_size)

    for electrode_id, electrode in enumerate(spectrum):
        plt.plot(frequency_vector, electrode, linewidth=2, label=rec.signal_names[electrode_id], alpha=.8,
                 color=colors[electrode_id])

    plt.xscale('symlog')
    plt.yscale('symlog')
    plt.grid(True)

    plt.xlim([frequency_vector[1]-0.1, rec.sampling_freq/2 + 15])

    plt.xlabel("frequency (Hz)", fontsize=f_size)
    plt.ylabel("mean amplitude (mV)", fontsize=f_size)

    x_tick_locations = [0, 1, 4, 8, 12, 30, 60, 120]
    # since some recordings have higher sampling frequency, adapt plot
    if rec.sampling_freq > 256:
        x_tick_locations += [180]
    x_tick_labels = [str(x_tick_location) for x_tick_location in x_tick_locations]
    plt.xticks(x_tick_locations, x_tick_labels, fontsize=f_size)
    plt.yticks(fontsize=f_size)

    plt.legend(loc="best", ncol=11, mode="expand", fontsize=f_size)

    if cmd_args.output is not None:
        path = os.path.join(cmd_args.output, class_name, edf_file_name.replace('edf', 'pdf').replace('/', '_'))
        # logging.info("\t\t\t\tSaving spectrum plot to {}.".format(path))
        plt.savefig(path, bbox_inches="tight")
    else:
        plt.show()
    plt.close("all")


# ______________________________________________________________________________________________________________________
def scatter_features(features_ab, threshold, out_dir=None):
    colors = ['r', 'b']
    for i, features in enumerate(features_ab):
        mean = np.mean(features)
        median = np.median(features)

        plot_rng = RandomState(349834)
        plt.scatter(i + plot_rng.randn(len(features)) * 0.1, features, alpha=0.7, color=colors[i], edgecolor='k')

        plt.plot(-.5 + 2 * i, mean, marker='s', markersize=15, color=colors[i], label='mean')
        plt.plot(-.75 + 2.5 * i, median, marker='o', markersize=15, color=colors[i], label='median')

        yerr_down = abs(mean - np.percentile(features, 25))
        yerr_up = abs(np.percentile(features, 75) - mean)
        plt.errorbar(-.5 + 2 * i, mean, [[yerr_down], [yerr_up]], color='k')
        # print(yerr_down, yerr_up)

        yerr_down = abs(median - np.percentile(features, 25))
        yerr_up = abs(np.percentile(features, 75) - median)
        plt.errorbar(-.75 + 2.5 * i, median, [[yerr_down], [yerr_up]], color='k')
        # print(yerr_down, yerr_up)

    plt.xticks([-.75, -.5, 0, 1, 1.5, 1.75], ['median', 'mean', 'excess_beta', 'normal_eeg', 'mean', 'median'],
               rotation=30)
    plt.yticks(np.arange(0, .0011, 0.0001))

    plt.xlim(-1, 2)
    plt.ylim(-.0001, .001)

    plt.title('median beta amplitude over all channels')
    plt.grid(True)
    # plt.legend(loc='best')

    plt.axhline(threshold, color='m')
    plt.text(.35, threshold*1.1, 'threshold', color='m')

    if out_dir is not None:
        plt.savefig(os.path.join(out_dir, 'scatter.pdf'), bbox_inches="tight")
    else:
        plt.show()
    plt.close("all")


# ______________________________________________________________________________________________________________________
def plot_important_features(out_dir, sorted_zip, title):
    sorted_features, sorted_mean_values, sorted_std_values = [], [], []
    for entry in sorted_zip:
        sorted_features.append(entry[0])
        sorted_mean_values.append(entry[1])
        sorted_std_values.append(entry[2])

    plt.bar(np.arange(len(sorted_features))+.1, sorted_mean_values, #yerr=sorted_std_values,
            error_kw=dict(ecolor='k', alpha=1), width=.8, color='grey')
    plt.title(' '.join(title))

    plt.xticks(np.arange(len(sorted_features))+.5, sorted_features, rotation='vertical', fontsize=8)
    plt.xlim(0, len(sorted_features))
    plt.ylabel("importance [%]")
    plt.xlabel("feature")
    plt.grid(False)
    plt.tight_layout()

    plt.savefig(os.path.join(out_dir, '_'.join(title) + '.pdf'))
    # plt.savefig(os.path.join(out_dir, '_'.join(title) + '.png'), dpi=400)

    plt.show()
    plt.close('all')


# ______________________________________________________________________________________________________________________
def plot_oob_scores(out_dir, oob_scores, estimators):
    oob_scores *= 100
    plt.bar(np.arange(len(oob_scores))+.1, oob_scores, width=.8)
    plt.xticks(np.linspace(1, len(oob_scores), len(oob_scores))-.5,
               np.linspace(1, len(oob_scores), len(oob_scores), dtype=int))
    plt.yticks(np.linspace(0, 100, 21, dtype=int), np.linspace(0, 100, 21, dtype=int))
    plt.ylim([50, 100])
    plt.xlabel('cv fold')
    plt.ylabel('oob score [%]')
    plt.title("RF (n={}) OOB Score".format(estimators))
    plt.tight_layout()

    plt.axhline(np.mean(oob_scores), color='black')

    plt.savefig(os.path.join(out_dir, 'rf_(n={})oob_score.pdf'.format(estimators)))
    # plt.savefig(os.path.join(out_dir, '_'.join(title) + '.png'), dpi=400)

    plt.show()
    plt.close('all')


# ______________________________________________________________________________________________________________________
def plot_confusion_matrices(out_dir, cms, classes, title='confusion matrix', cmap=plt.cm.Blues):
    for cm_id, cm in enumerate(cms):
        plot_confusion_matrix(out_dir, cm, classes, title_=title + ' ' + str(cm_id), cmap=cmap)


# ______________________________________________________________________________________________________________________
def plot_confusion_matrix(out_dir, cm, classes, title_='', cmap=plt.cm.Blues):
    """
    Taking from sklearn homepage
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    import itertools
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    cm *= 100

    title = ' '.join(['avg confusion matrix', title_])

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes, rotation=90)

    fmt = '.2f'
    thresh = cm.min() + (cm.max()-cm.min()) / 2.
    meanings = np.asarray([['true negative', 'false positive'], ['false negative', 'true positive']])
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt) + '\n'+meanings[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.grid(False)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

    plt.savefig(os.path.join(out_dir, title+'.pdf'), bbox_inches='tight')

    plt.show()
    plt.close('all')


# ______________________________________________________________________________________________________________________
def plot_feature_importances(out_dir, importances, feature_labels=None, n_most_important=10):
    """ plots the 10 most important features per fold.
    alsp plots the mean of all "10 most important features per fold" over all folds.
    :param importances: holds the importance values of all features for every fold
    :param out_dir: output directory
    :param feature_labels: the names of the features in the order they were computed and put into the feature vector
    (which should be the same order as the feature importances. check. double check!)
    :param n_most_important: the number that specifies the most important features that will be plot
    """

    important_features = []
    # importances has shape n_folds x n_features
    # iterate all folds and plot the n most important features
    for fi_id, fi in enumerate(importances):
        n_features = len(fi)
        # create ids for the feature labels
        ids = np.arange(n_features)
        zipped = zip(fi, ids)
        # sort the feature importances with their ids by highest value
        sorted_zipped = sorted(zipped, reverse=True)
        sorted_fimp, sorted_ids = zip(*sorted_zipped)
        # take the n highest values
        fimp, fimp_ids = list(sorted_fimp[:n_most_important]), list(sorted_ids[:n_most_important])

        important_features.extend(fimp_ids)

        # TODO: also add reordered versions of this plot for a better analysis
        plt.bar(np.arange(len(fimp_ids))+0.1, fimp, width=0.8, label="fold " + str(fi_id), color='grey')
        plt.title("10 most important features in fold " + str(fi_id+1))

        if feature_labels is not None:
            fimp_ids = feature_labels[fimp_ids]

        plt.xticks(np.arange(len(fimp_ids))+.5, fimp_ids, rotation='vertical')
        plt.yticks(np.linspace(0, np.max(fimp), 20))
        plt.ylabel("importance [%]")
        plt.xlabel("feature")
        plt.grid(False)
        plt.tight_layout()

        plt.savefig(os.path.join(out_dir, "top_importances_fold" + str(fi_id + 1) + ".pdf"))
        # plt.savefig(os.path.join(out_dir, "top_importances_fold" + str(fi_id + 1) + ".png"), dpi=400)
        plt.clf()

    # mean of all important features over folds
    important_features = np.unique(important_features)

    # mean over folds
    importances_mean = np.mean(importances, axis=0)
    important_features_mean = importances_mean[important_features]
    importances_std = np.std(importances, axis=0)
    important_features_std = importances_std[important_features]

    # for i, j in zip(importances_mean, importances_std):
    #     print(i, j)

    if feature_labels is not None:
        important_features = feature_labels[important_features]

    # sort the important features and their ids by highest value
    sorted_zip = sorted(zip(important_features, important_features_mean, important_features_std), key=lambda x: -x[1])
    title = ['mean', 'importance', 'of', 'important', 'features', 'over', 'all', 'folds', 'by', 'value']
    plot_important_features(out_dir, sorted_zip, title)

    # sort the important features and their ids by electrode
    zipped = zip(important_features, important_features_mean, important_features_std)
    sorted_zip = sorted(zipped, key=lambda x: str(x[0]).split('_')[-1] if '_' in str(x[0]) else x[0])
    title = ['mean', 'importance', 'of', 'important', 'features', 'over', 'all', 'folds', 'by', 'electrode']
    plot_important_features(out_dir, sorted_zip, title)

    # sort the important features and their ids by domain
    zipped = zip(important_features, important_features_mean, important_features_std)
    sorted_zip = sorted(zipped, key=lambda x: str(x[0]).split('_')[0] if '_' in str(x[0]) else x[0])
    title = ['mean', 'importance', 'of', 'important', 'features', 'over', 'all', 'folds', 'by', 'domain']
    plot_important_features(out_dir, sorted_zip, title)

    # sort the important features and their ids by frequency
    zipped = zip(important_features, important_features_mean, important_features_std)
    # how to "sort" sex, age and time features that don't have frequencies in their name? make a dummy that puts em to
    # the front
    dummy = [None, 100, None, 100, None]
    sorted_zi = sorted(zipped, key=lambda x: io.natural_key(str(x[0]).split('_')[-2]) if '_' in str(x[0]) else dummy)
    title = ['mean', 'importance', 'of', 'important', 'features', 'over', 'all', 'folds', 'by', 'frequency']
    plot_important_features(out_dir, sorted_zi, title)


# ______________________________________________________________________________________________________________________
def plot_feature_importances_spatial(out_dir, importances, feature_labels, n_most_important=5):
    """ plot the feature importances on a scheme of the head to add spatial relationship.
    :param importances: the feature importances as returned by the RF. shape n_folds x n_features
    :param out_dir: the output directory where the plots are saved to
    :param feature_labels: the list of features
    :param n_most_important: the number of features that should be analyzed
    """
    plt.rcParams['axes.facecolor'] = 'white'

    electrode_to_feature_labels = {
        'A1': [],
        'A2': [],
        'C3': [],
        'C4': [],
        'CZ': [],
        'F3': [],
        'F4': [],
        'F7': [],
        'F8': [],
        'FP1': [],
        'FP2': [],
        'FZ': [],
        'O1': [],
        'O2': [],
        'P3': [],
        'P4': [],
        'PZ': [],
        'T3': [],
        'T4': [],
        'T5': [],
        'T6': [],
    }

    feature_labels = [feature_label.replace("boundedvariation", "boundedvar") for feature_label in feature_labels]



    mean_importances = np.mean(importances, axis=0)

    # assign the mean importances to the respective electrode
    for feature_label_id, feature_label in enumerate(feature_labels):
        for electrode_name in ELECTRODE_NAMES:
            if electrode_name in feature_label.split('_')[-1]:
                electrode_to_feature_labels[electrode_name].append(feature_label_id)

    new_labels = []
    for feature_label in feature_labels:
        if "plv" in feature_label:
            new_labels.append(feature_label)
        else:
            new_labels.append('_'.join(feature_label.split('_')[:-1]))
    feature_labels = new_labels


    # this version of the plot is not quite as useful as the other version
    # print(electrode_to_feature_labels['O1'], len(electrode_to_feature_labels['O1']))

    # # TODO: make this dynamic for more then 3 most important
    # bars1, labels = [], []
    # bars2, bars3 = [], []
    # # for every electrode sort its assigned feature importances by highest value
    # for electrode_id, electrode in enumerate(electrode_names):
    #     zipped = zip(mean_importances[np.asarray(electrode_to_feature_labels[electrode])],
    #                  electrode_to_feature_labels[electrode])
    #     sorted_zipped = sorted(zipped, reverse=True)
    #     sorted_mean_imp, sorted_mean_imp_ids = zip(*sorted_zipped)
    #     most_important_mean_imp = sorted_mean_imp[:n_most_important]
    #     most_important_mean_imp_ids = sorted_mean_imp_ids[:n_most_important]
    #
    #     print(most_important_mean_imp, feature_labels[np.asarray(most_important_mean_imp_ids)])
    #
    #     # plt.subplot(len(electrode_names)/3, (electrode_id % 3)+1, (electrode_id % 7)+1)
    #
    #     # for i in range(n_most_important):
    #     #     print(i + 1 + electrode_id * n_most_important)
    #     #     plt.bar(i + 1 + electrode_id * n_most_important, most_imported_mean_imp[i], color=colors[electrode_id],
    #     #             alpha=.2*i)
    #
    #     bars1.append(most_important_mean_imp[0])
    #     bars2.append(most_important_mean_imp[1])
    #     bars3.append(most_important_mean_imp[2])
    #     # plt.xlabel(feature_labels[np.asarray(most_imported_mean_imp_ids)])
    #     sub_labels = feature_labels[np.asarray(most_important_mean_imp_ids)]
    #     # sub_labels = [sub_label + ' ' + str(most_imported_mean_imp[i]) for i, sub_label in enumerate(sub_labels)]
    #     labels.extend(sub_labels)
    #
    # # print(len(bars1))
    # # print(np.linspace(0,3*len(bars1)-1, len(bars1)))
    # plt.bar(np.linspace(0, 3*len(bars1)-3, len(bars1)), bars1, color='red')
    # plt.bar(np.linspace(0, 3*len(bars2)-3, len(bars2)) + 1, bars2, color='orangered', alpha=.7)
    # plt.bar(np.linspace(0, 3*len(bars3)-3, len(bars3)) + 2, bars3, color='orange', alpha=.4)
    #
    # for a, b in zip(np.linspace(0, 3*len(bars1)-3, len(bars2)), bars1):
    #     plt.text(a + .17, .05, str(b)[:4], rotation=90, fontsize=6, fontweight='bold')
    #
    # for a, b in zip(np.linspace(0, 3*len(bars2)-3, len(bars2)) + 1, bars2):
    #     plt.text(a + .17, .05, str(b)[:4], rotation=90, fontsize=6, fontweight='bold')
    #
    # for a, b in zip(np.linspace(0, 3*len(bars3)-3, len(bars3)) + 2, bars3):
    #     plt.text(a + .17, .05, str(b)[:4], rotation=90, fontsize=6, fontweight='bold')
    #
    #
    # # TODO: color the labels in the same way as the bars
    # plt.xticks(np.arange(len(labels)), labels, rotation=90, fontsize=8)
    # # plt.xlabel(labels, rottaion=90)
    # plt.xlim([0, 3*len(bars1)])
    # plt.tight_layout()
    # plt.savefig(os.path.join(out_dir, '3_most_important_features_per_electrode.pdf'))
    # plt.show()
    # plt.close('all')

    # _______________________________________________________________________________
    # offset due to aligning strings at the bottom left corner
    offset_x = 7.7
    offset_y = -6.3
    # the head
    ax = plt.gca()
    c1 = plt.Circle((50+offset_x, 50+offset_y), 50, fill=False, color='black', alpha=.5)
    ax.add_artist(c1)
    c2 = plt.Circle((50+offset_x, 50+offset_y), 40, fill=False, color='black', alpha=.5)
    ax.add_artist(c2)
    # the nose
    plt.plot([47+offset_x, 50+offset_x, 53+offset_x], [100+offset_y, 103+offset_y, 100+offset_y], color='black',
             alpha=.5, linewidth=.1)
    # from ear to ear
    plt.plot([0+offset_x, 100+offset_x], [50+offset_y, 50+offset_y], color='black', alpha=.5, linewidth=.1)
    # from nasion to inion
    plt.plot([50+offset_x, 50+offset_x], [0+offset_y, 100+offset_y], color='black', alpha=.5, linewidth=.1)

    # if n_most_important == 3:
    #     colors = ['red', 'orangered', 'orange']
    # else:
    #     color_map = plt.get_cmap('nipy_spectral')
    #     colors = [color_map(i) for i in np.linspace(0, 1, n_most_important)]

    # put the n most important feature of a electrode at the respective position
    # if only patient features are used, this will crash since the electrode_to_feature_labels map will not contain
    # any entry. this is because "age" and "sex" feature_label does not contain any electrode
    for electrode in ELECTRODE_NAMES:
        if not electrode_to_feature_labels[electrode]:
            continue
        zipped = zip(mean_importances[np.asarray(electrode_to_feature_labels[electrode])],
                     electrode_to_feature_labels[electrode])
        sorted_zipped = sorted(zipped, reverse=True)
        sorted_mean_imp, sorted_mean_imp_ids = zip(*sorted_zipped)
        most_important_mean_imp = sorted_mean_imp[:n_most_important]
        most_important_mean_imp_ids = sorted_mean_imp_ids[:n_most_important]

        [x, y] = ELECTRODE_LOCATIONS[electrode]
        for i in range(min(n_most_important, len(electrode_to_feature_labels['O1']))):
            # print(most_important_mean_imp[i])
            # print(feature_labels[most_important_mean_imp_ids[i]])
            name = feature_labels[most_important_mean_imp_ids[i]]
            value = str(most_important_mean_imp[i])[:7]
            # plot the electrode in the background
            #plt.text(x, y - 10, electrode, fontsize=40, color='blue', alpha=.03)
            if "sync_" in name:
                name = name.replace("sync_", "")
            if "boundedvar" in name:
                name = name.replace("fft_", "")
            if n_most_important == 3:
                # plot the n most important features together with their importances at their location
                plt.text(x, y-i*5.5, name, fontsize=6, fontweight='bold', color='black', alpha=1-.1*i)
                plt.text(x, y-(i+.53)*5.5, str(value),  fontsize=6, color='black', alpha=1-.3*i)
            else:
                plt.text(x, y-1-i*2.5, name + ' ' + value, fontsize=5, fontweight='bold', color='black', alpha=1-.15*i)

    title = [str(n_most_important), 'most', 'important', 'features', 'per', 'electrode']
    plt.title(' '.join(title))
    plt.xlim([-10, 125])
    plt.xticks([])
    plt.ylim([-10, 105])
    plt.yticks([])
    plt.tight_layout()

    plt.show()
    plt.savefig(os.path.join(out_dir, '_'.join(title) + '.pdf'))
    # plt.savefig(os.path.join(out_dir, '_'.join(title) + '.png'), dpi=400)
    plt.close('all')


# ______________________________________________________________________________________________________________________
def plot_phase_lock_values(out_dir, class_, file_names, subbands, feature_matrix):
    for file_name, feature_vector in zip(file_names, feature_matrix):
        phase_lock_values = np.zeros((8, 21, 21))
        band_plvs = np.asarray(np.split(feature_vector, 8))
        for band_id, plvs in enumerate(band_plvs):
            phase_lock_values[band_id] = squareform(plvs)
            np.fill_diagonal(phase_lock_values[band_id], 1)
        phase_lock_values = np.asarray(phase_lock_values)

        name = '/'.join(file_name.split('/')[-3:])
        pdf_name = name.replace('/', '-')[:-4]
        subdir = os.path.join(out_dir, pdf_name)
        if not os.path.exists(subdir):
            os.makedirs(os.path.join(subdir))
        pdf_pages = PdfPages(os.path.join(subdir, pdf_name + '.pdf'))

        n_bands, n_signals, n_signals = phase_lock_values.shape
        for i, subband_plv_values in enumerate(phase_lock_values):
            mpl.rcParams['figure.figsize'] = (15, 10)
            plt.matshow(subband_plv_values, vmin=0, vmax=1)

            hz = '-'.join([str(freq) for freq in subbands[i]])
            title = ', '.join([class_, name, hz + ' hz'])
            plt.title(title)
            plt.xticks(np.arange(n_signals), [electrode.split('-')[0][-3:] for electrode in ELECTRODE_NAMES],
                       rotation=90)
            plt.yticks(np.arange(n_signals), [electrode.split('-')[0][-3:] for electrode in ELECTRODE_NAMES])
            plt.grid(False)
            plt.colorbar(ticks=np.linspace(0, 1, 11), location='left', fraction=0.046, pad=0.04)
            ax = plt.gca()
            ax.xaxis.tick_bottom()
            ax.yaxis.tick_right()

            # plt.show()
            pdf_pages.savefig(bbox_inches='tight')
            # plt.savefig(os.path.join(out_dir, '_'.join(title) + '.png'), dpi=400)

        pdf_pages.close()
        plt.close('all')

        # takes quite some time and output is rather large. 600kb per plot collection
        pdf_pages2 = PdfPages(os.path.join(subdir, pdf_name + '-spatial.pdf'))
        plot_connectivity_in_all_bands(pdf_pages2, phase_lock_values, subbands)


# ______________________________________________________________________________________________________________________
def plot_polar_histogram(file_name, instantaneous_pahses, subband, band_id, elec1_id, elec2_id):
    seaborn.set_style('dark')
    mpl.rcParams['figure.figsize'] = (10, 10)
    inst_phase_diffs = instantaneous_pahses[band_id][elec1_id] - instantaneous_pahses[band_id][elec2_id]
    xs, ys = np.cos(inst_phase_diffs), np.sin(inst_phase_diffs)
    xs_mean, ys_mean = np.mean(xs), np.mean(ys)

    ax = plt.subplot(111, projection='polar')
    # TODO: make y axis a log scale?
    n, bins, patches = plt.hist(np.arctan2(ys, xs), 21, alpha=.5)

    mean_angle = np.mean(np.arctan2(ys_mean, xs_mean))
    plt.axvline(mean_angle, color='r')
    # TODO: don't re-compute. make this a look-up
    plv = np.sqrt(np.square(xs_mean) + np.square(ys_mean))
    plt.text(mean_angle, plt.yticks()[0][-1] * 1.13, 'PLV: {:.2f}'.format(plv),
             rotation=np.rad2deg(mean_angle),
             fontsize=16, weight='bold')

    plt.xticks(fontsize=12)
    plt.yticks(fontsize=12)

    name = '/'.join(file_name.split('/')[-3:])
    plt.title(name + '\nPLV: {} and {} in {} hz'.format(ELECTRODE_NAMES[elec1_id], ELECTRODE_NAMES[elec2_id], subband),
              y=1.08)


# ______________________________________________________________________________________________________________________
def plot_plvs_on_head(file_name, subband, band_id, electrode_id, phase_lock_values):
    seaborn.set_style("white")
    mpl.rcParams['figure.figsize'] = (10, 7)
    # offset due to aligning strings at the bottom left corner
    offset_x = 0
    offset_y = 0
    # the head
    ax = plt.gca()
#    ax.set_axis_bgcolor('white')
    c1 = plt.Circle((50+offset_x, 50+offset_y), 50, fill=False, color='black', alpha=.5)
    ax.add_artist(c1)
    c2 = plt.Circle((50+offset_x, 50+offset_y), 40, fill=False, color='black', alpha=.5)
    ax.add_artist(c2)
    # the nose
    plt.plot([47+offset_x, 50+offset_x, 53+offset_x], [100+offset_y, 103+offset_y, 100+offset_y], color='black',
             alpha=.5, linewidth=.2)
    # from ear to ear
    plt.plot([0+offset_x, 100+offset_x], [50+offset_y, 50+offset_y], color='black', alpha=.5, linewidth=.2)
    # from nasion to inion
    plt.plot([50+offset_x, 50+offset_x], [0+offset_y, 100+offset_y], color='black', alpha=.5, linewidth=.2)

    p = ELECTRODE_LOCATIONS[ELECTRODE_NAMES[electrode_id]]
    plt.plot(p[0]+offset_x, p[1]+offset_y, color='k', linestyle='', marker='o', markersize=58, zorder=1231023120831)
    plt.plot(p[0]+offset_x, p[1]+offset_y, color='r', linestyle='', marker='o', markersize=63)

    for electrode_id2 in range(len(ELECTRODE_NAMES)):
        p = ELECTRODE_LOCATIONS[ELECTRODE_NAMES[electrode_id2]]
        # plt.text(p[0], p[1] - 10, electrode_names[electrode_id2], fontsize=40, color='blue', alpha=.3)
        plt.plot(p[0]+offset_x, p[1]+offset_y, linestyle='', marker='o',
                 markersize=58 * phase_lock_values[band_id][electrode_id][electrode_id2],
                 color=[1 - phase_lock_values[band_id][electrode_id][electrode_id2],
                        1 - phase_lock_values[band_id][electrode_id][electrode_id2],
                        1 - phase_lock_values[band_id][electrode_id][electrode_id2]])

    plt.xlim([-15, 115])
    plt.xticks([])
    plt.ylim([-10, 105])
    plt.yticks([])
    plt.tight_layout()
    name = '/'.join(file_name.split('/')[-3:])
    plt.title('{}\nPLV: {} in {} hz'.format(name, ELECTRODE_NAMES[electrode_id], subband))


# ______________________________________________________________________________________________________________________
def plot_connectivity_in_all_bands(pdf_pages, phase_lock_values, subbands):
    electrode_histogram_location_map = {
        'A1': 15,
        'A2': 21,
        'C3': 17,
        'C4': 19,
        'CZ': 18,
        'F3': 10,
        'F4': 12,
        'F7': 9,
        'F8': 13,
        'FP1': 3,
        'FP2': 5,
        'FZ': 11,
        'O1': 31,
        'O2': 33,
        'P3': 24,
        'P4': 26,
        'PZ': 25,
        'T3': 16,
        'T4': 20,
        'T5': 23,
        'T6': 27,
    }

    for elec_id in range(len(ELECTRODE_NAMES)):
        for i in range(len(ELECTRODE_NAMES)):
            plt.subplot(5, 7, electrode_histogram_location_map[ELECTRODE_NAMES[i]])
            color = 'r' if elec_id == i else 'k'
            plt.title(ELECTRODE_NAMES[i], color=color)
            plt.barh(np.arange(len(phase_lock_values)),
                     len(phase_lock_values) * [1],
                     height=1,
                     color=[3 * [1 - phase_lock_values[0][i][elec_id]],
                            3 * [1 - phase_lock_values[1][i][elec_id]],
                            3 * [1 - phase_lock_values[2][i][elec_id]],
                            3 * [1 - phase_lock_values[3][i][elec_id]],
                            3 * [1 - phase_lock_values[4][i][elec_id]],
                            3 * [1 - phase_lock_values[5][i][elec_id]],
                            3 * [1 - phase_lock_values[6][i][elec_id]],
                            3 * [1 - phase_lock_values[7][i][elec_id]]],
                     )
            # plt.text(.5, i+.25, str([phase_lock_values[0][i][elec_id]]), color=3*[phase_lock_values[0][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[1][i][elec_id]]), color=3*[phase_lock_values[1][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[2][i][elec_id]]), color=3*[phase_lock_values[2][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[3][i][elec_id]]), color=3*[phase_lock_values[3][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[4][i][elec_id]]), color=3*[phase_lock_values[4][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[5][i][elec_id]]), color=3*[phase_lock_values[5][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[6][i][elec_id]]), color=3*[phase_lock_values[6][i][elec_id]])
            # plt.text(.5, i+.25, str([phase_lock_values[7][i][elec_id]]), color=3*[phase_lock_values[7][i][elec_id]])

            if ELECTRODE_NAMES[i] == 'A2':
                ax = plt.gca()
                ax.yaxis.tick_right()
                plt.yticks(np.arange(len(subbands)), subbands)
                plt.xticks([])
            else:
                plt.xticks([])
                plt.yticks([])
        pdf_pages.savefig(bbox_inches='tight')
        # plt.savefig(os.path.join(out_dir, '_'.join(title) + '.png'), dpi=400)

    pdf_pages.close()
    plt.close('all')


# ______________________________________________________________________________________________________________________
# def visualize_dwt(signal, wavelet, level):
#     dwtviz.dwtviz(signal, wavelet=wavelet, level=level)


def plot_cv_mean_roc_auc_curves(pipeline_results, title="", midline="", fs=20):
    from sklearn.metrics import roc_curve, roc_auc_score
    from scipy import interp
    plt.figure(figsize=(12,12))
    plt.title(title)
    n_folds = len(pipeline_results)
    #colors = [plt.cm.brg(i) for i in np.linspace(0, 0.9, n_folds)]
    colors = ["b" for i in range(n_folds)]
    plt.plot([0,1],[0,1], color="black", linestyle="--", label=midline)
    tprs, aucs = [], []
    mean_fpr=np.linspace(0,1,100)
    for i in range(n_folds):
        y_test = pipeline_results[i]["y_test"]
        # be careful here
        try:
            y_test_pred_proba = pipeline_results[i]["y_test_pred_proba"][:, 1]
        except KeyError:
            y_test_pred_proba = pipeline_results[i]["y_test_score"]
        fold_roc_curve = roc_curve(y_true=y_test, y_score=y_test_pred_proba)
        fold_roc_auc = roc_auc_score(y_true=y_test, y_score=y_test_pred_proba)
        plot_roc_auc_curve(fold_roc_curve, fold_roc_auc, colors, i, label=False, fs=fs)

        fpr, tpr, _ = fold_roc_curve
        aucs.append(fold_roc_auc)
        tprs.append(interp(mean_fpr, fpr, tpr))
        tprs[-1][0] = 0.0
        mean_tpr = np.mean(tprs, axis=0)
        mean_tpr[-1] = 1.0
    
    aucs = 100*np.array(aucs)
    plt.plot(mean_fpr, mean_tpr, color='r', lw=3, #alpha=.8,
             label="Mean AUC {:.2f} $\pm$ {:.2f}".format(np.mean(aucs), np.std(aucs)))
    plt.legend(loc="lower right", fontsize=fs)


def plot_cv_roc_auc_curves(pipeline_results, title="", midline="", fs=20):
    from sklearn.metrics import roc_curve, roc_auc_score
    plt.figure(figsize=(12,12))
    plt.title(title)
    n_folds = len(pipeline_results)
    colors = [plt.cm.brg(i) for i in np.linspace(0, 0.9, n_folds)]
    plt.plot([0,1],[0,1], color="black", linestyle="--", label=midline)
    for i in range(n_folds):
        y_test = pipeline_results[i]["y_test"]
        # be careful here
        try:
            y_test_pred_proba = pipeline_results[i]["y_test_pred_proba"][:, 1]
        except KeyError:
            y_test_pred_proba = pipeline_results[i]["y_test_score"]
        fold_roc_curve = roc_curve(y_true=y_test, y_score=y_test_pred_proba)
        fold_roc_auc = roc_auc_score(y_true=y_test, y_score=y_test_pred_proba)
        plot_roc_auc_curve(fold_roc_curve, fold_roc_auc, colors, i, fs=fs)
    plt.legend(loc="lower right", fontsize=fs)
    
        
def plot_roc_auc_curve(roc_curve, roc_auc, colors, i, label=True, fs=20):
    fpr, tpr, _ = roc_curve
    if label:
        label_txt = "Run {} ROC AUC {:.3f}".format(i, roc_auc) 
    else:
        label_txt=""
    plt.plot(fpr, tpr, color=colors[i], label=label_txt)
    plt.xlabel("False positive rate (1-specificity)", fontsize=fs)
    plt.ylabel("True positive rate (sensitivity)", fontsize=fs)
    plt.xticks(np.linspace(0, 1, 11), fontsize=fs)
    plt.yticks(np.linspace(0, 1, 11), fontsize=fs)  
    
    
def plot_cv_predicted_vs_true_ages(pipeline_results, title="", train_or_test="test"):
    from sklearn.metrics import mean_squared_error
    plt.figure(figsize=(12,12))
    plt.title(title)
    n_folds = len(pipeline_results)
    colors = [plt.cm.brg(i) for i in np.linspace(0, 0.9, n_folds)]
    plt.plot([0,100],[0,100], color="black", linestyle="--", label="ground truth")
    for i in range(n_folds):
        y_test = pipeline_results[i]["y_"+train_or_test]
        y_test_pred = pipeline_results[i]["y_"+train_or_test+"_pred"]
        print(np.min(y_test), np.max(y_test), np.min(y_test_pred), np.max(y_test_pred))
        
        mse = mean_squared_error(y_pred=y_test_pred, y_true=y_test)
        
        plot_predicted_vs_true_age(y_test, y_test_pred, colors, i, mse)
        y_test_pred_mean = np.mean(y_test_pred)
        y_test_pred_std = np.std(y_test_pred)
        
        #if len(np.unique(y_test_pred)) > 1:
        #    plt.plot(np.poly1d(np.polyfit(y_test_pred, y_test, 1))(np.unique(y_test_pred)), np.unique(y_test_pred), color=colors[i])

        #plt.plot(np.arange(10) * y_test_pred_mean, np.arange(10) * y_test_pred_std, color="cyan")
        
        
def plot_predicted_vs_true_age(y_true, y_pred, colors, i, rmse):
    plt.scatter(y_true, y_pred, label="Run {} RMSE {:.2f}".format(i, rmse), color=colors[i], alpha=.3)
    plt.xlabel("true age")
    plt.ylabel("predicted age")
    plt.xticks(np.linspace(0, 100, 11))
    plt.yticks(np.linspace(0, 100, 11))
    plt.legend(loc="lower right")
    
def show_losses(epochs_df, before_stop_df, best_id):
    fig = plt.figure(figsize=(15, 10))
    grid = plt.GridSpec(2, 2)
    ax1 = plt.subplot(grid[:, 0])
    ax2 = plt.subplot(grid[0, 1])
    ax3 = plt.subplot(grid[1, 1])

    fig.add_subplot(before_stop_df[["train_loss", "valid_loss", "test_loss"]][:best_id].plot(ax=ax1))
    ax1.set_xlabel("epoch")
    ax1.set_ylabel("nll loss")
    #ax1.semilogy()
    ax1.set_title("until best epoch")

    fig.add_subplot(before_stop_df[["train_loss", "valid_loss", "test_loss"]][best_id:].plot(ax=ax2), sharey=ax3)
    #ax2.semilogy()
    plt.legend([])
    ax2.set_title("until first stop")

    fig.add_subplot(epochs_df[["train_loss", "valid_loss", "test_loss"]][best_id:].plot(ax=ax3), sharey=ax2)
    plt.legend([])
    #ax3.semilogy()
    ax3.set_title("until second stop")
    
def show_error_rate(epochs_df):
    epochs_df[["train_misclass", "valid_misclass", "test_misclass"]].plot()