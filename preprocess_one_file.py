#!/usr/bin/env python3.5
from datetime import datetime, date
import numpy as np
import os

from feature_generation import frequency_feature_generator
from feature_generation import wavelet_feature_generator
from feature_generation import phase_feature_generator
from feature_generation import time_feature_generator
from feature_generation import meta_feature_generator
from utils import logger, arg_parser_one_file
from utils.io import FileReader, JsonIO

SFREQ = 100
CLIP_VALUE = 800
CHANNELS = sorted(['FP1', 'FP2', 'F3', 'F4', 'C3', 'C4', 'P3', 'P4', 'O1', 'O2', 'F7',
                   'F8', 'T3', 'T4', 'T5', 'T6', 'A1', 'A2', 'FZ', 'CZ', 'PZ'])
# BAND_LIMITS = [(0, 2), (2, 4), (4, 8), (8, 13), (13, 18), (18, 24), (24, 30), (30, 50)]
BAND_LIMITS = [(0, 2), (1, 3), (2, 4), (3, 6), (4, 8), (6, 10), (8, 13), (10, 15),
               (13, 18), (15, 21), (18, 24), (21, 27), (24, 30), (27, 40), (30, 50)]
AGG_MODE = "median"
WINDOW_NAME = "blackmanharris"
EPOCH_DURATION_S = 4  # s
IN_DIR = "/home/gemeinl/data/tuh-abnormal-eeg/cleaned/cleaned_final_rules_100Hz_500recs_3min/"
OUT_DIR = "/home/gemeinl/data/tuh-abnormal-eeg/cleaned/feature_vectors_cleaned_final_rules_100Hz_500recs_3min/"
VERBOSITY = 20


def split_into_epochs(signals, sfreq, epoch_duration_s):
    """ split the signals into non-overlapping epochs """
    n_samples = signals.shape[-1]
    n_samples_in_epoch = int(epoch_duration_s * sfreq)
    epochs = []
    for i in range(0, n_samples-n_samples_in_epoch, n_samples_in_epoch):
        epoch = np.take(signals, range(i, i + n_samples_in_epoch), axis=-1)
        epochs.append(epoch)
    return np.stack(epochs)


def reject_windows_with_outliers(epochs, outlier_value):
    """ reject windows that contain outliers / clipped values """
    pos_outliers = np.sum(epochs >= outlier_value, axis=(1, 2))
    epochs = epochs[pos_outliers == 0]
    neg_outliers = np.sum(epochs <= -1 * outlier_value, axis=(1, 2))
    epochs = epochs[neg_outliers == 0]
    return epochs


def apply_window_function(epochs, window_name, n_samples_in_epoch):
    """ apply blackmanharris window function """
    from scipy import signal
    method_to_call = getattr(signal, window_name)
    window_function = method_to_call(n_samples_in_epoch)
    return epochs * window_function


def filter_to_bands(signals, bands, sfreq):
    from mne.filter import filter_data
    """ filter signals to frequency ranges defined in bands """
    signals = signals.astype(np.float64)
    (n_signals, n_times) = signals.shape
    band_signals = np.ndarray(shape=(len(bands), n_signals, n_times))
    for band_id, band in enumerate(bands):
        lower, upper = band
        # if lowpass frequency is nyquist frequency, don't make a lowpass
        if upper >= sfreq/2:
            upper = None
        curr_band_signals = filter_data(
            data=signals, sfreq=sfreq, l_freq=lower, h_freq=upper, verbose='error')
        band_signals[band_id] = curr_band_signals
    return band_signals


def main():
    # this could again be moved to an argument parser to conveniently set this
    # when running script on cluster and setting via cmd arg
    in_dir, out_dir = IN_DIR, OUT_DIR
    sfreq = SFREQ
    band_limits = BAND_LIMITS
    channels = CHANNELS
    clip_value = CLIP_VALUE
    epoch_duration_s = EPOCH_DURATION_S
    agg_mode = None if AGG_MODE is None else getattr(np, AGG_MODE)
    window_name = WINDOW_NAME

    # run checks / assertions
    flattened_band_limits = np.array([i for tup in band_limits for i in tup])
    nyquist_freq = sfreq / 2
    assert np.sum(flattened_band_limits > nyquist_freq) == 0, \
        "Cannot have a frequency band limit higher than Nyquist frequency".format(nyquist_freq)
    bin_size = 1./epoch_duration_s
    band_widths = np.array([band_limit[1]-band_limit[0] for band_limit in band_limits])
    assert np.sum(band_widths < bin_size) == 0, \
        "Cannot have frequency bands smaller than bin size {}".format(bin_size)
    assert agg_mode in [np.mean, np.median, np.var, None], \
        "Unknown aggregation mode {}".format(agg_mode)
    assert window_name in ["boxcar", "blackmanharris", "hamming", "hann", "flattop", "triangle"], \
        "Cannot have a window {}".format(window_name)
    # TODO: check if enough samples in epochs
    n_samples_in_epoch = int(sfreq * epoch_duration_s)

    today, now = date.today(), datetime.time(datetime.now())
    logger.create_loggers(output=out_dir, verbosity=VERBOSITY)
    logger.info('Started on {} at {}.'.format(today, now))

    # read files to be processed and pick the one corresponding to job id
    file_reader = FileReader()
    files = file_reader.read_all_file_names(in_dir, ".npy")

    # indexing of sge starts at 1
    file_id = int(os.environ["SGE_TASK_ID"]) - 1
    # if there are more slurm jobs than files to process, exit
    if file_id > len(files):
        exit()

    # load the cleaned signals from file
    file = files[file_id]
    signals = np.load(file)

    # split into epochs and weight the samples
    epochs = split_into_epochs(
        signals=signals, sfreq=sfreq, epoch_duration_s=epoch_duration_s)
    epochs = reject_windows_with_outliers(
        outlier_value=clip_value, epochs=epochs)
    weighted_epochs = apply_window_function(
        epochs=epochs, window_name=window_name,
        n_samples_in_epoch=n_samples_in_epoch)
    band_signals = filter_to_bands(
        signals=signals, bands=band_limits, sfreq=sfreq)

    # generate features and store to file
    all_features, feature_labels = [], []
    start = datetime.now()
    fg = wavelet_feature_generator.WaveletFeatureGenerator(
        elecs=channels, agg=agg_mode, sfreq=sfreq, domain="cwt", wavelet="morl",
        # TODO: is 1.6927 needed? pywt.freqs says this gives freq 120. so a8 will be 120-125?
        # levels=["a8", "d8", "d7", "d6", "d5", "d4", "d3", "d2", "d1"],
        # scales=[1.6927, 3.3854, 6.77, 8.463, 11.282, 15.622, 25.375, 50.75, 101.5],
        levels=["a6", "d6", "d5", "d4", "d3", "d2", "d1"],
        scales=[2.708, 3.385, 4.513, 6.25, 10.15, 20.3, 40.6])
    features = fg.generate_features(weighted_windows=weighted_epochs)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    start = datetime.now()
    fg = wavelet_feature_generator.WaveletFeatureGenerator(
        agg=agg_mode, elecs=channels,
        levels=["a4", "d4", "d3", "d2", "d1"],
        scales=[], sfreq=sfreq, domain="dwt", wavelet="db4")
    features = fg.generate_features(weighted_windows=weighted_epochs)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    start = datetime.now()
    fg = frequency_feature_generator.FrequencyFeatureGenerator(
        agg=agg_mode, bands=band_limits, elecs=channels, sfreq=sfreq)
    features = fg.generate_features(weighted_epochs=weighted_epochs)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    start = datetime.now()
    fg = meta_feature_generator.MetaFeatureGenerator(
        agg=agg_mode, elecs=channels)
    features = fg.generate_features(file)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    start = datetime.now()
    fg = phase_feature_generator.PhaseFeatureGenerator(
        agg=agg_mode, bands=band_limits, elecs=channels, sfreq=sfreq,
        split_f=split_into_epochs, epoch_duration_s=epoch_duration_s)
    features = fg.generate_features(band_signals=band_signals)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    start = datetime.now()
    fg = time_feature_generator.TimeFeatureGenerator(
        agg=agg_mode, elecs=channels, outlier_value=clip_value, sfreq=sfreq)
    features = fg.generate_features(windows=epochs)
    all_features.extend(list(features))
    labels = fg.get_feature_labels()
    feature_labels.extend(list(labels))
    logger.info("Finished {} features in {}."
                .format(fg.domain, datetime.now()-start))

    # create output file with same structure and ending '_feature_vector.npy'
    file = file.replace(in_dir, out_dir)
    file = file.replace(".npy", "_feature_vector.npy")
    if not os.path.exists(os.path.dirname(file)):
        os.makedirs(os.path.dirname(file))
    np.save(file, np.array(all_features))
    logger.info("Wrote features to {}".format(file))

    # write a list containing the full names of all features
    # including electrode and freq range
    if file_id == 0:
        json_io = JsonIO()
        json_io.store(feature_labels, os.path.join(out_dir))

    logger.info('Finished on {} at {}.\n\n'
                .format(date.today(), datetime.time(datetime.now())))


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    main()
