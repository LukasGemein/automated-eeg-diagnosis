#!/usr/bin/env python3.5
from sklearn.exceptions import UndefinedMetricWarning
from datetime import datetime, date
import textwrap as _textwrap
import argparse
import warnings
import logging
import os

from feature_generation import feature_generator
from training import hmm_trainer
from windowing import splitter
from cleaning import cleaner
from utils import io
import pipeline


# ______________________________________________________________________________________________________________________
class LineWrapRawTextHelpFormatter(argparse.RawDescriptionHelpFormatter):
    # this makes the help look much nicer since it allows for a higher line width
    def _split_lines(self, text, width):
        text = self._whitespace_matcher.sub(' ', text).strip()
        return _textwrap.wrap(text, 96)


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    today, now = date.today(), datetime.time(datetime.now())

    if not cmd_args.no_pre:
        dir_name = ''
        for in_dir in cmd_args.input:
            cl = in_dir.split('/')[-5]
            dir_name += cl
            dir_name += '_'
        dir_name += cmd_args.agg
        dir_name += '_'
        dir_name += str(cmd_args.cut) + 's_'
        dir_name += '-'.join([cmd_args.window, str(cmd_args.windowsize)+'s', str(cmd_args.t_overlap)+'%'])
        dir_name += '_'
        dir_name += '-'.join(cmd_args.bands)
        dir_name += '-' + str(cmd_args.f_overlap)
        cmd_args.output = os.path.join(cmd_args.output, dir_name)

    warnings.filterwarnings("ignore", category=DeprecationWarning)
    warnings.filterwarnings("ignore", category=RuntimeWarning)
    warnings.filterwarnings("ignore", category=UndefinedMetricWarning)

    # create output directories
    io.check_out(cmd_args.output, cmd_args.input)
    name = cmd_args.output.split('/')[-1]
    if name == '':
        name = cmd_args.output.split('/')[-2]
    logname = os.path.join(cmd_args.output, name + '.log')
    formatter = logging.Formatter("[%(levelname)s] (%(module)s:%(lineno)d) %(message)s")
    logging.basicConfig(level=cmd_args.verbosity)

    rootlogger = logging.getLogger()
    rootlogger.handlers = []

    # add a logger that logs to console
    streamhandler = logging.StreamHandler()
    streamhandler.setFormatter(formatter)
    rootlogger.addHandler(streamhandler)

    # add a logger that logs to file
    filhandler = logging.FileHandler(logname)
    filhandler.setFormatter(formatter)
    rootlogger.addHandler(filhandler)

    logging.info('\tStarted on {} at {}.'.format(today, now))

    if not_known:
        logging.error('\tFollowing parameters could not be interpreted: {}.'.format(not_known))

    if 'all' in cmd_args.elecs:
        cmd_args.elecs = list(cleaner.ELECTRODES)
    if 'all' in cmd_args.domains:
        cmd_args.domains = feature_generator.DOMAINS
    else:
        cmd_args.domains = sorted(cmd_args.domains)

    if cmd_args.pre_only or (not cmd_args.pre_only and not cmd_args.no_pre):
        keys = cmd_args.__dict__
        args_listed = map(lambda x: ': '.join([x, str(keys[x]) if type(keys[x]) != list else ', '.join(keys[x])]), keys)
        logging.info('\tWill use the following parameters ...\n\t\t\t\t' +
                     '\n\t\t\t\t'.join(sorted(args_listed)))

    pl = pipeline.Pipeline(
        hmm_trainer.HMMTrainer(
            n_proc=cmd_args.n_proc,
            agg=cmd_args.agg,
            parallel_folds=cmd_args.parallel_folds,
            window_limit=cmd_args.window_limit,
        )
    )
    pl.run(cmd_args)
    
    logging.info('\tFinished on {} at {}.\n\n'.format(date.today(), datetime.time(datetime.now())))


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':

    parser = argparse.ArgumentParser(formatter_class=LineWrapRawTextHelpFormatter)

    # non-optional
    parser.add_argument('input', type=str, nargs='+', help='list of input directories / feature files if '
                                                           'preprocessing is skipped (-no-pre)')
    parser.add_argument('output', type=str, help='output directory')

    # optional
    parser.add_argument('--agg', type=str, default="none", choices=["mean", "median", "none"],
                        help='whether and how to aggregate features over time windows. default median')

    parser.add_argument('--bands', type=str, metavar='', nargs='+', help='frequency bands used in computation',
                        default=["0", "2", "4", "8", "13", "18", "24", "30", "60", "120"])

    parser.add_argument('--cut', type=int, default=None, metavar='',
                        help='toggles cutting of the signals by the specified amount')

    parser.add_argument('--domains', type=str, nargs='+', metavar='', default=['all'],
                        choices=feature_generator.DOMAINS + ['all'],
                        help='list of features to be computed / used for classification if preprocessing is skipped '
                             '(--no-pre). choose between: ' + ', '.join(feature_generator.DOMAINS) + ', all')

    parser.add_argument('--elecs', type=str, nargs='+', metavar='', default=['all'],
                        help='list of electrodes to be used for feature computation / prediction of preprocessing is '
                        'skipped (--no-pre). choose between: ' + ', '.join(cleaner.ELECTRODES) + ', all.')

    parser.add_argument('--feats', type=str, nargs='+', metavar='', default=['all'],
                        help='list of features to be computed / used for classification if preprocessing is '
                             'skipped (--no-pre). take a look at file "features.list" for choices.')

    parser.add_argument('--f_overlap', '--foverlap', action='store_true',
                        help='whether or not the frequency bands for should overlap by 50%%')

    parser.add_argument('--no-pre', '--nopre', action='store_true',
                        help='skips the preprocessing step. activate this when features are already available.')

    parser.add_argument('--n-proc', '--nproc', type=int, default=1, metavar='',
                        help='number of processes used to read data and compute features')

    parser.add_argument('--parallel-folds', '--parallelfolds', action='store_true',
                        help='whether or not cv should be done in parallel. requires more memory.')

    parser.add_argument('--no-up', '--noup', action='store_true',
                        help='use this option if you do NOT want to upload the results to google spreadsheet.')

    parser.add_argument('--pre-only', '--preonly', action='store_true',
                        help='set this if you want to compute features only. no predictions.')

    parser.add_argument('--subset', type=int, default=None, metavar='',
                        help='if a performance estimate on a random subset of the data is desired')

    parser.add_argument('--t_overlap', '--toverlap', type=int, default=0, metavar='',
                        help='percentage of overlap of time windows')

    parser.add_argument('--verbosity', type=int, default=20, metavar='', choices=[0, 10, 20, 30, 40, 50],
                        help='verbosity level of logging: ' + ', '.join(str(s) for s in [0, 10, 20, 30, 40, 50]))

    parser.add_argument('--visualize', type=str, nargs='+', metavar='', default=['train', 'pred', 'post'],
                        help='specify what steps of the pipeline should be visualized. choose between: ' +
                             ', '.join(['all', 'pre', 'train', 'pred', 'post', 'none']))

    parser.add_argument('--window-limit', '--windowlimit', type=int, default=None, metavar='',
                        help='toggles how many windows of the features should be used')

    parser.add_argument('-w', '--window', type=str, default='boxcar', choices=splitter.WINDOWS, metavar='',
                        help='window function to improve fourier transform. choose between: '
                             + ', '.join(splitter.WINDOWS))

    parser.add_argument('-ws', '--windowsize', '--window-size', type=float, default=2, metavar='',
                        help='length of the window in seconds')

    cmd_arguments, unknown = parser.parse_known_args()
    main(cmd_arguments, unknown)
