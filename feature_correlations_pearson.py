#!/usr/bin/env python3.5
from scipy.stats import spearmanr
from utils import io

import numpy as np
import sys
import os


# ______________________________________________________________________________________________________________________
def read_hdf_files(in_dir):
    agged = in_dir.split('_')[2]
    hdf5_files = io.read_all_file_names(in_dir, '.hdf5', key='natural')

    # read hdf5 files, patient domain does not have a time structure, eval files should not be read
    abnormal_hdf5_files, normal_hdf5_files = [], []
    for hdf5_file in hdf5_files:
        if "eval" in hdf5_file:
            continue

        hdf5_file_name = hdf5_file.split('/')[-1]
        if 'patient' in hdf5_file_name:
            continue

        if 'abnormal' in hdf5_file_name:
            abnormal_hdf5_files.append(hdf5_file)
        else:
            normal_hdf5_files.append(hdf5_file)

    # make sure hdf5 files are ordered as cwt, dwt, fft, pyeeg, sync, time
    normal_hdf5_files = sorted(normal_hdf5_files)
    abnormal_hdf5_files = sorted(abnormal_hdf5_files)
    return normal_hdf5_files, abnormal_hdf5_files, agged


# ______________________________________________________________________________________________________________________
def outer_correlations(hdf5_files):
    """ read all domains of a recording. mean over time windows. ravel. assemble. correlate over recordings """
    j = 0
    feature_matrix = []
    # for every reocrding of a class
    while True:
        # read the data from all domain files, mean over time windows, correlate over recordings
        all_features = []
        for hdf5_file in hdf5_files:
            data = io.read_domain_hdf5_successively(hdf5_file, str(j))

            if data is None:
                # n_recordings x N
                feature_matrix = np.stack(feature_matrix)
                # N x N
                return np.corrcoef(feature_matrix, rowvar=False)

            data = np.mean(data, axis=0).ravel()
            all_features.append(data)

        feature_matrix.append(np.hstack(all_features))

        j += 1


# ______________________________________________________________________________________________________________________
def inner_correlations(hdf5_files):
    """ read all domains of a recording. reshape to n_windows x N. correlate. assemble. average """
    j = 0
    pvalues = None
    correlations = None
    # for every reocrding of a class
    while True:
        # read the data from all domain files, correlate over time windows, mean over recordings
        all_features = []
        for hdf5_file in hdf5_files:
            data = io.read_domain_hdf5_successively(hdf5_file, str(j))

            if data is None:
                # N x N
                return correlations / j

            all_features.append(data.reshape(data.shape[0], -1))

        # n_windows x N
        all_features = np.hstack(all_features)

        # N x N
        if correlations is None:
            correlations = np.corrcoef(all_features, rowvar=False)
        else:
            tmp1 = np.corrcoef(all_features, rowvar=False)
            correlations += tmp1

        j += 1


# ______________________________________________________________________________________________________________________
def main(in_dir, out_dir):
    """  """
    normal_hdf5_files, abnormal_hdf5_files, agged = read_hdf_files(in_dir)

    # # compute correlation on normal features, abnormal features
    hdf_files_list = [normal_hdf5_files, abnormal_hdf5_files]
    l = ["normal", "abnormal"]

    print("computing outer correlation")
    #  mean over time windows, correlate over recordings
    for i, hdf5_files in enumerate(hdf_files_list):
        correlations, pvalues = outer_correlations(hdf5_files)
        print(correlations.shape)
        io.write_domain_hdf5_successively(os.path.join(out_dir, l[i]+'_outer_pearson_correlations.h5'), correlations, 0)

    # if time windows are not existent in the stored feature file, one cannot correlate over their dimension
    if agged != 'none':
        print("cannot compute inner correlation when windows are already aggregated")
        exit()

    print("computing inner correlation")
    # correlate over time windows, mean over recordings
    for i, hdf5_files in enumerate(hdf_files_list):
        correlations, pvalues = inner_correlations(hdf5_files)
        print(correlations.shape)
        io.write_domain_hdf5_successively(os.path.join(out_dir, l[i]+'_inner_pearson_correlations.h5'), correlations, 0)


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
