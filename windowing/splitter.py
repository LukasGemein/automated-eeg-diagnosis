from scipy import signal
import numpy as np
import unittest

WINDOWS = sorted([
    'barthann',
    'bartlett',
    'blackman',
    'blackmanharris',
    'bohman',
    'boxcar',
    'chebwin',
    'cosine',
    'exponential',
    'flattop',
    'gaussian',
    'hamming',
    'hann',
    'kaiser',
    'nuttall',
    'parzen',
    'slepian',
    'triang',
    'tukey',
    'multitaper',
])

# TODO: avoid duplication (only with time overlap) of data by storing start and end points of time window?


class Splitter(object):
    """
    """

# ______________________________________________________________________________________________________________________
    def apply_window_function(self, windows, window_size):
        """ weights the split signal by the specified window function
        :param windows: the signals split into time windows
        :param window_size: the number of samples in the window
        :return: the windows weighted by the specified window function
        """
        # TODO: switch to kwargs?
        # if the window function requires additional arguments, set them
        if self.window in ['chebwin', 'exponential', 'gaussian', 'kaiser', 'slepian', 'tukey']:
            if self.window == 'chebwin':
                args = [self.at]
            elif self.window == 'exponential':
                args = [self.center, self.tau]
            elif self.window == 'gaussian':
                args = [self.std]
            elif self.window == 'kaiser':
                args = [self.beta]
            elif self.window == 'tukey':
                args = [self.alpha]
            elif self.window == 'slepian':
                args = [self.width]

            method_to_call = getattr(signal, self.window)
            window = method_to_call(window_size, *args, sym=self.sym)

        # window functions without additional arguments
        else:
            try:
                method_to_call = getattr(signal, self.window)
            except AttributeError:
                return windows
            window = method_to_call(window_size, sym=self.sym)

        return windows * window

# ______________________________________________________________________________________________________________________
    def split_into_windows(self, signals, n_samples_in_window, stride):
        """
        :param signals: ndarray with the shape n_signals x n_samples
        :param n_samples_in_window: the number of samples in a single time window
        :param stride: the number of samples by which two adjacent time windows overlap
        :return: ndarray with the shape n_windows x n_signals x n_samples_in_window weighted by the window function
        """
        # written by robin tibor schirrmeister
        windows = []
        # stride cannot be 0 which would be 100% overlap
        for i_start in range(0, signals.shape[-1] - n_samples_in_window + 1, max(1, stride)):
            windows.append(np.take(signals, range(i_start, i_start + n_samples_in_window), axis=-1))

        return np.asarray(windows)

# ______________________________________________________________________________________________________________________
    def split(self, signals, sfreq, weight=True):
        """ splot a signal into time windows
        :param signals: list of signals
        :param sfreq: sampling frequency of the signals
        :param weight: whether or not to apply a window function
        :return: the (weigthed) windows of the signal
        """
        n_samples_in_window = int(sfreq * self.window_length)
        n_samples_in_overlap = int(self.overlap * n_samples_in_window)
        stride = n_samples_in_window - n_samples_in_overlap

        windows = self.split_into_windows(signals, n_samples_in_window, stride)

        if weight:
            windows = self.apply_window_function(windows, n_samples_in_window)

        return windows


# ______________________________________________________________________________________________________________________
    def get_n_windows(self, duration):
        if self.overlap == 0:
            return int(duration/self.window_length)
        else:
            return int(duration/self.window_length/(1-self.overlap)) -1

# ______________________________________________________________________________________________________________________
    def __init__(self, overlap, window, window_length):
        self.overlap = overlap/100.
        self.window = window
        self.window_length = window_length

        # additional arguments for window functions as taken from scipy.signal website examples
        self.at = 100
        self.std = 3
        self.beta = 14
        self.width = .3
        self.alpha = .5
        self.center = None
        self.tau = 1.

        # symmetric for filter design, asymmetric for spectral analysis. so...
        # self.sym = False
        self.sym = True


class TestSplitter(unittest.TestCase):
    def test_get_n_windows(self):
        sp = Splitter(
            overlap=0,
            window="boxcar",
            window_length=2
        )
        n_windows = sp.get_n_windows(
            680
        )
        self.assertEqual(n_windows, 340)
        n_windows = sp.get_n_windows(
            681
        )
        self.assertEqual(n_windows, 340)
        sp.overlap = .5
        n_windows = sp.get_n_windows(
            9
        )
        self.assertEqual(n_windows, 8)
        n_windows = sp.get_n_windows(
            10
        )
        self.assertEqual(n_windows, 9)
        # different overlap does not work correctly
        # sp.window_length = 3
        # sp.overlap = 1/3
        # n_windows = sp.get_n_windows(
        #     12
        # )
        # self.assertEqual(n_windows, 5)

    def test_split(self):
        sp = Splitter(
            overlap=0,
            window="boxcar",
            window_length=3
        )
        signals = np.arange(233*21*123+21*3).reshape(21, 233*123+3)
        epochs = sp.split(signals, sfreq=250, weight=False)
        self.assertEqual(epochs.shape, (38, 21, 750))
        sp.overlap = .5
        epochs = sp.split(signals, sfreq=250, weight=False)
        self.assertEqual(epochs.shape, (75, 21, 750))
        # this is not working
        # sp.overlap = .3
        # epochs = sp.split(signals, sfreq=250, weight=False)
        # self.assertEqual(epochs.shape, (50, 21, 750))

    def test_apply_window_function(self):
        sp = Splitter(
            overlap=0,
            window="blackmanharris",
            window_length=2
        )
        epochs = np.arange(113*21*251).reshape(113, 21, 251)
        epochs_weighted = sp.apply_window_function(epochs, 251)
        self.assertEqual(epochs_weighted.shape, epochs.shape)
        # what else can be tested here?
        # why isnt the weighting 0 at the left and right side?


if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSplitter)
    unittest.TextTestRunner(verbosity=2).run(suite)
