#!/usr/bin/env python3.5
from datetime import datetime, date

from utils import arg_parser, logger
from training import autoskl_trainer
import pipeline


# ______________________________________________________________________________________________________________________
def main(cmd_args, not_known):
    today, now = date.today(), datetime.time(datetime.now())
    logger.create_loggers(cmd_args.output, cmd_args.verbosity)
    logger.info('\tStarted on {} at {}.'.format(today, now))

    if not_known:
        logger.error('\tFollowing parameters could not be interpreted: {}.'.format(not_known))

    if cmd_args.auto_skl is None:
        logger.error("\tYou need to specify per run and total run time for autosklearn: "
                     "\n\t\t--auto_skl <total_tim> <run_time>")
        exit()

    pl = pipeline.Pipeline(
        autoskl_trainer.AutoSklearnTrainer(
            auto_skl=[int(b) for b in cmd_args.auto_skl],
            n_jobs=cmd_args.n_proc,
        )
    )
    pl.run(cmd_args)

    logger.info('\tFinished on {} at {}.\n\n'.format(date.today(), datetime.time(datetime.now())))


# ______________________________________________________________________________________________________________________
if __name__ == '__main__':
    parser = arg_parser.ArgParser()
    cmd_arguments, unknown = parser.parse_known_args()
    main(cmd_arguments, unknown)
